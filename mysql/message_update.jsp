<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_message.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<style type="text/css">
		.redTip{
			color:red;
		}
		.editTbl td{
			padding:2px 2px 2px 4px;
			height:25px;
		}
		.inputitem{
			width:150px;
		}
		</style>
		<script type="text/javascript">
		     function ValidateForm(theform){
		        if(theform.title.value==""){
		            alert("留言标题不能为空!");
		            theform.title.focus();
		            return false;
		        }
		        
		        if(theform.writer.value==""){
		            alert("联系人不能为空!");
		            theform.writer.focus();
		            return false;
		        }
		    }
		</script>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a>
						-&gt; 留言 
						-&gt; 修改
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
				<tr>
					<td height="22" class="title_td">
						&nbsp;
						<a href="message.jsp?act=list&pid=<%=params.get("pid")%>">列表</a> |&nbsp;
						<font color="#FF0000">修改</font>
					</td>
				</tr>
			</table>
			<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="padding-top:5px">
					<br/>
					<form action="message_update.jsp?act=doupdate<%=params.query("rowid,pid,psize,ref")%>" method="post" onsubmit="return ValidateForm(this)">
					<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
					<tr>
						<td bgcolor="#CEDDF0" width="90" style="font-weight:bold;width:90px;" align="left">标题：</td>
						<td bgcolor="#ffffff" colspan="5"><input type="text" name="title" size="50" class="inputbox1" value="<%=entity.get("TITLE")%>" /></td>
					</tr>
					<tr>
						<td bgcolor="#CEDDF0" width="90" style="font-weight:bold;width:90px;" align="left">相关文章：</td>
						<td bgcolor="#ffffff" colspan="3">
							<select name="articleid">
								<option value="">=选择文章=</option>
								<%
								for(int i = 0 ; i < articleList.size(); i++){
									DbMap item = (DbMap)articleList.get(i);
								%>
								<option value="<%=item.get("ROW_ID")%>" <%if(item.getString("ROW_ID").equals(entity.getString("ARTICLEID"))){%>selected="selected"<%}%>><%=item.get("TITLE")%></option>
								<%
								}
								%>
							</select>
						</td>
						<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">是否公开：</td>
						<td bgcolor="#ffffff">
							<input type="radio" name="isopen" value="0" <%if("0".equals(entity.getString("ISOPEN"))){%>checked="checked"<%}%> />否
							<input type="radio" name="isopen" value="1" <%if("1".equals(entity.getString("ISOPEN"))){%>checked="checked"<%}%> />是
						</td>
					</tr>
					<tr>
					<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">联系人：</td>
					<td bgcolor="#ffffff">
						<input type="text" name="writer" size="12" class="inputbox1" value="<%=entity.get("WRITER")%>" />
					</td>
					<td bgcolor="#CEDDF0" style="font-weight:bold;width:90px;" align="left" width="90">电话：</td>
					<td bgcolor="#ffffff" width="180" style="width:150px;"><input type="text" name="phone" size="20" class="inputbox1" value="<%=entity.get("PHONE")%>" /></td>
					<td bgcolor="#CEDDF0" style="font-weight:bold;width:90px;" align="left">手机号码：</td>
					<td bgcolor="#ffffff"><input type="text" name="mobile" size="20" class="inputbox1" value="<%=entity.get("MOBILE")%>" /></td>
					</tr>
					</table>
					<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
					<tr bgcolor="#CEDDF0">
						<td height="28" style="font-weight:bold;" align="left">
							内容：
						</td>
					</tr>
					<tr bgcolor="#ffffff">
						<td align="center">
						<textarea name="content" cols="60" rows="6" style="width:98%;margin:4px;font-size:12px;"><%=entity.get("CONTENT")%></textarea>
						</td>
					</tr>
					</table>
					<br/>
					<br/>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align=center>
						<tr>
							<td colspan="3" align="center">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<%if(operations.indexOf("|2001,update|") != -1 || "1".equals(current_userid)){%>
								<input type="submit" class="inputbox2" value=" 提 交 ">
								<%} %>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='message.jsp?act=list&pid=<%=params.get("pid")%>'">
								<br>
								<br>
							</td>
						</tr>
					</table>
					</form>
					<br/>
					<br/>
				</td>
			</tr>
		</table>
		<%if(!msg.emptyMessage()){%>
		<script type="text/javascript">
		alert('<%=msg.getMessage()%>');
		</script>
		<%}%>
		<%if(msg.getIsSuccess()){ %>
		<script type="text/javascript">
		location.href='message.jsp?act=list&pid=<%=params.get("pid")%>';
		</script>
		<%} %>
	</body>
</html>