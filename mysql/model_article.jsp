<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
List<DbMap> dbMapList = db.executeDbMapList("SELECT * FROM cms_variable ");
DbMap settings = new DbMap();
for(DbMap dbMap : dbMapList) settings.put(dbMap.getString("IDENTITY"), dbMap.getString("VAL"));

String act = params.get("act");
List<DbMap> itemList = new ArrayList<DbMap>();
List<String> dutyList = new ArrayList<String>();
List<DbMap> assortList = null;
List<DbMap> dwList = null;
Pager pager = new Pager(params.get("pid"), Util.getInteger(params.get("psize")) < 10 ? 10 : Util.getInteger(params.get("psize")));
DbMap entity = null;
String assortPath = "";

pager.setQueryString("act=" + params.get("act") + "&psize=" + params.get("psize"));

Parameters search = null;

if("ndlist".equals(act)) search = (Parameters) session.getAttribute("article_search_nodeal");
if("nalist".equals(act)) search = (Parameters) session.getAttribute("article_search_noaudit");
if("alist".equals(act)) search = (Parameters) session.getAttribute("article_search_audit");
if("slist".equals(act)) search = (Parameters) session.getAttribute("article_search_share");
if("dlist".equals(act)) search = (Parameters) session.getAttribute("article_search_deleted");

String where = "AND a.ASSORTID IN (SELECT ASSORTID FROM cms_user_assort WHERE USERID = '" + current.get("ROW_ID") + "')";
if(search == null) search = new Parameters();

if (!Util.empty(search.get("keyword"))) where += " and CONCAT(a.TITLE, a.SUMMARY, a.WRITER, b.DWMC, b.DWBH, c.NAME, c.NO) like '%" + search.get("keyword") + "%'";
if (!Util.empty(search.get("kstime"))) where += " and a.PUBDATE >= '" + search.get("kstime") + "'";
if (!Util.empty(search.get("jstime"))) where += " and a.PUBDATE <= '" + search.get("jstime") + "'";
if (!Util.empty(search.get("dwid"))) where += " and a.DWID = '" + search.get("dwid")+"'";

if (!Util.empty(search.get("assortid"))) {
	DbMap searchAssort = db.executeDbMap("SELECT * FROM cms_assort WHERE ROW_ID = ?", search.get("assortid"));
	if(searchAssort != null) where += " and c.PATH LIKE '" + searchAssort.getString("PATH") + "%'";
}

if (!Util.empty(search.get("sftype"))) where += " and a.STATE = '" + search.get("sftype") + "'";
if (!Util.empty(search.get("istop"))) where += " and a.ISTOP = '" + search.get("istop") + "'";
if (!Util.empty(search.get("isindex"))) where += " and a.ISINDEX = '" + search.get("isindex") + "'";

if("ndlist".equals(act)){
	List<?> list = db.executeDbMapPager("SELECT a.ROW_ID,a.UPDATEID,a.UPDATETIME,a.STATE,a.TITLE,a.PUBDATE,a.DUTY,a.BACKDES,a.ISTOP,a.ISINDEX,a.ORDERNO,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.STATE = -1 AND a.ISDELETED = 0 AND b.PATH LIKE '" + current.get("DWPATH") + "%' " + where + " ORDER BY a.PUBDATE DESC, a.UPDATETIME DESC", pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT `NAME` FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		itemList.add(item);
	}
}else if("nalist".equals(act)){
	List<?> list = db.executeDbMapPager("SELECT a.ROW_ID,a.UPDATEID,a.UPDATETIME,a.STATE,a.TITLE,a.PUBDATE,a.DUTY,a.BACKDES,a.ISTOP,a.ISINDEX,a.ORDERNO,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.STATE = 0 AND a.ISDELETED = 0 AND b.PATH LIKE '" + current.get("DWPATH") + "%' " + where + " ORDER BY a.PUBDATE DESC, a.UPDATETIME DESC", pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT NAME FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		itemList.add(item);
	}
}else if("alist".equals(act)){
	List<?> list = db.executeDbMapPager("SELECT a.ROW_ID,a.UPDATEID,a.UPDATETIME,a.STATE,a.TITLE,a.PUBDATE,a.DUTY,a.BACKDES,a.ISTOP,a.ISINDEX,a.ORDERNO,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.STATE > 0 AND a.ISDELETED = 0 AND b.PATH LIKE '" + current.get("DWPATH") + "%' " + where + " ORDER BY a.ISTOP DESC, a.ORDERNO DESC, a.PUBDATE DESC, a.UPDATETIME DESC", pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT NAME FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		itemList.add(item);
	}
}else if("slist".equals(act)){
	List<?> list = db.executeDbMapPager("SELECT a.ROW_ID,a.UPDATEID,a.UPDATETIME,a.STATE,a.TITLE,a.PUBDATE,a.DUTY,a.BACKDES,a.ISTOP,a.ISINDEX,a.ORDERNO,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.STATE > 0 AND a.ISOPEN = 1  AND a.ISDELETED = 0  " + where + " ORDER BY a.PUBDATE DESC, a.UPDATETIME DESC", pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT NAME FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		itemList.add(item);
	}
}else if("dlist".equals(act)){
	List<?> list = db.executeDbMapPager("SELECT a.ROW_ID,a.UPDATEID,a.UPDATETIME,a.STATE,a.TITLE,a.PUBDATE,a.DUTY,a.BACKDES,a.ISTOP,a.ISINDEX,a.ORDERNO,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID  AND a.ISDELETED = 1 AND b.PATH LIKE '" + current.get("DWPATH") + "%' " + where + " ORDER BY a.PUBDATE DESC, a.UPDATETIME DESC", pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT NAME FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		itemList.add(item);
	}
}else if("tlist".equals(act)){
	List<?> list = db.executeDbMapPager("SELECT a.ROW_ID,a.UPDATEID,a.UPDATETIME,a.STATE,a.TITLE,a.PUBDATE,a.DUTY,a.BACKDES,a.ISTOP,a.ISINDEX,a.ORDERNO,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID  AND a.STATE = -2 AND a.CREATEID = '" + current.get("ROW_ID") + "' ORDER BY a.PUBDATE DESC, a.UPDATETIME DESC", pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT NAME FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		itemList.add(item);
	}
}else if ("deletelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT TITLE FROM cms_article WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("TITLE");
			db.executeNonQuery("UPDATE cms_article SET ISDELETED = 1 WHERE ROW_ID = ?", checkbox);
		}
	}
	
	addLog("移除信息：" + itemMc, "10");
	String ref = params.get("ref");
	
	if("noaudit".equals(ref)){
		redirect("article_noaudit.jsp?act=nalist&pid=" + params.get("pid"));
	}else if("audit".equals(ref)){
		redirect("article_audit.jsp?act=alist&pid=" + params.get("pid"));
	}else if("nodeal".equals(ref)){
		redirect("article_nodeal.jsp?act=ndlist&pid=" + params.get("pid"));
	}
}else if ("movelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT TITLE FROM cms_article WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null && !Util.empty(params.get("hdnAssortId"))){
			itemMc += " " + tempDbMap.getString("TITLE");
			db.executeNonQuery("UPDATE cms_article SET ASSORTID = '" + params.get("hdnAssortId") + "' WHERE ROW_ID = ?", checkbox);
		}
	}
	
	addLog("移动信息：" + itemMc, "10");
	String ref = params.get("ref");
	
	if("noaudit".equals(ref)){
		redirect("article_noaudit.jsp?act=nalist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
	}else if("audit".equals(ref)){
		redirect("article_audit.jsp?act=alist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
	}else if("nodeal".equals(ref)){
		redirect("article_nodeal.jsp?act=ndlist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
	}
}else if ("orderlist".equals(act)){
	String[] rowids = request.getParameterValues("rowid");
	String[] ordernos = request.getParameterValues("orderno");
	String itemMc = "";
	
	if(rowids != null && ordernos != null){
		for (int i = 0; i < ordernos.length; i++) {
			DbMap tempDbMap = db.executeDbMap("SELECT TITLE FROM cms_article WHERE ROW_ID = ?", rowids[i]);
			if(tempDbMap != null){
				itemMc += " " + tempDbMap.getString("TITLE");
				db.executeNonQuery("UPDATE cms_article SET ORDERNO = ? WHERE ROW_ID = '" + rowids[i] + "'", ordernos[i]);
			}
		}
		
		addLog("批量排序信息：" + itemMc, "10");
	}
	
	redirect("article_audit.jsp?act=alist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
}else if ("deletedlist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT TITLE FROM cms_article WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("TITLE");
			db.executeNonQuery("DELETE FROM  cms_article WHERE ROW_ID = ?", checkbox);
		}
	}
	
	addLog("删除信息：" + itemMc, "10");
	
	if("temp".equals(params.get("ref"))) redirect("article_temp.jsp?act=tlist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
	else redirect("article_deleted.jsp?act=dlist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
}else if ("restorelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT TITLE FROM cms_article WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("TITLE");
			db.executeNonQuery("UPDATE cms_article SET ISDELETED = 0 WHERE ROW_ID = ?", checkbox);
		}
	}
	
	addLog("还原信息：" + itemMc, "10");
	redirect("article_deleted.jsp?act=dlist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
}else if ("auditlist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT TITLE FROM cms_article WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("TITLE");
			db.executeNonQuery("UPDATE cms_article SET STATE = 1, BACKDES = '' WHERE ROW_ID = ?", checkbox);
		}
	}
	
	addLog("审核信息：" + itemMc, "10");
	
	if("nodeal".equals(params.get("ref"))) redirect("article_nodeal.jsp?act=ndlist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
	else  redirect("article_noaudit.jsp?act=nalist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
}else if ("noauditlist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT TITLE FROM cms_article WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("TITLE");
			db.executeNonQuery("UPDATE cms_article SET STATE = 0 WHERE ROW_ID = ?", checkbox);
		}
	}
	
	addLog("不审核信息：" + itemMc, "10");
	redirect("article_nodeal.jsp?act=ndlist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
}else if ("articlelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT a.*,b.DWMC,b.DWBH,c.NAME,c.NO FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.ROW_ID = ?", checkbox);
		if(tempDbMap != null && tempDbMap.getInteger("STATE") != 2){
			DbMap boardDbMap = db.executeDbMap("SELECT * FROM board WHERE NO = ?", tempDbMap.getString("NO"));
			if(boardDbMap != null){
				itemMc += tempDbMap.getString("TITLE") + ",";
				db.executeNonQuery("UPDATE cms_article SET STATE = 2 WHERE ROW_ID = ?", checkbox);
			}
		}
	}
	
	addLog("发布信息", "10");
	redirect("article_audit.jsp?act=alist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
}else if ("update".equals(act) || "back".equals(act) || "view".equals(act)){
	String rowid = params.get("rowid");
	entity = db.executeDbMap("SELECT a.*,b.DWMC,b.DWBH,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.ROW_ID = ?", rowid);
	
	String indate = entity.getString("INDATE");
	if(indate.length() < 10) indate = "1900-01-01";
	else indate = indate.substring(0, 10);
	
	if("1900-01-01".equals(indate)) entity.put("INDATE", "");
	else entity.put("INDATE", indate);
	
	String assortCrumbString = "";
	DbMap entityAssort = db.executeDbMap("SELECT * FROM cms_assort WHERE ROW_ID = ?", entity.getString("ASSORTID"));
	if(entityAssort != null) {
		assortPath = entityAssort.getString("PATH");
		List<DbMap> assortCrumbs = db.executeDbMapList("SELECT * FROM cms_assort WHERE LENGTH(PATH) > 0 AND INSTR('" + entityAssort.getString("PATH") + "', PATH) = 1 ORDER BY PATH ASC");
		for(DbMap assortCrumb : assortCrumbs) assortCrumbString += entity.getString("assortCrumbs") + assortCrumb.getString("NAME") + "->";
	}
	
	if(assortCrumbString.endsWith("->")) assortCrumbString = assortCrumbString.substring(0, assortCrumbString.length() -2);
	entity.put("assortCrumbs", assortCrumbString);
	
	String[] imgList = Util.empty(entity.getString("IMGLIST")) ? new String[]{} : entity.getString("IMGLIST").split(",");
	for(int i = 0; i < imgList.length; i++){
		if(imgList[i].length() > 0 && !"/".equals(imgList[i].substring(0, 1))) imgList[i] = request.getContextPath() + "/" + imgList[i];
	}
	
	entity.put("IMGLIST", imgList);
}else if ("doupdate".equals(act)){
	String rowid = params.get("rowid");
	String title = params.get("title");
	String subject = params.get("subject");
	String writer = params.get("writer");
	String author = params.get("author");
	String source = params.get("source");
	String link = params.get("link");
	String pubdate = params.get("pubdate");
	String indate = params.get("indate");
	String duty = params.get("duty");
	String isopen = params.get("isopen");
	String istop = params.get("istop");
	String isindex = params.get("isindex");
	String content = params.get("content");
	String assortid = params.get("hdnAssortId");
	String iplimit = params.get("iplimit");
	String imglist = params.get("hdnPhotoPath");
	
	if(!Util.isUUID(assortid)) msg.putMessage("请选择具体分类！");
	else if(!Util.isDateString(pubdate)) msg.putMessage("发布时间格式不正确！");
	else{
		String img = "";
		
		if(Util.empty(params.get("hdnImgFocus"))){
		    if(content.indexOf("<img") != -1) {
		        String tmp = "";
		        tmp = content.substring(content.indexOf("<img"));
		        tmp = tmp.substring(0,tmp.indexOf(">")+1);
		
		        tmp = tmp.substring(tmp.indexOf("src=")+5);
		        img = tmp.substring(0,tmp.indexOf("\""));
		    }
		    
		    if("".equals(img)){
		        if (content.indexOf("<IMG")!=-1) {
		            String tmp = "";
		            tmp = content.substring(content.indexOf("<IMG"));
		            tmp = tmp.substring(0,tmp.indexOf(">")+1);
		            tmp = tmp.substring(tmp.indexOf("src=")+5);
		            img = tmp.substring(0,tmp.indexOf("\""));
		        }
		    }
		}else{
			img = params.get("hdnImgFocus");
		}
		
	    if(settings.getInteger("badwordenable") > 0) {
	    	content = cleanBadWord(content, settings.getString("badword"));
	    }
	    
	    String summary;
	    if(content == null || "".equals(content) || content.length() <= 0) summary = "";
		else{
			String contentTemp = content;
	    	contentTemp = contentTemp.replaceAll("<\\/?[^>]+>", "");
	    	contentTemp = contentTemp.replaceAll("&nbsp;", "");
	    	contentTemp = contentTemp.replaceAll("　", "");
	    	summary = contentTemp.length() > 200 ? contentTemp.substring(0, 200) : contentTemp;  
		}
	    
	    if(!Util.isDateString(indate)) indate = "1900-01-01 00:00:00";
	    
	    int entityState = -1;
	    
	    int auditflow = settings.getInteger("auditflow");
	    if(auditflow == 0) entityState = 1;
	    else{
	    	DbMap cassort = db.executeDbMap("SELECT * FROM cms_assort WHERE ROW_ID = ?", assortid);
	    	entityState = cassort.getInteger("STATE") == 1 ? 1 : -1;
	    }
	    
		DbCommand cmd = db.getSqlStringCommand("UPDATE cms_article SET TITLE=@TITLE,SUBJECT=@SUBJECT,CONTENT=@CONTENT,WRITER=@WRITER,PUBDATE=@PUBDATE," + 
				"AUTHOR=@AUTHOR, SOURCE=@SOURCE, LINK=@LINK,INDATE=@INDATE, " + 
				"UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME,IMG=@IMG,SUMMARY=@SUMMARY,ASSORTID=@ASSORTID,STATE=@STATE,BACKDES=''," +
				"DUTY=@DUTY,ISOPEN=@ISOPEN,ISTOP=@ISTOP,ISINDEX=@ISINDEX,IPLIMIT=@IPLIMIT,IMGLIST=@IMGLIST WHERE ROW_ID=@ROW_ID");
		cmd.addInParameter("ROW_ID", rowid);
		cmd.addInParameter("TITLE", title);
		cmd.addInParameter("SUBJECT", subject);
		cmd.addInParameter("CONTENT", content);
		cmd.addInParameter("WRITER", writer);
		cmd.addInParameter("AUTHOR", author);
		cmd.addInParameter("SOURCE", source);
		cmd.addInParameter("LINK", link);
		cmd.addInParameter("PUBDATE", pubdate);
		cmd.addInParameter("INDATE", indate);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.addInParameter("IMG", img);
		cmd.addInParameter("SUMMARY", summary);
		cmd.addInParameter("DWID", current.get("DWID"));
		cmd.addInParameter("ASSORTID", assortid);
		cmd.addInParameter("STATE", entityState);
		cmd.addInParameter("DUTY", duty);
		cmd.addInParameter("ISOPEN", Util.getInteger(isopen));
		cmd.addInParameter("ISTOP", Util.getInteger(istop));
		cmd.addInParameter("ISINDEX", Util.getInteger(isindex));
		cmd.addInParameter("IPLIMIT", iplimit);
		cmd.addInParameter("IMGLIST", imglist);
		
		cmd.executeNonQuery();
			
			
		addLog("修改信息：" + title , "10");
			
		msg.setIsSuccess(true);
		msg.setMessage(entityState == 1 ? "您已经成功修改信息！" : "您已经成功修改信息，请等待信息审核！");
	}
	
	entity = db.executeDbMap("SELECT a.*,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.ROW_ID = ?", rowid);
	
	indate = entity.getString("INDATE");
	if(indate.length() < 10) indate = "1900-01-01";
	else indate = indate.substring(0, 10);
	
	if("1900-01-01".equals(indate)) entity.put("INDATE", "");
	else entity.put("INDATE", indate);
	
	String[] imgList = Util.empty(entity.getString("IMGLIST")) ? new String[]{} : entity.getString("IMGLIST").split(",");
	for(int i = 0; i < imgList.length; i++){
		if(imgList[i].length() > 0 && !"/".equals(imgList[i].substring(0, 1))) imgList[i] = request.getContextPath() + "/" + imgList[i];
	}
	
	entity.put("IMGLIST", imgList);
}else if ("dosave".equals(act)){
	DbMap sentity = null;
	
	String rowid = params.get("rowid");
	String title = params.get("title");
	String subject = params.get("subject");
	String writer = params.get("writer");
	String author = params.get("author");
	String source = params.get("source");
	String link = params.get("link");
	String pubdate = params.get("pubdate");
	String indate = params.get("indate");
	String duty = params.get("duty");
	String isopen = params.get("isopen");
	String istop = params.get("istop");
	String isindex = params.get("isindex");
	String content = params.get("content");
	String assortid = params.get("hdnAssortId");
	String iplimit = params.get("iplimit");
	String imglist = params.get("hdnPhotoPath");
	
	if(!Util.isDateString(indate)) indate = "1900-01-01 00:00:00";
	if(Util.isUUID(rowid)) sentity = db.executeDbMap("SELECT * FROM cms_article WHERE ROW_ID = ?", rowid);
	
	String img = "";
	
	if(Util.empty(params.get("hdnImgFocus"))){
	    if(content.indexOf("<img") != -1) {
	        String tmp = "";
	        tmp = content.substring(content.indexOf("<img"));
	        tmp = tmp.substring(0,tmp.indexOf(">")+1);
	
	        tmp = tmp.substring(tmp.indexOf("src=")+5);
	        img = tmp.substring(0,tmp.indexOf("\""));
	    }
	    
	    if("".equals(img)){
	        if (content.indexOf("<IMG")!=-1) {
	            String tmp = "";
	            tmp = content.substring(content.indexOf("<IMG"));
	            tmp = tmp.substring(0,tmp.indexOf(">")+1);
	            tmp = tmp.substring(tmp.indexOf("src=")+5);
	            img = tmp.substring(0,tmp.indexOf("\""));
	        }
	    }
	}else{
		img = params.get("hdnImgFocus");
	}
	
	if(sentity == null){//添加保存
		if(Util.empty(title) || Util.empty(assortid) || Util.empty(content)) {
			redirect("article_add.jsp?act=add");
			return;
		}
		
		String uuid = Util.getUUID();
		DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_article(ROW_ID,TITLE,SUBJECT,CONTENT,WRITER,AUTHOR,SOURCE,LINK,PUBDATE,INDATE,STATE,CREATEID,CREATETIME,UPDATEID,UPDATETIME,IMG,SUMMARY,ASSORTID,DWID,DWBH,DUTY,ISOPEN,ISTOP,ISINDEX,IPLIMIT,IMGLIST) VALUES(" +
				"@ROW_ID,@TITLE,@SUBJECT,@CONTENT,@WRITER,@AUTHOR,@SOURCE,@LINK,@PUBDATE,@INDATE,@STATE,@CREATEID,@CREATETIME,@UPDATEID,@UPDATETIME,@IMG,@SUMMARY,@ASSORTID,@DWID,@DWBH,@DUTY,@ISOPEN,@ISTOP,@ISINDEX,@IPLIMIT,@IMGLIST)");
		cmd.addInParameter("ROW_ID", uuid);
		cmd.addInParameter("TITLE", title);
		cmd.addInParameter("SUBJECT", subject);
		cmd.addInParameter("CONTENT", content);
		cmd.addInParameter("WRITER", writer);
		cmd.addInParameter("AUTHOR", author);
		cmd.addInParameter("SOURCE", source);
		cmd.addInParameter("LINK", link);
		cmd.addInParameter("PUBDATE", pubdate);
		cmd.addInParameter("INDATE", indate);
		cmd.addInParameter("STATE", -2);
		cmd.addInParameter("CREATEID", current.get("ROW_ID"));
		cmd.addInParameter("CREATETIME", Util.getDate());
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.addInParameter("IMG", img);
		cmd.addInParameter("SUMMARY", "");
		cmd.addInParameter("DWID", current.get("DWID"));
		cmd.addInParameter("DWBH", current.get("DWBH"));
		cmd.addInParameter("ASSORTID", assortid);
		cmd.addInParameter("DUTY", duty);
		cmd.addInParameter("ISOPEN", Util.getInteger(isopen));
		cmd.addInParameter("ISTOP", Util.getInteger(istop));
		cmd.addInParameter("ISINDEX", Util.getInteger(isindex));
		cmd.addInParameter("IPLIMIT", iplimit);
		cmd.addInParameter("IMGLIST", imglist);
		
		cmd.executeNonQuery();
		
		rowid = uuid;
		params.set("rowid", uuid);
		params.set("ref", "temp");
	}else{
		DbCommand cmd = db.getSqlStringCommand("UPDATE cms_article SET TITLE=@TITLE,SUBJECT=@SUBJECT,CONTENT=@CONTENT,WRITER=@WRITER,PUBDATE=@PUBDATE," + 
				"AUTHOR=@AUTHOR, SOURCE=@SOURCE, LINK=@LINK,INDATE=@INDATE, " + 
				"UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME,ASSORTID=@ASSORTID,STATE=@STATE,BACKDES=''," +
				"DUTY=@DUTY,ISOPEN=@ISOPEN,ISTOP=@ISTOP,ISINDEX=@ISINDEX,IPLIMIT=@IPLIMIT,IMGLIST=@IMGLIST,IMG=@IMG WHERE ROW_ID=@ROW_ID");
		cmd.addInParameter("ROW_ID", rowid);
		cmd.addInParameter("TITLE", title);
		cmd.addInParameter("SUBJECT", subject);
		cmd.addInParameter("CONTENT", content);
		cmd.addInParameter("WRITER", writer);
		cmd.addInParameter("AUTHOR", author);
		cmd.addInParameter("SOURCE", source);
		cmd.addInParameter("LINK", link);
		cmd.addInParameter("PUBDATE", pubdate);
		cmd.addInParameter("INDATE", indate);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.addInParameter("DWID", current.get("DWID"));
		cmd.addInParameter("ASSORTID", Util.isUUID(assortid) ? assortid : sentity.get("ASSORTID"));
		cmd.addInParameter("STATE", -2);
		cmd.addInParameter("DUTY", duty);
		cmd.addInParameter("ISOPEN", Util.getInteger(isopen));
		cmd.addInParameter("ISTOP", Util.getInteger(istop));
		cmd.addInParameter("ISINDEX", Util.getInteger(isindex));
		cmd.addInParameter("IPLIMIT", iplimit);
		cmd.addInParameter("IMGLIST", imglist);
		cmd.addInParameter("IMG", img);
		
		cmd.executeNonQuery();
	}
	
	redirect("article_update.jsp?act=update" + params.query("rowid,pid,ref"));
	return;
}else if ("add".equals(act)){
	
}else if ("doadd".equals(act)){
	String title = params.get("title");
	String subject = params.get("subject");
	String writer = params.get("writer");
	String author = params.get("author");
	String source = params.get("source");
	String link = params.get("link");
	String pubdate = params.get("pubdate");
	String indate = params.get("indate");
	String duty = params.get("duty");
	String isopen = params.get("isopen");
	String istop = params.get("istop");
	String isindex = params.get("isindex");
	String content = params.get("content");
	String assortid = params.get("hdnAssortId");
	String iplimit = params.get("iplimit");
	String imglist = params.get("hdnPhotoPath");
	
	if(!Util.isUUID(assortid)) msg.putMessage("请选择具体分类！");
	else if(!Util.isDateString(pubdate)) msg.putMessage("发布时间格式不正确！");
	else{
		String img = "";
		
		if(Util.empty(params.get("hdnImgFocus"))){
		    if(content.indexOf("<img") != -1) {
		        String tmp = "";
		        tmp = content.substring(content.indexOf("<img"));
		        tmp = tmp.substring(0,tmp.indexOf(">")+1);
		
		        tmp = tmp.substring(tmp.indexOf("src=")+5);
		        img = tmp.substring(0,tmp.indexOf("\""));
		    }
		    
		    if("".equals(img)){
		        if (content.indexOf("<IMG")!=-1) {
		            String tmp = "";
		            tmp = content.substring(content.indexOf("<IMG"));
		            tmp = tmp.substring(0,tmp.indexOf(">")+1);
		            tmp = tmp.substring(tmp.indexOf("src=")+5);
		            img = tmp.substring(0,tmp.indexOf("\""));
		        }
		    }
		}else{
			img = params.get("hdnImgFocus");
		}
	    
	    if(settings.getInteger("badwordenable") > 0) content = cleanBadWord(content, settings.getString("badword"));
	    
	    String summary;
	    if(content == null || "".equals(content) || content.length() <= 0) summary = "";
		else{
			String contentTemp = content;
	    	contentTemp = contentTemp.replaceAll("<\\/?[^>]+>", "");
	    	contentTemp = contentTemp.replaceAll("&nbsp;", "");
	    	contentTemp = contentTemp.replaceAll("　", "");
	    	summary = contentTemp.length() > 200 ? contentTemp.substring(0, 200) : contentTemp; 
		}
	    
	    int entityState = -1;
	    
	    int auditflow = settings.getInteger("auditflow");
	    if(auditflow == 0) entityState = 1;
	    else{
	    	DbMap cassort = db.executeDbMap("SELECT * FROM cms_assort WHERE ROW_ID = ?", assortid);
	    	entityState = cassort.getInteger("STATE") == 1 ? 1 : -1;
	    }
	    
		String uuid = Util.getUUID();
		if(!Util.isDateString(indate)) indate = "1900-01-01 00:00:00";
		
		DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_article(ROW_ID,TITLE,SUBJECT,CONTENT,WRITER,AUTHOR,SOURCE,LINK,PUBDATE,INDATE,STATE,CREATEID,CREATETIME,UPDATEID,UPDATETIME,IMG,SUMMARY,ASSORTID,DWID,DWBH,DUTY,ISOPEN,ISTOP,ISINDEX,IPLIMIT,IMGLIST) VALUES(" +
				"@ROW_ID,@TITLE,@SUBJECT,@CONTENT,@WRITER,@AUTHOR,@SOURCE,@LINK,@PUBDATE,@INDATE,@STATE,@CREATEID,@CREATETIME,@UPDATEID,@UPDATETIME,@IMG,@SUMMARY,@ASSORTID,@DWID,@DWBH,@DUTY,@ISOPEN,@ISTOP,@ISINDEX,@IPLIMIT,@IMGLIST)");
		cmd.addInParameter("ROW_ID", uuid);
		cmd.addInParameter("TITLE", title);
		cmd.addInParameter("SUBJECT", subject);
		cmd.addInParameter("CONTENT", DbType.NText, content);
		cmd.addInParameter("WRITER", writer);
		cmd.addInParameter("AUTHOR", author);
		cmd.addInParameter("SOURCE", source);
		cmd.addInParameter("LINK", link);
		cmd.addInParameter("PUBDATE", pubdate);
		cmd.addInParameter("INDATE", indate);
		cmd.addInParameter("STATE", entityState);
		cmd.addInParameter("CREATEID", current.get("ROW_ID"));
		cmd.addInParameter("CREATETIME", Util.getDate());
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.addInParameter("IMG", img);
		cmd.addInParameter("SUMMARY", summary);
		cmd.addInParameter("DWID", current.get("DWID"));
		cmd.addInParameter("DWBH", current.get("DWBH"));
		cmd.addInParameter("ASSORTID", assortid);
		cmd.addInParameter("DUTY", duty);
		cmd.addInParameter("ISOPEN", Util.getInteger(isopen));
		cmd.addInParameter("ISTOP", Util.getInteger(istop));
		cmd.addInParameter("ISINDEX", Util.getInteger(isindex));
		cmd.addInParameter("IPLIMIT", iplimit);
		cmd.addInParameter("IMGLIST", imglist);
		
		cmd.executeNonQuery();
		
		addLog("发布信息：" + title , "10");
		msg.setIsSuccess(true);
		msg.setMessage(entityState == 1 ? "您已经成功发布信息！" : "您已经成功发布信息，请等待信息审核！");
	}
}else if ("doback".equals(act)){
	String rowid = params.get("rowid");
	String backdes = params.get("backdes");
	if(Util.empty(backdes)) msg.putMessage("退回理由不能为空！");
	else{
		String title = db.executeScalar("SELECT TITLE FROM cms_article WHERE ROW_ID = ?", rowid).toString();
		
		DbCommand cmd = db.getSqlStringCommand("UPDATE cms_article SET BACKDES=@BACKDES,STATE=-1," +
				"UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME WHERE ROW_ID=@ROW_ID");
		cmd.addInParameter("ROW_ID", rowid);
		cmd.addInParameter("BACKDES", backdes);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
			
		cmd.executeNonQuery();
			
			
		addLog("退回信息：" + title , "10");
			
		msg.setIsSuccess(true);
		msg.setMessage("您已经成功退回信息！");
	}
	
	entity = db.executeDbMap("SELECT a.*,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.ROW_ID = ?", rowid);
	
	String indate = entity.getString("INDATE");
	if(indate.length() < 10) indate = "1900-01-01";
	else indate = indate.substring(0, 10);
	
	if("1900-01-01".equals(indate)) entity.put("INDATE", "");
	else entity.put("INDATE", indate);
	
	if(!"1".equals(current_userid)){
		String content = current.getString("DWCONTENT");
		if(!Util.empty(content)){
			String[] dutys = current.getString("DWCONTENT").split(",");
			for(String dutyTemp : dutys) dutyList.add(dutyTemp);
		}
	}
	
	String[] imgList = Util.empty(entity.getString("IMGLIST")) ? new String[]{} : entity.getString("IMGLIST").split(",");
	for(int i = 0; i < imgList.length; i++){
		if(imgList[i].length() > 0 && !"/".equals(imgList[i].substring(0, 1))) imgList[i] = request.getContextPath() + "/" + imgList[i];
	}
	
	entity.put("IMGLIST", imgList);
}else if ("search".equals(act)){
	String ref = params.get("ref");
	if("nodeal".equals(ref)) session.removeAttribute("article_search_nodeal");
	if("noaudit".equals(ref)) session.removeAttribute("article_search_noaudit");
	if("audit".equals(ref)) session.removeAttribute("article_search_audit");
	if("share".equals(ref)) session.removeAttribute("article_search_share");
	if("deleted".equals(ref)) session.removeAttribute("article_search_deleted");
	
	dwList = db.executeDbMapList("SELECT * FROM cms_dw WHERE PATH LIKE '" + current.get("DWPATH") + "%'  ORDER BY DWBH ASC");
	
}else if ("dosearch".equals(act)){
	String ref = params.get("ref");
	
	Parameters searchParams = new Parameters();
	if(!Util.empty(params.get("keyword"))) searchParams.set("keyword", params.get("keyword"));
	if("1".equals(params.get("sftype")) || "0".equals(params.get("sftype")) || "-1".equals(params.get("sftype"))) searchParams.set("sftype", params.get("sftype"));
	if(!Util.empty(params.get("stime"))) searchParams.set("kstime", params.get("stime"));
	if(!Util.empty(params.get("etime"))) searchParams.set("jstime", params.get("etime"));
	if(!"0".equals(params.get("dwid"))) searchParams.set("dwid", params.get("dwid"));
	if(!Util.empty(params.get("hdnAssortId"))) searchParams.set("assortid", params.get("hdnAssortId"));
	
	if("1".equals(params.get("istop")) || "0".equals(params.get("istop"))) searchParams.set("istop", params.get("istop"));
	if("1".equals(params.get("isindex")) || "0".equals(params.get("isindex"))) searchParams.set("isindex", params.get("isindex"));
	
	dwList = db.executeDbMapList("SELECT * FROM cms_dw WHERE PATH LIKE '" + current.get("DWPATH") + "%'  ORDER BY DWBH ASC");
	
	if("nodeal".equals(ref)){
		session.setAttribute("article_search_nodeal", searchParams);
		href("article_nodeal.jsp?act=ndlist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
	}else if("noaudit".equals(ref)){
		session.setAttribute("article_search_noaudit", searchParams);
		href("article_noaudit.jsp?act=nalist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
	}else if("audit".equals(ref)){
		session.setAttribute("article_search_audit", searchParams);
		href("article_audit.jsp?act=alist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
	}else if("share".equals(ref)){
		session.setAttribute("article_search_share", searchParams);
		href("article_share.jsp?act=slist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
	}else if("deleted".equals(ref)){
		session.setAttribute("article_search_deleted", searchParams);
		href("article_deleted.jsp?act=dlist&pid=" + params.get("pid") + "&psize=" + params.get("psize"));
	}
	
}else if("ilist".equals(act)){
	entity = new DbMap();
	entity.put("nodealList", db.executeDbMapList("SELECT a.ROW_ID,a.UPDATEID,a.UPDATETIME,a.STATE,a.TITLE,a.PUBDATE,a.DUTY,a.BACKDES,a.ISTOP,a.ISINDEX,a.ORDERNO,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.STATE = -1 AND a.ISDELETED = 0 AND b.PATH LIKE '" + current.get("DWPATH") + "%' ORDER BY a.PUBDATE DESC, a.UPDATETIME DESC LIMIT 0,6"));
	entity.put("noauditList", db.executeDbMapList("SELECT a.ROW_ID,a.UPDATEID,a.UPDATETIME,a.STATE,a.TITLE,a.PUBDATE,a.DUTY,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.STATE = 0 AND a.ISDELETED = 0 AND b.PATH LIKE '" + current.get("DWPATH") + "%' ORDER BY a.PUBDATE DESC, a.UPDATETIME DESC LIMIT 0,6"));
	entity.put("auditList", db.executeDbMapList("SELECT a.ROW_ID,a.UPDATEID,a.UPDATETIME,a.STATE,a.TITLE,a.PUBDATE,a.DUTY,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.STATE > 0 AND a.ISDELETED = 0 AND b.PATH LIKE '" + current.get("DWPATH") + "%'  ORDER BY a.PUBDATE DESC, a.UPDATETIME DESC LIMIT 0,6"));
	entity.put("shareList", db.executeDbMapList("SELECT a.ROW_ID,a.UPDATEID,a.UPDATETIME,a.STATE,a.TITLE,a.PUBDATE,a.DUTY,b.DWMC,c.NAME FROM cms_article a, cms_dw b, cms_assort c WHERE a.DWID = b.ROW_ID AND a.ASSORTID = c.ROW_ID AND a.ISOPEN = 1 AND a.ISDELETED = 0  ORDER BY a.PUBDATE DESC, a.UPDATETIME DESC LIMIT 0,6"));
	entity.put("noticeList", db.executeDbMapList("SELECT a.ROW_ID,a.UPDATEID,a.UPDATETIME,a.STATE,a.TITLE,a.HITS FROM cms_notice a ORDER BY a.UPDATETIME DESC LIMIT 0,6"));
}else if("stat".equals(act)){
	if (!Util.empty(params.get("pstarttime"))) where += " and a.PUBDATE >= '" + params.get("pstarttime") + "'";
	if (!Util.empty(params.get("pendtime"))) where += " and a.PUBDATE <= '" + params.get("pendtime") + "'";
	itemList = db.executeDbMapList("SELECT d.*, " +
			"(SELECT COUNT(1) FROM cms_article a WHERE a.DWID = d.ROW_ID AND a.ISDELETED = 0 " + where + ") AS COUNT, " +
			"(SELECT COUNT(1) FROM cms_article a WHERE a.DWID = d.ROW_ID AND a.STATE = -1 AND a.ISDELETED = 0 " + where + ") AS NDCOUNT, " +
			"(SELECT COUNT(1) FROM cms_article a WHERE a.DWID = d.ROW_ID AND a.STATE = 0 AND a.ISDELETED = 0 " + where + ") AS NACOUNT, " +
			"(SELECT COUNT(1) FROM cms_article a WHERE a.DWID = d.ROW_ID AND a.STATE > 0 AND a.ISDELETED = 0 " + where + ") AS ACOUNT, " +
			"(SELECT COUNT(1) FROM cms_article a WHERE a.DWID = d.ROW_ID AND a.STATE = 2 AND a.ISDELETED = 0 " + where + ") AS RCOUNT " +
			"FROM cms_dw d WHERE d.PATH LIKE '" + current.get("DWPATH") + "%'  ORDER BY d.DWBH ASC");
	for(int i = 0; i < itemList.size();i++){
		DbMap item = (DbMap)itemList.get(i);
		String space = "";
		String[] paths = item.getString("PATH").split(",");
		if(paths.length > 1){
			for(int j = 1; j < paths.length; j++) space += "&nbsp;&nbsp;";
			item.put("DWMC", space + "&nbsp;└&nbsp;" + item.getString("DWMC"));
		}
	}
}
%>