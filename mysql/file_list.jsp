<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_file.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<link href="../editor/themes/default/default.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
		<script type="text/javascript" src="../editor/kindeditor.js"></script>
		<script type="text/javascript" src="../editor/lang/zh_CN.js"></script>
		
		<style type="text/css">
		.ke-button-common {background:#EDEDED;}
		</style>
		<script language="javascript" type="text/javascript">
	    function delone(urls) {
	        if (confirm("确认进行删除吗？")){
				 location = urls;
			}
	    }
	    
	    KindEditor.ready(function(K) {
			var uploadbutton = K.uploadbutton({
				button : K('#uploadButton')[0],
				fieldName : 'imgFile',
				url : 'upload_file.jsp?path=<%=params.get("path") %>',
				afterUpload : function(data) {
					if (data.error === 0) {
						location.href = 'file_list.jsp?act=list&path=<%=params.get("path") %>';
					} else {
						alert(data.message);
					}
				},
				afterError : function(str) {
					alert('自定义错误信息: ' + str);
				}
			});
			uploadbutton.fileBox.change(function(e) {
				uploadbutton.submit();
			});
		});
		</script>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
				&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a> 
				-&gt; <a href="file_list.jsp?act=list">模板</a> 
				<%for(DbMap crumb : crumbs){ %>
				<%if(!Util.empty(crumb.get("NAME"))){%>
				-&gt; <a href="file_list.jsp?act=list&path=<%=crumb.get("PATH")%>"><%=crumb.get("NAME")%></a> 
				<%}}%>
				</td>
				<td width="80" class="title" align="right">
				</td>
			</tr>
			<tr>
				<td height="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td" colspan="2">
					&nbsp;
					<a href="file_list.jsp?act=list&path=<%=params.get("path") %>"><font color="#FF0000">列表</font>
					</a> |
					<%if(operations.indexOf("|3002,add|") != -1 || "1".equals(current_userid)){%>
					<input type="button" id="uploadButton" value="添加文件" style="border:0; background:none;"/>
					<%} %>
				</td>
			</tr>
		</table>
		<form name="formlist" id="formlist" method="post" action="dw_list.jsp?act=deletelist&predwid=<%=params.get("predwid") %>">
			<br/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table width="95%" align="center" border="0" class="TableList"
							cellpadding="0" cellspacing="0">
							<tr align="center">
								<td width="30" height="21" class="biaot">
									序号
								</td>
								<td class="biaot" align="left">
									名称
								</td>
								<td width="80" class="biaot">
									大小
								</td>
								<td width="150" class="biaot">
									更新时间
								</td>
								<td class="biaot1" width="90">
									操作
								</td>
							</tr>
							<%for(int i = 0; i < itemList.size();i++){
								DbMap item = (DbMap)itemList.get(i);
							%>
								<tr bgcolor="#FFFFFF" onMouseOver="overtable(this);"
									onMouseOut="outtable(this);" id="<%=i + 1%>">
									<td height="21" class="biaol" align="center">
										<%=i + 1%>
									</td>
									<td class="biaol" style="font-size:16px; padding:4px;">
										<%if(item.get("TYPE").equals("DIR")){%>
										<img src="images/folder.gif" border="0">
										<a href="file_list.jsp?act=list&path=<%=params.get("path") %><%=item.get("NAME")%>/"><%=item.get("NAME")%></a>
										<%}else{%>
										<img src="images/file.gif" border="0">
										<%=item.get("NAME")%>
										<%}%>
									</td>
									<td class="biaol" align="center">&nbsp;<%=item.get("SIZE")%></td>
									<td class="biaol" align="center">
										<%=Util.format(item.get("UPDATETIME"), "yyyy-MM-dd h:i")%>
									</td>
									<td class="biaol" align="center">
										<%if(item.get("TYPE").equals("FILE")){%>
										<%if(item.getBoolean("EDIT")){%>
										<%if(operations.indexOf("|2003,update|") != -1 || "1".equals(current_userid)){%>
											<a href="file_update.jsp?act=update&file=<%=item.get("NAME")%>&path=<%=params.get("path") %>">修改</a>
										<%}}%>
										<%if((operations.indexOf("|2003,delete|") != -1 || "1".equals(current_userid))){%>
											<a href="javascript:delone('file_list.jsp?act=delete&file=<%=item.get("NAME")%>&path=<%=params.get("path") %>')">删除</a>
										<%} %>
										<%}else{%>
										—
										<%} %>
									</td>
								</tr>
							<%} %>
						</table>
						<%if(itemList.size() == 0){ %>
						<table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
							<tr><td height="60">未发现文件！</td></tr>
						</table>
						<%} %>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>