<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_setting.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>###</title>
	<link href="images/style.css" rel="stylesheet" type="text/css">
	<link href="images/datepicker.css" rel="stylesheet" type="text/css" />

	<script src="js/jquery-1.7.1.js" type="text/javascript"></script>
	<script src="laydate/laydate.js" type="text/javascript"></script>
	<script src="../editor/kindeditor.js" type="text/javascript"></script>
	<script src="../editor/lang/zh_CN.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	KindEditor.ready(function(K){
		var kindEditor = K.create('#txt_copyright', {
			resizeType:1, 
			pasteType:1, 
			allowImageUpload:true,
			allowFileManager:false, 
			allowPreviewEmoticons:false, 
			uploadJson : 'upload_json.jsp', 
			items : ['source', 'bold', 'italic', 'underline', 'link']
		});
	});
	</script>
<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
<form action="settings.jsp?act=setup" method="post">
<table width="95%" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="title">&nbsp;&nbsp;系统管理 -&gt; <font color="#FF0000">参数配置</font></td>
  </tr>
  <tr>
    <td height="2"></td>
  </tr>
  
</table><br>
    <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td align="center" class="title_td1">
        <br>
        <br>
         <table width="90%" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr >
                  <td height="25" align="right">站点名称：</td>
                  <td width="76%" align="left"><input type="text" name="sysname" class="inputbox1" size="25"  value="<%=settings.getString("sysname")%>"></td>
                </tr>
                <tr >
                  <td height="25" align="right">管理员：</td>
                  <td align="left"><input type="text" name="adminname" class="inputbox1" size="25" value="<%=settings.getString("adminname")%>"></td>
                </tr>
                <tr style="display:none;">
                  <td height="25" align="right">敏感词过滤：</td>
                  <td align="left">
                  <%if(settings.getInteger("badwordenable") > 0){ %>
                  <input type="radio" name="badwordenable" value="1" checked="checked">开启
                  <input type="radio" name="badwordenable" value="0">关闭
                  <%}else{ %>
                  <input type="radio" name="badwordenable" value="1">开启
                  <input type="radio" name="badwordenable" value="0" checked="checked">关闭
                  <%} %>
                  </td>
                </tr>
                <tr style="display:none;">
                  <td height="25" align="right">过滤词语：</td>
                  <td align="left">
                  <textarea name="badword" rows="8" cols="45" class="inputarea"><%=settings.getString("badword") %></textarea>
                  <div style="padding:6px;">词语用半角空格隔开</div>
                  </td>
                </tr>
                <tr>
                  <td height="25" align="right">文章审核：</td>
                  <td align="left">
                  <%if(settings.getInteger("auditflow") == 1){ %>
                  <input type="radio" name="auditflow" value="1" checked="checked">审核后发布
                  <input type="radio" name="auditflow" value="0">直接发布
                  <%}else{ %>
                  <input type="radio" name="auditflow" value="1">审核后发布
                  <input type="radio" name="auditflow" value="0" checked="checked">直接发布
                  <%} %>
                  </td>
                </tr>
                <tr>
                  <td height="25" align="right">留言板分类：</td>
                  <td align="left"><input type="text" name="assort" class="inputbox1" size="25" value="<%=settings.getString("assort")%>"></td>
                </tr>
                <tr>
                  <td height="25" align="right">附件存放路径：</td>
                  <td align="left"><input type="text" name="uploadpath" class="inputbox1" size="25" value="<%=settings.getString("uploadpath")%>"></td>
                </tr>
                <tr>
                  <td height="25" align="right">版权内容：</td>
                  <td align="left">
                  <textarea name="copyright" id="txt_copyright" cols="45" rows="4" style="width:70%; margin:4px;height:200px;"><%=settings.getString("copyright")%></textarea>
                  </td>
                </tr>
                <tr >
                  <td height="25" align="right">计数器：</td>
                  <td align="left"><input type="text" name="counter" class="inputbox1" size="10" value="<%=settings.getString("counter")%>"></td>
                </tr>
                <tr>
                	<td></td>
                	<td>
                	 <br/>
                	 <br/>
                	<%if(operations.indexOf("|4001,setup|") != -1 || "1".equals(current_userid)){%>
                	<input type="submit" class="inputbox2" value=" 确 定 ">
                	<%} %>
                	</td>
                </tr>
            </table>
            <br/>
            <br/>
    </td>
  </tr>
</table>
</form>
<%if(!msg.emptyMessage()){%>
<script type="text/javascript">
alert('<%=msg.getMessage()%>');
</script>
<%}%>
<%if(msg.getIsSuccess()){ %>
<script type="text/javascript">
location.href='settings.jsp?act=list';
</script>
<%}%>
</body>

</html>
