<%@ page language="java" pageEncoding="utf-8"%>
<%@ page import="com.fairy.lib.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.IOException"%>
<%@ page import="javax.servlet.http.HttpServletResponse"%>
<%!
private Database db;
private ActionMessage msg;
private Parameters params;
private String path;
private String serverName;

private DbMap current;
private String operations;
private String current_userid;
private HttpServletRequest pri_request;
private HttpServletResponse pri_response;

private void addLog(String content, String identity) {
	DbMap resource = (DbMap)db.executeDbMap("SELECT * FROM cms_resource WHERE `IDENTITY` = ?", identity);
	DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_log(ROW_ID,CONTENT,CREATEID,RESOURCEID,CREATEIP,CREATETIME,DWID) " +
			"VALUES(@ROW_ID,@CONTENT,@CREATEID,@RESOURCEID,@CREATEIP,@CREATETIME,@DWID)");
	cmd.addInParameter("ROW_ID", Util.getUUID());
	cmd.addInParameter("CONTENT", content);
	cmd.addInParameter("CREATEID", current.getString("ROW_ID"));
	cmd.addInParameter("RESOURCEID", resource == null ? "0" : resource.getString("ROW_ID"));
	cmd.addInParameter("CREATEIP", current.getString("LOGINIP"));
	cmd.addInParameter("CREATETIME", Util.getDate());
	cmd.addInParameter("DWID", current.getString("DWID"));
	cmd.executeNonQuery();
}

private void href(String url) throws IOException{
	pri_response.getWriter().println("<script type=\"text/javascript\">location.href='" + url + "';</script>");
}

private void redirect(String url) throws IOException{
	pri_response.sendRedirect(url);
}

private void forward(String url) throws ServletException, IOException{
	getServletContext().getRequestDispatcher(pri_request.getContextPath() + url).forward(pri_request, pri_response);
}

private void assign(String key, Object value){
	pri_request.setAttribute(key, value);
}

public String cleanBadWord(String content, String badwords){
	if(badwords == null || "".equals(badwords)) return content;
	String[] badwordArray = badwords.split(" ");
	for(String badword : badwordArray){
		content = content.replaceAll(badword, "<font color=red>**</font>");
	}
	return content;
}
%>
<%
path = request.getContextPath();
serverName = request.getServerName().toLowerCase();

response.setContentType("text/html;charset=utf-8");
request.setCharacterEncoding("utf-8");

ConnectSetting setting = new ConnectSetting("cms_java");

///mysql
setting.setHost("localhost");
setting.setPort("3306");
setting.setUser("root");
setting.setPassword("mysql");
setting.setPool("50");
setting.setDatabaseName("cms_java");
setting.setProvider("mysql");

db = DatabaseFactory.create(setting);

msg = new ActionMessage();
params = new Parameters(request);

pri_request = request;
pri_response = response;
%>