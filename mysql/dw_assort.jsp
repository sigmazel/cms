<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_dw.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<link href="zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="js/jquery.ztree.core.js"></script>
		<script type="text/javascript" src="js/jquery.ztree.excheck.js"></script>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">

		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
					&nbsp;&nbsp;用户管理 
					-&gt; <a href="dw_list.jsp?act=list">单位</a> 
					<%for(int i = 0; i < crumbs.size();i++ ){
					DbMap crumb = (DbMap)crumbs.get(i);
					%>
					-&gt; <a href="dw_list.jsp?act=list&predwid=<%=crumb.get("ROW_ID")%>"><%=crumb.get("DWMC")%></a>
					<%} %>
					-&gt; 分类
				</td>
			</tr>
			<tr>
				<td height="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td">
					&nbsp;
					<a href="dw_list.jsp?act=list&predwid=<%=params.get("predwid")%>">列表</a> |&nbsp;
					<font color="#FF0000">分类</font>
				</td>
			</tr>
		</table>
		<br>
		<form action="dw_assort.jsp?act=doassort&dwid=<%=params.get("dwid")%>&predwid=<%=params.get("predwid")%>" method="post">
		<input type="hidden" name="dwAssortIdString" value="<%=dwAssortIdString%>"/>
			<table width="95%" align="center" border="0" cellpadding="0"
				cellspacing="0">
				<tr>
					<td align="center" class="title_td1">
						<br>
						<br>
						<br>
						<table width="75%" align="center" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="35%" height="30" align="right" valign="top">
									系统分类列表：
								</td>
								<td width="65%" align="left">
									<div><input type="checkbox" name="toggleAll" onClick="toggle_ztree_all(this);">全部选中？</div>
									<div id="divTree" class="ztree"></div>
								</td>
							</tr>
						</table>
						<p>
							<%if(operations.indexOf("|300201,setup|") != -1 || "1".equals(current_userid)){%>
								<input type="button" class="inputbox2" value=" 提 交 " onclick="setupDwAssort(this.form)">
							<%} %>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" class="inputbox2" value=" 返 回 " onclick="location='dw_list.jsp?act=list&predwid=<%=params.get("predwid")%>'">
							<br>
							<br>
						</p>
						<p>
							&nbsp;
							<br>
							<br>
						</p>
					</td>
				</tr>

			</table>
		<script type="text/javascript">
		var setting = {
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};

		setting.check.chkboxType = {Y:'Y',N:''};
		
		var zNodes =[<%for(DbMap dbMap : assortList){%>
			{id:'<%=dbMap.getString("ROW_ID")%>', pId:'<%=dbMap.getString("PARENTID")%>', name:"<%=dbMap.getString("NAME")%>", checked:<%=dbMap.getString("CHECKED")%>},<%}%>
			{}
		];
		
		
		$.fn.zTree.init($("#divTree"), setting, zNodes);
		var zTreeObj = $.fn.zTree.getZTreeObj("divTree");
		
		function setupDwAssort(frm){
			var dwAssortIdString = frm.dwAssortIdString;
			dwAssortIdString.value = '';
			var nodes = zTreeObj.getCheckedNodes(true);
			for(var i = 0 ; i < nodes.length; i++) if(nodes[i].checked) dwAssortIdString.value = dwAssortIdString.value + ',' + nodes[i].id;
			frm.submit();
		}
		
		function toggle_ztree_all(cbx){
			zTreeObj.checkAllNodes(cbx.checked);
		}
		</script>
		<%if(!msg.emptyMessage()){%>
		<script type="text/javascript">
		alert('<%=msg.getMessage()%>');
		</script>
		<%}%>
		<%if(msg.getIsSuccess()){ %>
		<script type="text/javascript">
		location.href='dw_list.jsp?act=list&predwid=<%=params.get("predwid")%>';
		</script>
		<%} %>
	</body>

</html>
