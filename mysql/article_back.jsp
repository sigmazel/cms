<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_article.jsp" %>
<%
String ref = params.get("ref");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<style type="text/css">
		.redTip{
			color:red;
		}
		.editTbl td{
			padding:2px 2px 2px 4px;
			height:25px;
		}
		.inputitem{
			width:150px;
		}
		</style>
		<script language="JavaScript" type="text/javascript">
		     function ValidateForm(theform){
		        if(theform.backdes.value==""){
		            alert("退回理由不能为空!");
		            theform.backdes.focus();
		            return false
		        }
		    }
		</script>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">	
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a> 
						-&gt; 已审核文章 
						-&gt; 退回
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
				<tr>
					<td height="22" class="title_td">
						&nbsp;
						<a href="article_audit.jsp?act=alist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">退回</font>
					</td>
				</tr>
			</table>
			<table width="95%" border="0" align="center"
				cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="padding-top:5px">
		<br/>
		<form action="article_back.jsp?act=doback<%=params.query("rowid,pid,psize,ref")%>" method="post" onsubmit="return ValidateForm(this)">
			<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
			<tr>
				<td bgcolor="#CEDDF0" width="90" style="font-weight:bold;width:90px;" align="left">信息分类：</td>
				<td bgcolor="#ffffff" colspan="5"><%=entity.get("assortCrumbs")%></td>
			</tr>
			<tr>
				<td bgcolor="#CEDDF0" style="font-weight:bold;width:90px;" align="left">标题：</td>
				<td bgcolor="#ffffff" colspan="5"><%=entity.get("TITLE")%></td>
			</tr>
			<tr>
			<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">是否共享：</td>
			<td bgcolor="#ffffff" width="90">
			<%if("0".equals(entity.getString("ISOPEN"))){%>否<%}%>
			<%if("1".equals(entity.getString("ISOPEN"))){%>是<%}%>
			</td>
			<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left" width="90">发布时间：</td>
			<td bgcolor="#ffffff" width="150" style="width:120px;"><%=entity.get("PUBDATE")%></td>
			<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left" width="90">过期时间：</td>
			<td bgcolor="#ffffff"><%=entity.get("INDATE")%></td>
			</tr>
			</table>
			<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
			<tr bgcolor="#CEDDF0">
				<td height="28" style="font-weight:bold;" align="left">
					内容：
				</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="left">
				<%=entity.get("CONTENT")%>
				</td>
			</tr>
			</table>
			<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
			<tr bgcolor="#CEDDF0">
				<td height="28" style="font-weight:bold;" align="left">
					退回理由(200字以内)：
				</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="left">
				<textarea name="backdes" cols="60" rows="4" style="width:98%;margin:4px;"></textarea>
				</td>
			</tr>
			</table>
			<br/>
			<br/>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align=center>
				<tr>
					<td colspan="3" align="center">
						&nbsp;&nbsp;&nbsp;&nbsp;
						<%if(operations.indexOf("|1006,back|") != -1 || "1".equals(current_userid)){%>
						<input type="submit" class="inputbox2" value=" 提 交 ">
						<%} %>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_audit.jsp?act=alist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
						<br>
						<br>
					</td>
				</tr>
			</table>
			</form>
			<br/>
			<br/>
			</td>
			</tr>
		</table>
		<%if(!msg.emptyMessage()){%>
		<script type="text/javascript">
		alert('<%=msg.getMessage()%>');
		</script>
		<%}%>
		<%if(msg.getIsSuccess()){ %>
		<script type="text/javascript">
		location.href='article_audit.jsp?act=alist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>';
		</script>
		<%} %>
	</body>
</html>