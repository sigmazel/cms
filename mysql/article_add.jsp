<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_article.jsp" %>
<%
String ref = params.get("ref");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<link href="images/datepicker.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
		.redTip{
			color:red;
		}
		.editTbl td{
			padding:2px 2px 2px 4px;
			height:25px;
		}
		.inputitem{
			width:150px;
		}
		</style>
		<script src="js/jquery-1.7.1.js" type="text/javascript"></script>
		<script src="laydate/laydate.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="../swfupload/swfupload.js"></script>
		<script type="text/javascript" src="../swfupload/swfupload_handlers.js"></script>
		<script type="text/javascript" src="../swfupload/swfupload_images.js"></script>
		
		<script type="text/javascript" src="../editor/kindeditor.js"></script>
		<script type="text/javascript" src="../editor/lang/zh_CN.js"></script>
		
		<script type="text/javascript">
			var swfu;
			
			function uploadSuccess(file, serverData){
				try {
					var progress = new FileProgress(file,  this.customSettings.upload_target);
					var maxdisplayorder = 0;
					$('#div_thumb_list input').each(function(index, item){
						if(item.name.indexOf('PhotoDisplayOrder') != -1 && item.value) {
							maxdisplayorder = item.value.isInt() ? item.value - 0 : 1;
						}
					});
					
					maxdisplayorder++;
					
					if(serverData.indexOf('FILEID:') != -1) serverData = serverData.substring(serverData.indexOf('FILEID:'));
					
					if (serverData.substring(0, 7) === "FILEID:") {
						addImage(serverData.substring(7));
						progress.setStatus("图片加载完成");
						progress.toggleCancel(false);
					} else {
						addImage("../swfupload/error.gif");
						progress.setStatus("错误");
						progress.toggleCancel(false);
						alert(serverData);
					}
				} catch (ex) {}
			}
			
			
			function removePhoto(link){
				$(link).parent().parent().remove();
				checkPhotos();
			}
			
			function checkPhotos(){
				if($("#div_thumb_list .photo_focus").length == 0){
					var photo = $("#div_thumb_list .photo").get(0);
					$(photo).addClass('photo_focus');
				}
			}
			
			function photoFocus(photo){
				$("#div_thumb_list .photo_focus").removeClass('photo_focus').addClass('photo');
				$(photo).removeClass('photo').addClass('photo_focus');
			}
			
			function addImage(src){
				$('#div_thumb_list').append($('#div_thumb_tpl').html());
				var tmp = $('#div_thumb_list .thumb').last();
				
				$('img', tmp).attr('src', src);
				$('input', tmp).val('');
				$("input[name='hdnPhotoPath']", tmp).val(src);
				
				checkPhotos();
			}
			
			swfupload_params.limit = 0;
			swfupload_params.uploaded = 0;
			
			KindEditor.ready(function(K){
				var kindEditor = K.create('#txt_content', {
					resizeType:1, 
					pasteType:1, 
					allowImageUpload:true,
					allowFileManager:false, 
					allowPreviewEmoticons:false, 
					uploadJson : 'upload_json.jsp', 
					items : ['source', '|', 'copy', 'paste', 'plainpaste', 'wordpaste', '|', 'bold', 'italic', 'underline', 'justifyleft', 'justifycenter', 'justifyright', 'link', 'insertfile', 'image', 'template']
				});
			});
			
		    function ValidateForm(theform){
		        if(theform.title.value==""){
		            alert("文章标题不能为空!");
		            theform.title.focus();
		            return false;
		        }
		        
		        if(theform.hdnAssortId.value == '-1' || theform.hdnAssortId.value == '0'){
		            alert("请选择具体分类！");
		            return false;
		        }
		        
		        theform.hdnImgFocus.value = $("#div_thumb_list .photo_focus").length > 0 ? $("#div_thumb_list .photo_focus").attr('src') : '';
		        
		        return true;
		    }
		    
		    function form_save(frm){
		    	frm.action = 'article_add.jsp?act=dosave<%=params.query("pid,psize,ref")%>';
		    	frm.hdnImgFocus.value = $("#div_thumb_list .photo_focus").attr('src');
		    	frm.submit();
		    }
		    
		    $(function(){
				swfu = new SWFUpload({
					upload_url: "upload_img.jsp?crypt=<%=current.get("ROW_ID")%>",
					
					file_size_limit : "4 MB", 
					file_types : "*.jpg;*.bmp;*.gif",
					file_types_description : "JPG Images",
					file_upload_limit : "0",
					
					file_queue_error_handler : fileQueueError,
					file_dialog_complete_handler : fileDialogComplete,
					upload_progress_handler : uploadProgress,
					upload_error_handler : uploadError,
					upload_success_handler : uploadSuccess,
					upload_complete_handler : uploadComplete,
					
					button_image_url : "../swfupload/swfupload.png",
					button_placeholder_id : "spnButtonPlaceholder",
					button_width: 120,
					button_height: 18,
					button_text : '<span class="button">选择图片<span class="buttonSmall">(限2MB)</span></span>',
					button_text_style : '.button{ font-family: "宋体"; font-size:12px; color:#000;} .buttonSmall{font-size:12px;color:#000000;}',
					button_text_top_padding: 0,
					button_text_left_padding: 18,
					button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
					button_cursor: SWFUpload.CURSOR.HAND,
					
					flash_url : "../swfupload/swfupload.swf",
					
					custom_settings : {
						upload_target : "divFileProgressContainer"
					},
					
					debug: false
				});
		    });
		</script>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a>
						<%if("noaudit".equals(ref)){%>
						-&gt; 未通过信息 
						-&gt; 添加
						<%}else if("audit".equals(ref)){%>
						-&gt; 已通过信息 
						-&gt; 添加
						<%}else if("temp".equals(ref)){%>
						-&gt; 草稿箱 
						-&gt; 添加
						<%}else if("deleted".equals(ref)){%>
						-&gt; 回收站 
						-&gt; 添加
						<%}else if("share".equals(ref)){%>
						-&gt; 共享信息 
						-&gt; 添加
						<%}else{%>
						-&gt; 快速发布
						<%} %>
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
				<tr>
					<td height="22" class="title_td">
						&nbsp;
						<%if("noaudit".equals(ref)){%>
						<a href="article_noaudit.jsp?act=nalist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">添加</font>
						<%}else if("audit".equals(ref)){%>
						<a href="article_audit.jsp?act=alist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">添加</font>
						<%}else if("temp".equals(ref)){%>
						<a href="article_temp.jsp?act=tlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">添加</font>
						<%}else if("deleted".equals(ref)){%>
						<a href="article_deleted.jsp?act=dlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">添加</font>
						<%}else if("share".equals(ref)){%>
						<a href="article_share.jsp?act=slist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">添加</font>
						<%}else{%>
						<font color="#FF0000">快速发布</font>
						<%} %>
					</td>
				</tr>
			</table>
			<table width="95%" border="0" align="center"
				cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="padding-top:5px">
					<br/>
					<form id="frm_edit" name="frm_edit" action="article_add.jsp?act=doadd<%=params.query("pid,psize,ref")%>" method="post" onsubmit="javascript:return ValidateForm(this);">
					<input type="hidden" name="hdnAssortId" id="hdn_AssortId" value="0" />
					<input type="hidden" name="hdnImgFocus" value="" />
					<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
					<tr>
						<td bgcolor="#CEDDF0" width="90" style="font-weight:bold;width:90px;" align="left">选择具体分类</td>
						<td bgcolor="#ffffff" colspan="5">
						<div id="pnlTreeList" style="padding:0px;margin:0px;"></div>
						<script type="text/javascript">
						var assortPath = '';
						
		             	function getSelectList(id, slt){
		             		var index = slt == null ? -1 : slt.selectedIndex;
		             		var pid = index != -1  ? slt.options[index].value : '';
		             		var node = $('#' + id).get(0);
		             		var hdn = $('#hdn_AssortId').get(0);
		             		hdn.value = pid;
		             		pid = pid.split(',')[0];
		             		
		             		if(pid == '-1'){node.innerHTML = '';return;}
		             		
			             	$.getJSON('article_assort_list.jsp?act=ajax&rnd=' + Math.random(),{parentid:pid},function(data){
								if(data.length == 0){node.innerHTML = ''; return;}
								node.innerHTML = '';
								
								node.innerHTML += "<select class=\"slt\" name='slt_" + pid + "' id='slt_" + pid + "' onchange=\"getSelectList('pnlTreeList_" + pid + "', this)\" ></select>&nbsp;";
								node.innerHTML += "<span id='pnlTreeList_" + pid + "'></span>";
								
								var slt = $('#slt_' + pid).get(0);
								slt.options[0] = new Option('==请选取具体分类==', '-1');
								
								for(var i=0; i < data.length; i++){
									var dataitem = data[i];
									slt.options[i+1] = new Option(dataitem.NAME, dataitem.ROW_ID);
									if(assortPath.indexOf(',' + dataitem.ROW_ID + ',') != -1){
										slt.options[i + 1].selected = 'selected';
										getSelectList('pnlTreeList_'+pid, slt);
									}
								}
							});
		             	}
		             	
		             	getSelectList('pnlTreeList', null);
		             	</script>
						</td>
					</tr>
					<tr>
						<td bgcolor="#CEDDF0" style="font-weight:bold;width:90px;" align="left"><span id="spn_title">标题</span>：</td>
						<td bgcolor="#ffffff"><input type="text" name="title" size="50" class="inputbox1" /></td>
						<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">置顶：</td>
						<td bgcolor="#ffffff" width="160">
						<input type="radio" name="istop" value="0" checked="checked" />否
						<input type="radio" name="istop" value="1" />是
						</td>
						<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">首页：</td>
						<td bgcolor="#ffffff" width="160">
						<input type="radio" name="isindex" value="0" checked="checked" />否
						<input type="radio" name="isindex" value="1" />是
						</td>
					</tr>
					<tr>
						<td bgcolor="#CEDDF0" style="font-weight:bold;width:90px;" align="left">小标题/专题：</td>
						<td bgcolor="#ffffff" colspan="5"><input type="text" name="subject" size="50" class="inputbox1" /></td>
					</tr>
					<tr>
					<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">链接：</td>
					<td bgcolor="#ffffff">
						<input type="text" name="link" size="50" class="inputbox1" />
					</td>
					<td bgcolor="#CEDDF0" style="font-weight:bold;width:90px;" align="left" width="90"><span id="spn_source">来源</span>：</td>
					<td bgcolor="#ffffff" width="180" style="width:150px;"><input type="text" name="source" size="30" class="inputbox1" /></td>
					<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left"><span id="spn_author">作者</span>：</td>
					<td bgcolor="#ffffff"><input type="text" name="author" size="12" class="inputbox1" /></td>
					</tr>
					<tr>
					<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">是否共享：</td>
					<td bgcolor="#ffffff">
						<input type="radio" name="isopen" value="0" checked="checked" />否
						<input type="radio" name="isopen" value="1" />是
					</td>
					<td bgcolor="#CEDDF0" style="font-weight:bold;width:90px;" align="left" width="90">发布时间：</td>
					<td bgcolor="#ffffff" width="180" style="width:150px;"><input type="text" name="pubdate" size="15" class="inputbox1" value="<%=Util.format(Util.getDate(), "yyyy-MM-dd")%>" onclick="laydate({istime:false, format:'YYYY-MM-DD'})"/></td>
					<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">过期时间：</td>
					<td bgcolor="#ffffff"><input type="text" name="indate" size="15" class="inputbox1" onclick="laydate({istime:false, format:'YYYY-MM-DD'})"/></td>
					</tr>
					</table>
					<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
					<tr bgcolor="#ffffff">
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
								<td width="150">
									<div id="divFileUploadButton" class="upload-button"><span id="spnButtonPlaceholder"></span></div>
								</td>
								<td>
									上传附图，可多选。注：红色为封面，点击可重设。
								</td>
								</tr>
							</table>
							<div id="divFileProgressContainer" class="upload-progress"></div>
			            	<div id="div_thumb_list"></div>
						</td>
					</tr>
					</table>
					<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
					<tr bgcolor="#CEDDF0">
						<td height="28" style="font-weight:bold;" align="left">
							内容：
						</td>
					</tr>
					<tr bgcolor="#ffffff">
						<td align="center" style="padding:1px 3px 1px 1px;">
							<textarea name="content" id="txt_content" cols="60" rows="24" style="width:100%;"></textarea>
						</td>
					</tr>
					<%if(operations.indexOf("|1010,setup|") != -1 || "1".equals(current_userid)){%>
					<tr bgcolor="#CEDDF0">
						<td height="28" style="font-weight:bold;" align="left">
							允许IP：
						</td>
					</tr>
					<tr bgcolor="#ffffff">
						<td>
							<textarea name="iplimit" cols="60" rows="4" class="inputarea"></textarea><br/>
							示例：(不填即不作限制！)<br/>
							单个IP：192.168.0.100<br/>
							IP段：192.168.0.100-192.168.0.200
						</td>
					</tr>
					<%}%>
					</table>
					<br/>
					<br/>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align=center>
						<tr>
							<td colspan="3" align="center">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<%if(operations.indexOf("|1003,add|") != -1 || "1".equals(current_userid)){%>
								<input type="submit" class="inputbox2" value=" 提 交 "/>&nbsp;&nbsp;
								<input type="button" class="inputbox2" value=" 保 存 " onclick="form_save(this.form)"/>
								<%} %>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<%if("noaudit".equals(ref)){%>
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_noaudit.jsp?act=nalist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
								<%}else if("audit".equals(ref)){%>
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_audit.jsp?act=alist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
								<%}else if("temp".equals(ref)){%>
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_temp.jsp?act=tlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
								<%}else if("deleted".equals(ref)){%>
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_deleted.jsp?act=dlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
								<%}else if("share".equals(ref)){%>
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_share.jsp?act=slist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
								<%} %>
								<br>
								<br>
							</td>
						</tr>
					</table>
					</form>
					<br/>
					<br/>
				</td>
			</tr>
		</table>
		
		<div id="div_thumb_tpl" style="display:none;">
			<div class="thumb">
				<input type="hidden" name="hdnPhotoPath"/>
				<p class="img">
					<a class="btn btn-delete" href="javascript:void(null);" onclick="removePhoto(this)">删除</a>
		    		<img class="photo" onclick="photoFocus(this)" src="" border="0"/>
				</p>
			</div>
		</div>
		
		<%if(!msg.emptyMessage()){%>
		<script type="text/javascript">
		alert('<%=msg.getMessage()%>');
		</script>
		<%}%>
		<%if(msg.getIsSuccess()){ %>
		<script type="text/javascript">
		<%if("noaudit".equals(ref)){%>
		location.href='article_noaudit.jsp?act=nalist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>';
		<%}else if("audit".equals(ref)){%>
		location.href='article_audit.jsp?act=alist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>';
		<%}else if("temp".equals(ref)){%>
		location.href='article_temp.jsp?act=tlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>';
		<%}else if("deleted".equals(ref)){%>
		location.href='article_deleted.jsp?act=dlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>';
		<%}else if("share".equals(ref)){%>
		location.href='article_share.jsp?act=slist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>';
		<%}%>
		</script>
		<%} %>
	</body>
</html>