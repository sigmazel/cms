<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
String act = params.get("act");
List<DbMap> dbMapList = db.executeDbMapList("SELECT * FROM cms_variable");
DbMap settings = new DbMap();
for(DbMap dbMap : dbMapList) settings.put(dbMap.getString("IDENTITY"), dbMap.getString("VAL"));

if("setup".equals(act)){
	settings.set("sysname", params.getString("sysname"));
	settings.set("adminname", params.getString("adminname"));
	settings.set("badwordenable", params.getInteger("badwordenable") + "");
	settings.set("badword", params.getString("badword"));
	settings.set("auditflow", params.getInteger("auditflow") + "");
	settings.set("uploadpath", params.getString("uploadpath") + "");
	
	settings.set("copyright", params.getString("copyright"));
	settings.set("counter", params.getInteger("counter"));
	settings.set("assort", params.getString("assort"));
	
	DbCommand cmd = db.getSqlStringCommand("UPDATE cms_variable SET VAL = @VAL WHERE `IDENTITY` = 'sysname'");
	cmd.addInParameter("VAL", settings.getString("sysname"));
	cmd.executeNonQuery();
	
	cmd = db.getSqlStringCommand("UPDATE cms_variable SET VAL = @VAL WHERE `IDENTITY` = 'adminname'");
	cmd.addInParameter("VAL", settings.getString("adminname"));
	cmd.executeNonQuery();
	
	cmd = db.getSqlStringCommand("UPDATE cms_variable SET VAL = @VAL WHERE `IDENTITY` = 'badwordenable'");
	cmd.addInParameter("VAL", settings.getInteger("badwordenable") + "");
	cmd.executeNonQuery();
	
	cmd = db.getSqlStringCommand("UPDATE cms_variable SET VAL = @VAL WHERE `IDENTITY` = 'badword'");
	cmd.addInParameter("VAL", settings.getString("badword"));
	cmd.executeNonQuery();
	
	cmd = db.getSqlStringCommand("UPDATE cms_variable SET VAL = @VAL WHERE `IDENTITY` = 'auditflow'");
	cmd.addInParameter("VAL", settings.getInteger("auditflow") + "");
	cmd.executeNonQuery();
	
	cmd = db.getSqlStringCommand("UPDATE cms_variable SET VAL = @VAL WHERE `IDENTITY` = 'uploadpath'");
	cmd.addInParameter("VAL", settings.getString("uploadpath") + "");
	cmd.executeNonQuery();
	
	cmd = db.getSqlStringCommand("UPDATE cms_variable SET VAL = @VAL WHERE `IDENTITY` = 'copyright'");
	cmd.addInParameter("VAL", settings.getString("copyright") + "");
	cmd.executeNonQuery();
	
	cmd = db.getSqlStringCommand("UPDATE cms_variable SET VAL = @VAL WHERE `IDENTITY` = 'counter'");
	cmd.addInParameter("VAL", settings.getString("counter") + "");
	cmd.executeNonQuery();
	
	cmd = db.getSqlStringCommand("UPDATE cms_variable SET VAL = @VAL WHERE `IDENTITY` = 'assort'");
	cmd.addInParameter("VAL", settings.getString("assort") + "");
	cmd.executeNonQuery();
	
	msg.setIsSuccess(true);
	msg.setMessage("您已经成功修改系统参数,重新登录后才能生效！");
	
	addLog("修改系统参数", "4001");
}
%>