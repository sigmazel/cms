<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
String act = params.get("act");
List<DbMap> itemList = new ArrayList<DbMap>();

ServletContext sc = session.getServletContext();
String rootPath = sc.getRealPath("/") + "tpl/";
rootPath = rootPath.replace('\\', '/');

String path = params.get("path");
if(path == null) path = "";

path = path.replaceAll("\\.\\.\\/", "");
path = path.replaceAll("\\.\\.", "");

if(path.length() > 0){
	path = "/".equals(path.substring(0, 1)) ? path.substring(1) : path;
}

params.set("path", path);

List<DbMap> crumbs = new ArrayList<DbMap>();

String[] paths = path.split("\\/");
for(int i = 0; i < paths.length; i++){
	DbMap crumb = new DbMap();
	String tmp = "";
	for(int j = 0; j <= i; j++){
		tmp += paths[j] + "/";
	}
	
	crumb.set("NAME", paths[i]);
	crumb.set("PATH", tmp);
	crumbs.add(crumb);
}

String fileContent = "";

if("list".equals(act)){
       File dir = new File(rootPath + path);
       if(dir.exists()){
           String[] fileList = dir.list();
           
           for(int i = 0; i < fileList.length; i++){
               File fileItem = new File(dir, fileList[i]);
               
               if(fileItem.isDirectory()){
            	   DbMap fileMap = new DbMap();
            	   
                   fileMap.set("NAME", fileItem.getName());
                   fileMap.set("UPDATETIME", fileItem.lastModified());
                   fileMap.set("PATH", fileItem.getPath().replace('\\','/'));
            	   fileMap.set("SIZE", 0);
            	   fileMap.set("TYPE", "DIR");
            	   fileMap.set("EDIT", false);
            	   
                   itemList.add(fileMap);
               }
           }
           
           for(int i = 0; i < fileList.length; i++){
               File fileItem = new File(dir, fileList[i]);
               
               if(!fileItem.isDirectory()){
            	   DbMap fileMap = new DbMap();
                   fileMap.set("NAME", fileItem.getName());
                   fileMap.set("UPDATETIME", fileItem.lastModified());
                   fileMap.set("PATH", fileItem.getPath().replace('\\','/'));

                   DecimalFormat decimalFormat = new DecimalFormat("#,##,###");
                   String size = decimalFormat.format(fileItem.length()) + " byte";
                   fileMap.set("SIZE", size);
                   fileMap.set("TYPE", "FILE");
                   
                   String fileExt = fileItem.getName().substring(fileItem.getName().lastIndexOf(".") + 1);
                   fileExt = fileExt.toLowerCase();
                   
                   fileMap.set("EDIT", ",htm,html,css,js,txt,".indexOf("," + fileExt + ",") != -1);
                   itemList.add(fileMap);
               }
           }
       }
}else if("update".equals(act)){
	File tmpFile = new File(rootPath + path + params.get("file"));
	if(tmpFile.exists()) fileContent = Util.getFileContents(tmpFile);
	else msg.putMessage("文件不存在!!");
}else if("doupdate".equals(act)){
	File tmpFile = new File(rootPath + path + params.get("file"));
	if(tmpFile.exists()){
		FileOutputStream fileOutputStream = new FileOutputStream(tmpFile);
		fileOutputStream.write(params.get("content").getBytes("utf-8"));
		fileOutputStream.close();
		
		addLog("修改文件：" + params.get("file"), "2003");
		redirect("file_list.jsp?act=list&path=" + params.get("path"));
	}else msg.putMessage("文件不存在!!");
}else if("delete".equals(act)){
	File tmpFile = new File(rootPath + path + params.get("file"));
	if(tmpFile.exists()) tmpFile.delete();
	
	addLog("删除文件：" + params.get("file"), "2003");
	redirect("file_list.jsp?act=list&path=" + params.get("path"));
}
%>