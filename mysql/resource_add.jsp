<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_resource.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
		<script>
	     function ValidateForm(theform){
	        if(theform.identity.value==""){
	            alert("标识号不能为空。");
	            theform.identity.focus();
	            return false
	        }
	       if(theform.name.value==""){
	            alert("名称不能为空。");
	            theform.name.focus();
	            return false
	        }
	    }
	</script>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">

		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
					&nbsp;&nbsp;用户管理 
					-&gt; <a href="resource_list.jsp?act=list">权限</a> 
					<%for(int i = 0; i < crumbs.size();i++ ){
					DbMap crumb = (DbMap)crumbs.get(i);
					%>
					-&gt; <a href="resource_list.jsp?act=list&preid=<%=crumb.get("ROW_ID")%>"><%=crumb.get("NAME")%></a>
					<%} %>
					-&gt; 添加
				</td>
			</tr>
			<tr>
				<td height="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td">
					&nbsp;
					<a href="resource_list.jsp?act=list&preid=<%=params.get("preid")%>">列表</a> |&nbsp;
					<font color="#FF0000">添加</font>
				</td>
			</tr>
		</table>
		<br>
		<form action="resource_add.jsp?act=doadd&preid=<%=params.get("preid")%>" method="post" onsubmit="return ValidateForm(this)">
			<table width="95%" align="center" border="0" cellpadding="0"
				cellspacing="0">
				<tr>
					<td align="center" class="title_td1">
						<br>
						<br>
						<br>
						<table width="75%" align="center" border="0" cellpadding="0"
							cellspacing="0">
							<tr>
								<td width="35%" height="30" align="right">
									标识号：
								</td>
								<td width="65%" align="left">
									<input type="text" name="identity" class="inputbox1" size="15">
									<font color="#FF0000">*由字符组成</font>
								</td>
							</tr>
							<tr>
								<td width="35%" height="30" align="right">
									名称：
								</td>
								<td width="65%" align="left">
									<input type="text" name="name" class="inputbox1" size="40">
									<font color="#FF0000">*</font>
								</td>
							</tr>
							<tr>
								<td width="35%" height="30" align="right">
									备注：
								</td>
								<td width="65%" align="left">
									<input type="text" name="comment" class="inputbox1" size="40">
								</td>
							</tr>
							<tr>
								<td width="35%" height="30" align="right">
									相关操作：
								</td>
								<td width="65%" align="left">
									<%for(int i = 0; i < operationList.size(); i++){
										DbMap item = (DbMap)operationList.get(i);
									%>
									<input type="checkbox" name="checkbox" value="<%=item.get("ROW_ID")%>" /><%=item.get("NAME")%>
									<%if((i + 1)% 6 == 0){%><br/><%} %>
									<%} %>
								</td>
							</tr>
						</table>
						<p>
							<%if(operations.indexOf("|3004,add|") != -1 || "1".equals(current_userid)){%>
								<input type="submit" class="inputbox2" value=" 提 交 ">
							<%} %>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" class="inputbox2" value=" 返 回 " onclick="location='resource_list.jsp?act=list&preid=<%=params.get("preid")%>'">
							<br>
							<br>
						</p>
						<p>
							&nbsp;
							<br>
							<br>
						</p>

					</td>
				</tr>

			</table>
		</form>
		<%if(!msg.emptyMessage()){%>
		<script type="text/javascript">
		alert('<%=msg.getMessage()%>');
		</script>
		<%}%>
		<%if(msg.getIsSuccess()){ %>
		<script type="text/javascript">
		location.href='resource_list.jsp?act=list&preid=<%=params.get("preid")%>';
		</script>
		<%} %>
	</body>

</html>
