<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_article.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<title>###</title>
		<style type="text/css">
		<!--
		body {
			background-color: #EDF4FD;
		}
		-->
		</style>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.ext.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
		<script language="javascript" type="text/javascript">
		function orderno(){
	        if (confirm("确认进行批量排序吗？")) {
	        	document.forms[0].action = "article_audit.jsp?act=orderlist&ref=audit<%=params.query("pid,psize")%>";
	            document.forms[0].submit();
	        }
		}
		
	    function del() {
	        if (0 == listcount) {
	            alert("请选择记录。")
	            return;
	        }
	        if (confirm("确认进行删除吗？")) {
	            document.forms[0].submit();
	        }
	    }
	    function delone(urls) {
	        if (confirm("确认进行删除吗？"))	{
				 location = urls;
			}
	    }
	    function move(){
	    	if (0 == listcount){
	            alert("请选择记录。")
	            return;
	        }
	        
	    	$.dialog({width:700, title:'移动分类', href:'article_move.jsp?act=move', remove:false, button: {cancel:'关闭', submit:'移动'}, submit:function(){
	    		if($('#hdn_AssortId').val().length < 32){
	    			alert('请选择具体分类！');
	    			return;
	    		}
	    		
	    		document.forms[0].action = 'article_audit.jsp?act=movelist&ref=audit<%=params.query("pid,psize")%>';
	    		document.forms[0].submit();
	    	}});
	    }
	</script>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0">

		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
					&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a> 
					-&gt; 已审核文章
					<%if(search.size() > 0){%>
	            	 -&gt; <span class="required">查询结果</span>
	            	<%} %>
				</td>
			</tr>
			<tr>
				<td height="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td">
					&nbsp;
					<a href="article_audit.jsp?act=alist"><font color="#FF0000">列表</font> </a> |&nbsp;
					<%if(operations.indexOf("|1006,add|") != -1 || "1".equals(current_userid)){%>
						<a href="article_add.jsp?act=add&ref=noaudit">添加</a> |&nbsp;
					<%} %>
					<%if(operations.indexOf("|1006,delete|") != -1 || "1".equals(current_userid)){%>
						<a href="javascript:del()">删除</a> |&nbsp;
					<%}%>
					<%if(operations.indexOf("|1006,update|") != -1 || "1".equals(current_userid)){%>
						<a href="javascript:move()">移动</a> |&nbsp;
					<%}%>
					<%if(operations.indexOf("|1006,search|") != -1 || "1".equals(current_userid)){%>
						<a href="article_search.jsp?act=search&ref=audit">查询</a> |&nbsp;
					<%}%>
					<%if(operations.indexOf("|1006,order|") != -1 || "1".equals(current_userid)){%>
						<a href="javascript:orderno()">批量排序</a>
					<%}%>
				</td>
			</tr>
		</table>
		<br/>
		<form name="formlist" id="formlist" action="article_audit.jsp?act=deletelist&ref=audit<%=params.query("pid,psize")%>" method="post">
			<input type="hidden" name="hdnAssortId" id="hdn_AssortId" value="0" />
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table width="95%" align="center" border="0" class="TableList"
							cellpadding="0" cellspacing="0">
							<tr align="center">
								<td width="30" height="21" class="biaot">
									选择
								</td>
								<td width="120" class="biaot">
									分类
								</td>
								<%if(operations.indexOf("|1006,order|") != -1 || "1".equals(current_userid)){%>
								<td width="40" class="biaot">
									序号
								</td>
								<%} %>
								<td class="biaot">
									标题
								</td>
								<td width="80" class="biaot">
									发布时间
								</td>
								<td width="80" class="biaot">
									操作人
								</td>
								<td width="100" class="biaot">
									操作时间
								</td>
								<td class="biaot1" width="120">
									操作
								</td>
							</tr>
							<%
							for(int i = 0 ; i < itemList.size(); i++){
								DbMap item = (DbMap)itemList.get(i);
							%>
								<tr bgcolor="#FFFFFF" onMouseOver="overtable(this);"
									id="<%=i + 1%>" onMouseOut="outtable(this);">
									<td height="21" class="biaol" align="center">
										<%if(item.getInteger("STATE") != 2){%>
										<input type="checkbox" name="checkbox" id="c<%=i + 1%>" value="<%=item.get("ROW_ID") %>" onClick="selectbox(this)">
										<%} %>
									</td>
									<td class="biaol" align="center">&nbsp;<%= item.get("NAME")%></td>
									<%if(operations.indexOf("|1006,order|") != -1 || "1".equals(current_userid)){%>
									<td class="biaol" align="center">
										<input type="hidden" name="rowid" value="<%=item.get("ROW_ID") %>" />
										<input type="text" name="orderno" size="4" style="width:35px;" value="<%=item.get("ORDERNO")%>" maxlength="4"/>
									</td>
									<%} %>
									<td class="biaol">
										<a href="article_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=audit">
											<%if(item.getBoolean("ISTOP")){ %>【置顶】<%}%><%if(item.getBoolean("ISINDEX")){ %>【首页】<%}%>
											<%=item.get("TITLE") %>(<%=item.get("DWMC")%>)
										</a>
									</td>
									<td class="biaol" align="center">
										<%=item.get("PUBDATE") %>
									</td>
									<td class="biaol" align="center">
										<%=item.get("EDITER") %>
									</td>
									<td class="biaol" align="center">
										<%=Util.format(item.get("UPDATETIME"), "yyyy-MM-dd h:i") %>
									</td>
									<td class="biaol" align="left">&nbsp;
										<%if(operations.indexOf("|1006,update|") != -1 || "1".equals(current_userid)){%>
											<a href="article_update.jsp?act=update&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=audit">修改</a>
										<%}%>
										<%if(operations.indexOf("|1006,delete|") != -1 || "1".equals(current_userid)){%>
											<a href="javascript:delone('article_audit.jsp?checkbox=<%=item.get("ROW_ID") %>&act=deletelist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=audit')">删除</a>
										<%}%>
										<%if(operations.indexOf("|1006,back|") != -1 || "1".equals(current_userid)){%>
											<a href="article_back.jsp?act=back&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=audit')">退回</a>
										<%}%>
									</td>
								</tr>
							<%
							}
							%>
						</table>
					</td>
				</tr>

			</table>
			<table width="95%" align="center" border="0" cellpadding="0"
				cellspacing="0">
				<tr>
					<td width="30" height="25" valign="bottom">
						<input type="checkbox" name="toggleAll" onClick="ToggleAll(this);">
					</td>
					<td valign="bottom">
						全选&nbsp;&nbsp;
						<%if(operations.indexOf("|1006,order|") != -1 || "1".equals(current_userid)){%>
						(注：序号越大越靠前！)
						<%} %>
					</td>
				</tr>
			</table>
		</form>
		<table width="95%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td height="30" colspan="2">
					<%=pager%>
				</td>
				<td align="right">
					每页：
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=10" <%if(pager.getPageSize() == 10){%>style="font-weight:bold; color:red;"<%}%>>10</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=20" <%if(pager.getPageSize() == 20){%>style="font-weight:bold; color:red;"<%}%>>20</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=30" <%if(pager.getPageSize() == 30){%>style="font-weight:bold; color:red;"<%}%>>30</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=50" <%if(pager.getPageSize() == 50){%>style="font-weight:bold; color:red;"<%}%>>50</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=100" <%if(pager.getPageSize() == 100){%>style="font-weight:bold; color:red;"<%}%>>100</a>
					条
				</td>
			</tr>
		</table>
	</body>
</html>
