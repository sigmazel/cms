<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_article.jsp" %>
<%
String ref = params.get("ref");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<link href="images/datepicker.css" rel="stylesheet" type="text/css" />
		<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
		<script src="laydate/laydate.js" type="text/javascript"></script>
		<style type="text/css">
		.redTip{
			color:red;
		}
		.editTbl td{
			padding:2px 2px 2px 4px;
			height:25px;
		}
		.inputitem{
			width:150px;
		}
		</style>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">	
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a> 
						<%if("nodeal".equals(ref)){%>
						-&gt; 待审核文章 
						-&gt; 查询
						<%}else if("noaudit".equals(ref)){%>
						-&gt; 未通过文章 
						-&gt; 查询
						<%}else if("audit".equals(ref)){%>
						-&gt; 已审核文章 
						-&gt; 查询
						<%}else if("deleted".equals(ref)){%>
						-&gt; 回收站 
						-&gt; 查询
						<%}else if("share".equals(ref)){%>
						-&gt; 共享文章 
						-&gt; 查询
						<%}else{%>
						-&gt; 查询
						<%} %>
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
				<tr>
					<td height="22" class="title_td">
						&nbsp;
						<%if("nodeal".equals(ref)){%>
						<a href="article_nodeal.jsp?act=ndlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">查询</font>
						<%}else if("noaudit".equals(ref)){%>
						<a href="article_noaudit.jsp?act=nalist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">查询</font>
						<%}else if("audit".equals(ref)){%>
						<a href="article_audit.jsp?act=alist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">查询</font>
						<%}else if("deleted".equals(ref)){%>
						<a href="article_deleted.jsp?act=dlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">查询</font>
						<%}else if("share".equals(ref)){%>
						<a href="article_share.jsp?act=slist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">查询</font>
						<%}else{%>
						<font color="#FF0000">查询</font>
						<%} %>
					</td>
				</tr>
			</table>
			<table width="95%" border="0" align="center"
				cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="padding-top:5px">
				<br/>
				<form action="article_search.jsp?act=dosearch<%=params.query("pid,psize,ref")%>" method="post">
				<input type="hidden" name="hdnAssortId" id="hdn_AssortId" value="0" />
					<table width="500" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl">
					<tr bgcolor="#CEDDF0">
						<td width="120" style="font-weight:bold;" align="left" id="tr1_1">
						关键词：
						</td>
						<td>
						<input type="text" name="keyword" class="inputbox1" size="12" />
						</td>
					</tr>
					<tr bgcolor="#ffffff">
					<td style="font-weight:bold;" align="left">所属单位：</td>
					<td>
					<select name="dwid">
					<option value="0">选择具体单位</option>
					<%
					for(int i = 0; i < dwList.size();i++){
						DbMap item = (DbMap)dwList.get(i);
						String space = "";
						String[] paths = item.getString("PATH").split(",");
						if(paths.length > 1){
							for(int j = 1; j < paths.length; j++) space += "&nbsp;&nbsp;";
							item.put("DWMC", space + "&nbsp;└&nbsp;" + item.getString("DWMC"));
						}
					%>
					<option value="<%=item.get("ROW_ID")%>"><%=item.get("DWMC")%></option>
					<%
					}
					%>
					</select>
					</td>
					</tr>
					<tr bgcolor="#CEDDF0">
					<td style="font-weight:bold;" align="left">发布时间：</td>
					<td>
					<input type="text" name="stime" class="inputbox1" size="12" onclick="laydate({istime:false, format:'YYYY-MM-DD'})"/>
					至
					<input type="text" name="etime" class="inputbox1" size="12" onclick="laydate({istime:false, format:'YYYY-MM-DD'})"/>
					</td>
					</tr>
					<%if("deleted".equals(ref)){%>
					<tr bgcolor="#ffffff">
					<td style="font-weight:bold;" align="left">是否审核：</td>
					<td>
					<input type="radio" name="sftype" value="" checked="checked" />所有类型
					<input type="radio" name="sftype" value="-1" />待审核
					<input type="radio" name="sftype" value="0" />不审核
					<input type="radio" name="sftype" value="1" />已审核
					</td>
					</tr>
					<%}%>
					<tr bgcolor="#CEDDF0">
					<td style="font-weight:bold;" align="left" valign="top">所属分类：</td>
					<td>
					<div id="pnlTreeList" style="padding:0px;margin:0px;"></div>
					<script type="text/javascript">
					var assortPath = '';
					
		           	function getSelectList(id, slt){
		           		var index = slt == null ? -1 : slt.selectedIndex;
		           		var pid = index != -1  ? slt.options[index].value : '';
		           		var node = $('#' + id).get(0);
		           		var hdn = $('#hdn_AssortId').get(0);
		           		hdn.value = pid;
		           		pid = pid.split(',')[0];
		           		
		           		if(pid == '-1'){node.innerHTML = '';return;}
		            	$.getJSON('article_assort_list.jsp?act=ajax&rnd=' + Math.random(),{parentid:pid},function(data){
							if(data.length == 0){node.innerHTML = ''; return;}
							node.innerHTML = '';
							
							node.innerHTML += "<div style='padding:2px 0;'><select class=\"slt\" name='slt_" + pid + "' id='slt_" + pid + "' onchange=\"getSelectList('pnlTreeList_" + pid + "', this)\" ></select>&nbsp;";
							node.innerHTML += "<span id='pnlTreeList_" + pid + "'></span></div>";
							
							var slt = $('#slt_' + pid).get(0);
							slt.options[0] = new Option('==请选取具体分类==', '-1');
							
							for(var i=0; i < data.length; i++){
								var dataitem = data[i];
								slt.options[i+1] = new Option(dataitem.NAME, dataitem.ROW_ID);
								if(assortPath.indexOf(',' + dataitem.ROW_ID + ',') != -1){
									slt.options[i + 1].selected = 'selected';
									getSelectList('pnlTreeList_'+pid, slt);
								}
							}
						});
		            }
		            	
		           	getSelectList('pnlTreeList', null);
		           	</script>
					</td>
					</tr>
					<tr bgcolor="#ffffff">
					<td style="font-weight:bold;" align="left">是否置顶：</td>
					<td>
					<input type="radio" name="istop" value="" checked="checked" />不限
					<input type="radio" name="istop" value="0" />否
					<input type="radio" name="istop" value="1" />是
					</td>
					</tr>
					<tr bgcolor="#ffffff">
					<td style="font-weight:bold;" align="left">是否首页：</td>
					<td>
					<input type="radio" name="isindex" value="" checked="checked" />不限
					<input type="radio" name="isindex" value="0" />否
					<input type="radio" name="isindex" value="1" />是
					</td>
					</tr>
					</table>
					<br/>
					<br/>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align=center>
						<tr>
							<td colspan="3" align="center">
								&nbsp;&nbsp;&nbsp;&nbsp;
								<%if(operations.indexOf("|1004,search|") != -1 || operations.indexOf("|1005,search|") != -1 || operations.indexOf("|1006,search|") != -1 || operations.indexOf("|1007,search|") != -1 || operations.indexOf("|1008,search|") != -1 || "1".equals(current_userid)){%>
								<input type="submit" class="inputbox2" value=" 提 交 ">
								<%} %>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<%if("nodeal".equals(ref)){%>
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_nodeal.jsp?act=ndlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
								<%}else if("noaudit".equals(ref)){%>
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_noaudit.jsp?act=nalist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
								<%}else if("audit".equals(ref)){%>
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_audit.jsp?act=alist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
								<%}else if("deleted".equals(ref)){%>
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_deleted.jsp?act=dlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
								<%}else if("share".equals(ref)){%>
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_share.jsp?act=slist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
								<%} %>
								<br>
								<br>
							</td>
						</tr>
					</table>
					</form>
					<br/>
					<br/>
				</td>
			</tr>
		</table>
	</body>
</html>