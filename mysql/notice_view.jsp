<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_notice.jsp" %>
<%
String ref = params.get("ref");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<style type="text/css">
		.redTip{
			color:red;
		}
		.editTbl td{
			padding:2px 2px 2px 4px;
			height:25px;
		}
		.inputitem{
			width:150px;
		}
		</style>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">	
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a></a> 
						-&gt; 通知公告 
						-&gt; 查看
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
				<tr>
					<td height="22" class="title_td">
						&nbsp;
						<%if("index".equals(ref)){%>
						<a href="article_index.jsp?act=ilist">列表</a> |&nbsp;
						<font color="#FF0000">查看</font>
						<%}else{%>
						<a href="notice.jsp?act=list&pid=<%=params.get("pid")%>">列表</a> |&nbsp;
						<font color="#FF0000">查看</font>
						<%} %>
					</td>
				</tr>
			</table>
			<table width="95%" border="0" align="center"
				cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="padding-top:5px">
		<br/>
		<form action="#" method="post">
			<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
			<tr>
				<td bgcolor="#CEDDF0" width="90" style="font-weight:bold;width:90px;" align="left">标题：</td>
				<td bgcolor="#ffffff" colspan="5"><%=entity.get("TITLE")%></td>
			</tr>
			</table>
			<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
			<tr bgcolor="#CEDDF0">
				<td height="28" style="font-weight:bold;" align="left">
					内容：
				</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="left">
				<%=entity.get("CONTENT")%>
				</td>
			</tr>
			</table>
			<br/>
			<br/>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align=center>
				<tr>
					<td colspan="3" align="center">
						&nbsp;&nbsp;&nbsp;&nbsp;
						<%if("index".equals(ref)){%>
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_index.jsp?act=ilist'">
						<%}else{%>
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='notice.jsp?act=list&pid=<%=params.get("pid")%>'">
						<%} %>
						<br>
						<br>
					</td>
				</tr>
			</table>
			</form>
			<br/>
			<br/>
			</td>
			</tr>
		</table>
	</body>
</html>