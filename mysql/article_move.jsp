<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_article.jsp" %>

<div id="pnlTreeList" style="padding:20px 25px;margin:0px;"></div>
<script type="text/javascript">
var assortPath = '';

function getSelectList(id, slt){
	var index = slt == null ? -1 : slt.selectedIndex;
	var pid = index != -1  ? slt.options[index].value : '';
	var node = $('#' + id).get(0);
	var hdn = $('#hdn_AssortId').get(0);
	hdn.value = pid;
	pid = pid.split(',')[0];
	
	if(pid == '-1'){node.innerHTML = '';return;}
	
	$.getJSON('article_assort_list.jsp?act=ajax&rnd=' + Math.random(),{parentid:pid},function(data){
		if(data.length == 0){node.innerHTML = ''; return;}
		node.innerHTML = '';
		
		node.innerHTML += "<select class=\"slt\" name='slt_" + pid + "' id='slt_" + pid + "' onchange=\"getSelectList('pnlTreeList_" + pid + "', this)\" ></select>&nbsp;";
		node.innerHTML += "<span id='pnlTreeList_" + pid + "'></span>";
		
		var slt = $('#slt_' + pid).get(0);
		slt.options[0] = new Option('==请选取具体分类==', '-1');
		
		for(var i=0; i < data.length; i++){
			var dataitem = data[i];
			slt.options[i+1] = new Option(dataitem.NAME, dataitem.ROW_ID);
			if(assortPath.indexOf(',' + dataitem.ROW_ID + ',') != -1){
				slt.options[i + 1].selected = 'selected';
				getSelectList('pnlTreeList_'+pid, slt);
			}
		}
	});
}

getSelectList('pnlTreeList', null);
</script>
