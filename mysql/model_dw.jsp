<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
String act = params.get("act");
List<DbMap> itemList = new ArrayList<DbMap>();
DbMap entity = null;
DbMap parent = null;

String predwid = params.get("predwid");
List<DbMap> crumbs = new ArrayList<DbMap>();
crumbs.clear();

List<DbMap> assortList = new ArrayList<DbMap>();
String dwAssortIdString = "";

if ("1".equals(current.getString("ROW_ID"))) predwid = "0";
else{
	if(Util.empty(predwid) || "0".equals(predwid)) predwid = current.getString("DWID");
	
	parent = (DbMap)db.executeDbMap("SELECT * FROM cms_dw WHERE ROW_ID = ?", predwid);
	crumbs = db.executeDbMapList("SELECT * FROM cms_dw WHERE INSTR('" + parent.getString("PATH")+ "', PATH) = 1 ORDER BY DWBH ASC");
}

Parameters search = (Parameters)request.getSession().getAttribute("dw_search");
if(search == null) search = new Parameters();
String where = "";

if(!Util.empty(search.get("dwbh"))) where += " AND DWBH like '%" + search.get("dwbh") + "%'";
if(!Util.empty(search.get("dwmc"))) where += " AND DWMC like '%" + search.get("dwmc") + "%'";

if("list".equals(act)){
	List<?> list = db.executeDbMapList("SELECT * FROM cms_dw  WHERE STATE = 0 AND PREDWID = '" + predwid + "' " + where + " ORDER BY DWBH ASC");
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT NAME FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		itemList.add(item);
	}
}else if ("deletelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT * FROM cms_dw WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("DWMC");
			db.executeNonQuery("DELETE FROM cms_dw WHERE ROW_ID = ?", checkbox);
			db.executeNonQuery("DELETE FROM cms_user WHERE DWID = ?", checkbox);
			if(!"0".equals(tempDbMap.getString("PREDWID"))){
				db.executeNonQuery("UPDATE cms_dw SET CHILDREN = CHILDREN - 1 WHERE ROW_ID = ?", tempDbMap.getString("PREDWID"));
			}
		}
	}
	
	addLog("删除单位：" + itemMc, "3002");
	redirect("dw_list.jsp?act=list&predwid=" + params.get("predwid"));
}else if ("search".equals(act)){
	session.removeAttribute("dw_search");
}else if ("dosearch".equals(act)){
	Parameters tempDbMap = new Parameters();
	if(!Util.empty(params.get("dwbh"))) tempDbMap.put("dwbh", params.get("dwbh"));
	if(!Util.empty(params.get("dwmc"))) tempDbMap.put("dwmc", params.get("dwmc"));
	session.setAttribute("dw_search", tempDbMap);
	redirect("dw_list.jsp?act=list&predw=" + params.get("predwid"));
}else if ("update".equals(act)){
	String rowid = params.get("rowid");
	entity = db.executeDbMap("SELECT * FROM cms_dw WHERE ROW_ID = ?", rowid);
}else if ("doupdate".equals(act)){
	String rowid = params.get("rowid");
	String dwmc = params.get("dwmc");
	String dwbh = params.get("dwbh");
	String dwalias = params.get("dwalias");
	String content = params.get("content");
	
	DbCommand cmd = db.getSqlStringCommand("SELECT * FROM cms_dw WHERE DWBH = @DWBH AND ROW_ID <> @ROW_ID");
	cmd.addInParameter("DWBH", dwbh);
	cmd.addInParameter("ROW_ID", rowid);
	DbMap tempMap = cmd.executeDbMap();
	
	if(tempMap != null) msg.putMessage("系统已经存在此编号单位!!");
	else {
		cmd = db.getSqlStringCommand("UPDATE cms_dw SET DWMC=@DWMC,DWBH=@DWBH,DWALIAS=@DWALIAS,CONTENT=@CONTENT," +
				"UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME WHERE ROW_ID =@ROW_ID");
		cmd.addInParameter("ROW_ID", rowid);
		cmd.addInParameter("DWMC", dwmc);
		cmd.addInParameter("DWBH", dwbh);
		cmd.addInParameter("DWALIAS", dwalias);
		cmd.addInParameter("CONTENT", content);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.executeNonQuery();
		
		addLog("修改单位：" + dwmc , "3002");
		
		db.executeNonQuery("UPDATE cms_article SET DWBH = '" + dwbh + "' WHERE DWID = '" + rowid + "'");
		
		msg.setIsSuccess(true);
	}
	
	entity = db.executeDbMap("SELECT * FROM cms_dw WHERE ROW_ID = ?", rowid);
}else if ("doadd".equals(act)){
	String dwmc = params.get("dwmc");
	String dwbh = params.get("dwbh");
	String dwalias = params.get("dwalias");
	String content = params.get("content");
	
	DbMap tempMap = db.executeDbMap("SELECT * FROM cms_dw WHERE DWBH = ?", dwbh);
	if(tempMap != null) msg.putMessage("系统已经存在此编号单位!!");
	else {
		String uuid = Util.getUUID();
		
		DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_dw(ROW_ID,DWBH,DWMC,PREDWID,UPDATEID,UPDATETIME,STATE,PATH,CHILDREN,DWALIAS,CONTENT) VALUES(" +
				"@ROW_ID,@DWBH,@DWMC,@PREDWID,@UPDATEID,@UPDATETIME,@STATE,@PATH,@CHILDREN,@DWALIAS,@CONTENT)");
		cmd.addInParameter("ROW_ID", uuid);
		cmd.addInParameter("DWBH", dwbh);
		cmd.addInParameter("DWMC", dwmc);
		cmd.addInParameter("PREDWID", parent == null ? "0" : parent.getString("ROW_ID"));
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.addInParameter("STATE", 0);
		cmd.addInParameter("PATH", parent == null ? uuid + "," : parent.getString("PATH") + uuid + ",");
		cmd.addInParameter("CHILDREN", 0);
		cmd.addInParameter("DWALIAS", dwalias);
		cmd.addInParameter("CONTENT", content);
		
		cmd.executeNonQuery();
		
		if(parent != null){
			db.executeNonQuery("UPDATE cms_dw SET CHILDREN = CHILDREN + 1 WHERE ROW_ID = ?", parent.getString("ROW_ID"));
		}
		
		addLog("添加单位：" + dwmc , "3002");
		
		msg.setIsSuccess(true);
	}
}else if ("assort".equals(act)){
	String dwid = params.get("dwid");
	entity = db.executeDbMap("SELECT * FROM cms_dw WHERE ROW_ID = ?", dwid);
	
	List<DbMap> dwAssortList = db.executeDbMapList("SELECT * FROM cms_dw_assort WHERE DWID = ?", dwid);
	for(DbMap dbMap : dwAssortList) dwAssortIdString += "," + dbMap.getString("ASSORTID") + ",";
	
	assortList = db.executeDbMapList("SELECT * FROM cms_assort ORDER BY LENGTH(NO) ASC, NO ASC");
	for(DbMap dbMap : assortList){
		dbMap.put("NAME", dbMap.getString("NAME").replaceAll("\"", "“"));
		dbMap.put("CHECKED", dwAssortIdString.indexOf("," + dbMap.getString("ROW_ID") + ",") != -1 ? "true" : "false");
	}
}else if ("doassort".equals(act)){
	String dwid = params.get("dwid");
	entity = db.executeDbMap("SELECT * FROM cms_dw WHERE ROW_ID = ?", dwid);
	
	db.executeNonQuery("DELETE FROM cms_dw_assort WHERE DWID = ?", dwid);
	
	String[] assortIds = params.get("dwAssortIdString").split(",");
	for(int i = 0; i < assortIds.length; i++){
		if(!Util.isUUID(assortIds[i])) continue;
		
		DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_dw_assort(ROW_ID, DWID, ASSORTID) VALUES(@ROW_ID, @DWID, @ASSORTID)");
		cmd.addInParameter("ROW_ID", Util.getUUID());
		cmd.addInParameter("DWID", dwid);
		cmd.addInParameter("ASSORTID", assortIds[i]);
		cmd.executeNonQuery();
	}
	
	addLog("设置单位的分类：" + entity.getString("DWMC") , "3002");
	
	msg.setIsSuccess(true);
}
%>