<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_message.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<style type="text/css">
		.redTip{
			color:red;
		}
		.editTbl td{
			padding:2px 2px 2px 4px;
			height:25px;
		}
		.inputitem{
			width:150px;
		}
		</style>
		<script type="text/javascript" src="<%=path%>/editor/kindeditor-min.js"></script>
		<script type="text/javascript" src="<%=path%>/editor/lang/zh_CN.js"></script>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a>
						-&gt; 留言 
						-&gt; 查看
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
				<tr>
					<td height="22" class="title_td">
						&nbsp;
						<a href="message.jsp?act=list&pid=<%=params.get("pid")%>">列表</a> |&nbsp;
						<font color="#FF0000">查看</font>
					</td>
				</tr>
			</table>
			<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="padding-top:5px">
					<br/>
					<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
					<tr>
						<td bgcolor="#CEDDF0" width="90" style="font-weight:bold;width:90px;" align="left">标题：</td>
						<td bgcolor="#ffffff" colspan="5"><%=entity.get("TITLE") %></td>
					</tr>
					<tr>
						<td bgcolor="#CEDDF0" width="90" style="font-weight:bold;width:90px;" align="left">相关文章：</td>
						<td bgcolor="#ffffff" colspan="3"><%=entity.get("ARTICLE") %></td>
						<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">是否公开：</td>
						<td bgcolor="#ffffff">
							<%if("0".equals(entity.getString("ISOPEN"))){%>否<%}%>
							<%if("1".equals(entity.getString("ISOPEN"))){%>是<%}%>
						</td>
					</tr>
					<tr>
					<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">联系人：</td>
					<td bgcolor="#ffffff"><%=entity.get("WRITER") %></td>
					<td bgcolor="#CEDDF0" style="font-weight:bold;width:90px;" align="left" width="90">电话：</td>
					<td bgcolor="#ffffff" width="180" style="width:150px;"><%=entity.get("PHONE") %></td>
					<td bgcolor="#CEDDF0" style="font-weight:bold;width:90px;" align="left">手机号码：</td>
					<td bgcolor="#ffffff"><%=entity.get("MOBILE") %></td>
					</tr>
					</table>
					<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
					<tr bgcolor="#CEDDF0">
						<td height="28" style="font-weight:bold;" align="left">
							内容：
						</td>
					</tr>
					<tr bgcolor="#ffffff">
						<td align="left">
						<%=entity.get("CONTENT") %>
						</td>
					</tr>
					<tr bgcolor="#CEDDF0">
						<td height="28" style="font-weight:bold;" align="left">
							回复：
						</td>
					</tr>
					<tr bgcolor="#ffffff">
						<td align="left">
						<%=Util.nlToBr(entity.getString("REPLY"))%>
						</td>
					</tr>
					</table>
					<br/>
					<br/>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align=center>
						<tr>
							<td colspan="3" align="center">
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='message.jsp?act=list&pid=<%=params.get("pid")%>'">
								<br>
								<br>
							</td>
						</tr>
					</table>
					<br/>
					<br/>
				</td>
			</tr>
		</table>
	</body>
</html>