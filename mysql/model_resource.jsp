<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
String act = params.get("act");
List<DbMap> itemList = new ArrayList<DbMap>();
List<DbMap> operationList = new ArrayList<DbMap>();
Pager pager = new Pager(params.get("pid"), 100);
DbMap entity = null;
DbMap parent = null;
String operationString = "|";

String preid = params.get("preid");
List<DbMap> crumbs = new ArrayList<DbMap>();
crumbs.clear();

if (Util.empty(preid) || "0".equals(preid)) preid = "0";
else{
	parent = (DbMap)db.executeDbMap("SELECT * FROM cms_resource WHERE ROW_ID = ?", preid);
	crumbs = db.executeDbMapList("SELECT * FROM cms_resource WHERE INSTR('" + parent.getString("PATH")+ "', PATH) = 1 ORDER BY `IDENTITY` ASC ");
}

pager.setQueryString("act=" + params.get("act") + "&preid=" + params.get("preid"));

if("list".equals(act)){
	List<?> list = db.executeDbMapPager("SELECT * FROM cms_resource WHERE PARENTID = '" + preid + "' ORDER BY `IDENTITY` ASC", pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT NAME FROM cms_user WHERE ROW_ID = ?", item.get("EDITUSER"));
		if("1".equals(item.getString("EDITUSER"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		itemList.add(item);
	}
}else if ("deletelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT * FROM cms_resource WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("NAME");
			db.executeNonQuery("DELETE FROM cms_resource WHERE ROW_ID = ?", checkbox);
			if(!"0".equals(tempDbMap.getString("PARENTID"))){
				db.executeNonQuery("UPDATE cms_resource SET CHILDREN = CHILDREN - 1 WHERE ROW_ID = ?", tempDbMap.getString("PARENTID"));
			}
		}
	}
	
	addLog("删除权限：" + itemMc, "3004");
	redirect("resource_list.jsp?act=list&preid=" + params.get("preid"));
}else if ("update".equals(act)){
	operationList = db.executeDbMapList("SELECT * FROM cms_operation ORDER BY SORTNO ASC");
	String rowid = params.get("rowid");
	entity = db.executeDbMap("SELECT * FROM cms_resource WHERE ROW_ID = ?", rowid);
	
	List operations = db.executeDbMapList("SELECT * FROM cms_privilege WHERE RESOURCEID = ?", rowid);
	for(int i = 0 ; i < operations.size();i++){
		DbMap item = (DbMap)operations.get(i);
		operationString += item.getString("OPERATIONID") + "|";
	}
}else if ("doupdate".equals(act)){
	operationList = db.executeDbMapList("SELECT * FROM cms_operation ORDER BY SORTNO ASC");
	
	String rowid = params.get("rowid");
	String name = params.get("name");
	String identity = params.get("identity");
	String comment = params.get("comment");
	
	DbCommand cmd = db.getSqlStringCommand("SELECT * FROM cms_resource WHERE `IDENTITY` = @IDENTITY AND ROW_ID <> @ROW_ID");
	cmd.addInParameter("IDENTITY", identity);
	cmd.addInParameter("ROW_ID", rowid);
	DbMap tempMap = cmd.executeDbMap();
	
	if(tempMap != null) msg.putMessage("系统已经存在此标识号权限!!");
	else {
		cmd = db.getSqlStringCommand("UPDATE cms_resource SET NAME=@NAME,`IDENTITY`=@IDENTITY,COMMENT=@COMMENT," +
				"EDITUSER=@EDITUSER,EDITTIME=@EDITTIME WHERE ROW_ID =@ROW_ID");
		cmd.addInParameter("ROW_ID", rowid);
		cmd.addInParameter("NAME", name);
		cmd.addInParameter("IDENTITY", identity);
		cmd.addInParameter("COMMENT", comment);
		cmd.addInParameter("EDITUSER", current.get("ROW_ID"));
		cmd.addInParameter("EDITTIME", Util.getDate());
		
		cmd.executeNonQuery();
		
		db.executeNonQuery("DELETE FROM cms_privilege WHERE RESOURCEID = ?", rowid);
		String[] checkboxs = request.getParameterValues("checkbox");
		if(checkboxs != null){
			for(String checkbox : checkboxs){
				cmd = db.getSqlStringCommand("INSERT INTO cms_privilege(ROW_ID,RESOURCEID,OPERATIONID) VALUES(@ROW_ID,@RESOURCEID,@OPERATIONID)");
				cmd.addInParameter("ROW_ID", Util.getUUID());
				cmd.addInParameter("RESOURCEID", rowid);
				cmd.addInParameter("OPERATIONID", checkbox);
				cmd.executeNonQuery();
			}
		}
		
		addLog("修改权限：" + name , "3004");
		
		msg.setIsSuccess(true);
	}
	
	entity = db.executeDbMap("SELECT * FROM cms_resource WHERE ROW_ID = ?", rowid);
}else if ("add".equals(act)){
	operationList = db.executeDbMapList("SELECT * FROM cms_operation ORDER BY SORTNO ASC");
}else if ("doadd".equals(act)){
	operationList = db.executeDbMapList("SELECT * FROM cms_operation ORDER BY SORTNO ASC");
	String name = params.get("name");
	String identity = params.get("identity");
	String comment = params.get("comment");
	
	DbMap tempMap = db.executeDbMap("SELECT * FROM cms_resource WHERE `IDENTITY` = ?", identity);
	if(tempMap != null) msg.putMessage("系统已经存在此标识号权限!!");
	else {
		String uuid = Util.getUUID();
		
		DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_resource(ROW_ID,NAME,`IDENTITY`,PARENTID,EDITUSER,EDITTIME,COMMENT,PATH,CHILDREN,CREATETIME,SYSID) VALUES(" +
				"@ROW_ID,@NAME,@IDENTITY,@PARENTID,@EDITUSER,@EDITTIME,@COMMENT,@PATH,@CHILDREN,@CREATETIME,'')");
		cmd.addInParameter("ROW_ID", uuid);
		cmd.addInParameter("NAME", name);
		cmd.addInParameter("IDENTITY", identity);
		cmd.addInParameter("PARENTID", parent == null ? "0" : parent.getString("ROW_ID"));
		cmd.addInParameter("EDITUSER", current.get("ROW_ID"));
		cmd.addInParameter("EDITTIME", Util.getDate());
		cmd.addInParameter("COMMENT", comment);
		cmd.addInParameter("PATH", parent == null ? uuid + "," : parent.getString("PATH") + uuid + ",");
		cmd.addInParameter("CHILDREN", 0);
		cmd.addInParameter("CREATETIME", Util.getDate());
		
		cmd.executeNonQuery();
		
		if(parent != null){
			db.executeNonQuery("UPDATE cms_resource SET CHILDREN = CHILDREN + 1 WHERE ROW_ID = ?", parent.getString("ROW_ID"));
		}
		
		String[] checkboxs = request.getParameterValues("checkbox");
		if(checkboxs != null){
			for(String checkbox : checkboxs){
				cmd = db.getSqlStringCommand("INSERT INTO cms_privilege(ROW_ID,RESOURCEID,OPERATIONID) VALUES(@ROW_ID,@RESOURCEID,@OPERATIONID)");
				cmd.addInParameter("ROW_ID", Util.getUUID());
				cmd.addInParameter("RESOURCEID", uuid);
				cmd.addInParameter("OPERATIONID", checkbox);
				cmd.executeNonQuery();
			}
		}
		
		addLog("添加权限：" + name , "3004");
		
		msg.setIsSuccess(true);
	}
}
%>