<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.fairy.lib.*" %>
<%@ page import="java.util.*,java.io.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="com.google.gson.*"%>
<%@ page import="java.text.*"%>

<%@ page import="java.awt.Image"%>
<%@ page import="java.awt.image.BufferedImage"%>

<%@ page import="com.sun.image.codec.jpeg.JPEGCodec"%>
<%@ page import="com.sun.image.codec.jpeg.JPEGImageEncoder"%>

<%@ page import="javax.imageio.ImageIO"%>

<%@ include file="init.jsp" %>
<%
DbMap user = db.executeDbMap("SELECT * FROM cms_user WHERE ROW_ID = ?", request.getParameter("crypt"));
if(user == null) {
	out.println("未登录，不能上传文件。");
	return;
}

//文件保存目录路径
String savePath = pageContext.getServletContext().getRealPath("/") + "attached/";

//文件保存目录URL
String saveUrl  = request.getContextPath() + "/attached/";

//定义允许上传的文件扩展名
HashMap<String, String> extMap = new HashMap<String, String>();
extMap.put("image", "jpg,jpeg");

//最大文件大小
long maxSize = 2048000;

response.setContentType("text/html; charset=UTF-8");

if(!ServletFileUpload.isMultipartContent(request)){
	out.println("请选择文件。");
	return;
}
//检查目录
File uploadDir = new File(savePath);
if(!uploadDir.isDirectory()){
	out.println("上传目录不存在。");
	return;
}

//检查目录写权限
if(!uploadDir.canWrite()){
	out.println("上传目录没有写权限。");
	return;
}

String dirName = request.getParameter("dir");
if (dirName == null) {
	dirName = "image";
}
if(!extMap.containsKey(dirName)){
	out.println("目录名不正确。");
	return;
}
//创建文件夹
savePath += dirName + "/";
saveUrl += dirName + "/";
File saveDirFile = new File(savePath);
if (!saveDirFile.exists()) {
	saveDirFile.mkdirs();
}
SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
String ymd = sdf.format(new Date());
savePath += ymd + "/";
saveUrl += ymd + "/";
File dirFile = new File(savePath);
if (!dirFile.exists()) {
	dirFile.mkdirs();
}

FileItemFactory factory = new DiskFileItemFactory();
ServletFileUpload upload = new ServletFileUpload(factory);
upload.setHeaderEncoding("UTF-8");
List items = upload.parseRequest(request);

Iterator itr = items.iterator();
while (itr.hasNext()) {
	FileItem item = (FileItem) itr.next();
	String fileName = item.getName();
	long fileSize = item.getSize();
	if (!item.isFormField()) {
		//检查文件大小
		if(item.getSize() > maxSize){
			out.println("上传文件大小超过限制。");
			return;
		}
		
		String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
		
		//检查扩展名
		if("jpg,jpeg,bmp,gif".indexOf(fileExt) == -1){
			out.println("上传文件扩展名是不允许的扩展名。\n只允许jpg、bmp、gif格式。");
			return;
		}

		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000);
		String newRealFileName = newFileName + ".n." + fileExt;
		
		newFileName = newFileName + "." + fileExt;
		
		try{
			File uploadedFile = new File(savePath, newFileName);
			item.write(uploadedFile);
			
			boolean successed = true;
			float formatWidth = 640;
			float formatHeight = 0;
			
			try{
				
				File _file = new File(savePath + newFileName);
	            BufferedImage bufferedImage = ImageIO.read(_file);
	            
	            int imageWidth = bufferedImage.getWidth(null);
	            int imageHeight = bufferedImage.getHeight(null);
	            
                if(imageWidth > formatWidth){
                	formatHeight =  (int) imageHeight * (formatWidth / imageWidth);
                	
                	BufferedImage thumbImage = new BufferedImage((int)formatWidth, (int)formatHeight, BufferedImage.TYPE_INT_RGB);
                	thumbImage.getGraphics().drawImage(bufferedImage, 0, 0, (int)formatWidth, (int)formatHeight, null);
                    FileOutputStream fileOutputStream = new FileOutputStream(savePath + newRealFileName);
                    JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(fileOutputStream);
                    encoder.encode(thumbImage);
                    fileOutputStream.close();
                }else{
                	successed = false;
                }
	            
	    	}catch(Exception ex){
	    		successed = false;
	    	}
			
			out.println("FILEID:" + saveUrl + (successed ? newRealFileName : newFileName));
			return;
		}catch(Exception e){
			out.println("上传文件失败。");
			return;
		}
	}
}
%>