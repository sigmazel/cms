<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_article.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<title>###</title>
		<style type="text/css">
		<!--
		body {
			background-color: #EDF4FD;
		}
		-->
		</style>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
		<script language="javascript" type="text/javascript">
	    function del() {
	        if (0 == listcount) {
	            alert("请选择记录。")
	            return;
	        }
	        if (confirm("确认进行彻底删除吗？")) {
	            document.forms[0].submit();
	        }
	    }
	    function delone(urls) {
	        if (confirm("确认进行彻底删除吗？"))	{
				 location = urls;
			}
	    }
	    
	    function restore() {
	        if (0 == listcount) {
	            alert("请选择记录。")
	            return;
	        }
	        if (confirm("确认还原所选信息吗？")) {
	        	document.forms[0].action = "article_deleted.jsp?act=restorelist<%=params.query("pid,psize")%>";
	            document.forms[0].submit();
	        }
	    }
	    
	    function restoreone(urls) {
	        if (confirm("确认还原此信息吗？"))	{
				 location = urls;
			}
	    }
	    
	</script>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0">

		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
					&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a> 
					-&gt; 回收站
					<%if(search.size() > 0){%>
	            	 -&gt; <span class="required">查询结果</span>
	            	<%} %>
				</td>
			</tr>
			<tr>
				<td height="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td">
					&nbsp;
					<a href="article_deleted.jsp?act=dlist"><font color="#FF0000">列表</font> </a> |&nbsp;
					<%if(operations.indexOf("|1008,delete|") != -1 || "1".equals(current_userid)){%>
						<a href="javascript:del()">彻底删除</a> |&nbsp;
					<%}%>
					<%if(operations.indexOf("|1008,restore|") != -1 || "1".equals(current_userid)){%>
						<a href="javascript:restore()">还原</a> |&nbsp;
					<%}%>
					<%if(operations.indexOf("|1008,search|") != -1 || "1".equals(current_userid)){%>
						<a href="article_search.jsp?act=search&ref=deleted">查询</a>
					<%}%>

				</td>
			</tr>
		</table>
		<br/>
		<form name="formlist" id="formlist" action="article_deleted.jsp?act=deletedlist<%=params.query("pid,psize")%>" method="post">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table width="95%" align="center" border="0" class="TableList"
							cellpadding="0" cellspacing="0">
							<tr align="center">
								<td width="30" height="21" class="biaot">
									选择
								</td>
								<td width="120" class="biaot">
									分类
								</td>
								<td class="biaot">
									标题
								</td>
								<td width="80" class="biaot">
									还原时间
								</td>
								<td width="80" class="biaot">
									操作人
								</td>
								<td width="100" class="biaot">
									操作时间
								</td>
								<td class="biaot1" width="120">
									操作
								</td>
							</tr>
							<%
							for(int i = 0 ; i < itemList.size(); i++){
								DbMap item = (DbMap)itemList.get(i);
							%>
								<tr bgcolor="#FFFFFF" onMouseOver="overtable(this);"
									id="<%=i + 1%>" onMouseOut="outtable(this);">
									<td height="21" class="biaol" align="center">
										<input type="checkbox" name="checkbox" id="c<%=i + 1%>"
											value="<%=item.get("ROW_ID") %>" onClick="selectbox(this)">
									</td>
									<td class="biaol" align="center">&nbsp;<%= item.get("NAME")%>
									</td>
									<td class="biaol">
									<a href="article_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%><%=params.query("pid,psize")%>&ref=deleted"><%=item.get("TITLE") %>（<%=item.get("DWMC")%>）</a>
									</td>
									<td class="biaol" align="center">
										<%=item.get("PUBDATE") %>
									</td>
									<td class="biaol" align="center">
										<%=item.get("EDITER") %>
									</td>
									<td class="biaol" align="center">
										<%=Util.format(item.get("UPDATETIME"), "yyyy-MM-dd h:i") %>
									</td>
									<td class="biaol" align="left">&nbsp;
										<%if(operations.indexOf("|1008,restore|") != -1 || "1".equals(current_userid)){%>
											<a href="javascript:restoreone('article_deleted.jsp?checkbox=<%=item.get("ROW_ID") %>&act=restorelist<%=params.query("pid,psize")%>&ref=deleted')">还原</a>
										<%}%>
										<%if(operations.indexOf("|1008,delete|") != -1 || "1".equals(current_userid)){%>
											<a href="javascript:delone('article_deleted.jsp?checkbox=<%=item.get("ROW_ID") %>&act=deletedlist<%=params.query("pid,psize")%>&ref=deleted')">彻底删除</a>
										<%}%>
									</td>
								</tr>
							<%
							}
							%>
						</table>
					</td>
				</tr>

			</table>
			<table width="95%" align="center" border="0" cellpadding="0"
				cellspacing="0">
				<tr>
					<td width="4%" height="25" valign="bottom">
						<input type="checkbox" name="toggleAll" onClick="ToggleAll(this);">
					</td>
					<td width="96%" valign="bottom">
						全选
					</td>
				</tr>
			</table>
		</form>
		<table width="95%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td height="30" colspan="2">
					<%=pager%>
				</td>
				<td align="right">
					每页：
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=10" <%if(pager.getPageSize() == 10){%>style="font-weight:bold; color:red;"<%}%>>10</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=20" <%if(pager.getPageSize() == 20){%>style="font-weight:bold; color:red;"<%}%>>20</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=30" <%if(pager.getPageSize() == 30){%>style="font-weight:bold; color:red;"<%}%>>30</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=50" <%if(pager.getPageSize() == 50){%>style="font-weight:bold; color:red;"<%}%>>50</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=100" <%if(pager.getPageSize() == 100){%>style="font-weight:bold; color:red;"<%}%>>100</a>
					条
				</td>
			</tr>
		</table>
	</body>
</html>
