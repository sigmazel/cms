<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
List<DbMap> dbMapList = db.executeDbMapList("SELECT * FROM cms_variable");
DbMap settings = new DbMap();
for(DbMap dbMap : dbMapList) settings.put(dbMap.getString("IDENTITY"), dbMap.getString("VAL"));
		
String assort_message = Util.empty(settings.get("assort")) ? "message" : settings.getString("assort");
		
String act = params.get("act");
List<DbMap> itemList = new ArrayList<DbMap>();
List<DbMap> articleList = new ArrayList<DbMap>();

Pager pager = new Pager(params.get("pid"), Util.getInteger(params.get("psize")) < 10 ? 10 : Util.getInteger(params.get("psize")));
DbMap entity = null;

pager.setQueryString("act=" + params.get("act"));

Parameters search = null;

if("list".equals(act)) search = (Parameters) session.getAttribute("message_search");

String where = "";
if(search == null) search = new Parameters();

if (!Util.empty(search.get("keyword"))) where += " and a.TITLE + a.WRITER + a.PHONE + a.MOBILE like '%" + search.get("keyword") + "%'";
if (!Util.empty(search.get("kstime"))) where += " and a.CREATETIME >= '" + search.get("kstime") + "'";
if (!Util.empty(search.get("jstime"))) where += " and a.CREATETIME <= '" + search.get("jstime") + "'";
if (!Util.empty(search.get("articleid"))) where += " and a.ARTICLEID = '" + search.get("articleid") + "'";

if("list".equals(act)){
	List<?> list = db.executeDbMapPager("cms_message," + where, pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT [NAME] FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		itemList.add(item);
	}
}else if ("deletelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT TITLE FROM cms_message WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("TITLE");
			db.executeNonQuery("DELETE FROM  cms_message WHERE ROW_ID = ?", checkbox);
		}
	}
	
	addLog("删除留言：" + itemMc, "2001");
	redirect("message.jsp?act=list&pid=" + params.get("pid"));
}else if ("update".equals(act) || "reply".equals(act) || "view".equals(act)){
	String rowid = params.get("rowid");
	entity = db.executeDbMap("SELECT a.*, b.TITLE AS ARTICLE from cms_message a, cms_article b WHERE a.ARTICLEID = b.ROW_ID AND a.ROW_ID = ?", rowid);
	articleList = db.executeDbMapList("SELECT a.ROW_ID, a.TITLE FROM cms_article a, cms_assort b WHERE a.ASSORTID = b.ROW_ID AND b.[NO] = '" + assort_message + "' AND a.STATE = 1 ORDER BY a.PUBDATE DESC");
}else if ("doupdate".equals(act)){
	String rowid = params.get("rowid");
	String title = params.get("title");
	String articleid = params.get("articleid");
	String isopen = params.get("isopen");
	String writer = params.get("writer");
	String phone = params.get("phone");
	String mobile = params.get("mobile");
	String content = params.get("content");
	
	DbCommand cmd = db.getSqlStringCommand("UPDATE cms_message SET TITLE=@TITLE,CONTENT=@CONTENT,WRITER=@WRITER,PHONE=@PHONE,MOBILE=@MOBILE," +
			"UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME,ARTICLEID=@ARTICLEID,ISOPEN=@ISOPEN WHERE ROW_ID=@ROW_ID");
	cmd.addInParameter("ROW_ID", rowid);
	cmd.addInParameter("TITLE", title);
	cmd.addInParameter("WRITER", writer);
	cmd.addInParameter("PHONE", phone);
	cmd.addInParameter("MOBILE", mobile);
	cmd.addInParameter("CONTENT", content);
	cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
	cmd.addInParameter("UPDATETIME", Util.getDate());
	cmd.addInParameter("ARTICLEID", articleid);
	cmd.addInParameter("DWID", current.get("DWID"));
	cmd.addInParameter("ISOPEN", Util.getInteger(isopen));
		
	cmd.executeNonQuery();
		
		
	addLog("修改留言：" + title , "2001");
		
	msg.setIsSuccess(true);
	msg.setMessage("您已经成功修改留言！");
	
	entity = db.executeDbMap("SELECT * from cms_message WHERE ROW_ID = ?", rowid);
	
}else if ("doreply".equals(act)){
	String rowid = params.get("rowid");
	String reply = params.get("reply");
	
	DbCommand cmd = db.getSqlStringCommand("UPDATE cms_message SET REPLY=@REPLY,UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME,STATE=@STATE WHERE ROW_ID=@ROW_ID");
	cmd.addInParameter("ROW_ID", rowid);
	cmd.addInParameter("REPLY", reply);
	cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
	cmd.addInParameter("UPDATETIME", Util.getDate());
	cmd.addInParameter("STATE", Util.empty(reply) ? 0 : 1);
	
	cmd.executeNonQuery();
	
	entity = db.executeDbMap("SELECT * from cms_message WHERE ROW_ID = ?", rowid);
	
	addLog("回复留言：" + entity.get("TITLE") , "2001");
		
	msg.setIsSuccess(true);
	msg.setMessage("您已经成功回复留言！");
}else if ("add".equals(act)){
	articleList = db.executeDbMapList("SELECT a.ROW_ID, a.TITLE FROM cms_article a, cms_assort b WHERE a.ASSORTID = b.ROW_ID AND b.[NO] = '" + assort_message + "' AND a.STATE = 1 ORDER BY a.PUBDATE DESC");
}else if ("doadd".equals(act)){
	String title = params.get("title");
	String articleid = params.get("articleid");
	String isopen = params.get("isopen");
	String writer = params.get("writer");
	String phone = params.get("phone");
	String mobile = params.get("mobile");
	String content = params.get("content");
	
	if(!Util.isUUID(articleid)) msg.putMessage("请选择具体文章！");
	else{
		DbMap articleTemp = db.executeDbMap("SELECT a.TITLE FROM cms_article a, cms_assort b WHERE a.ASSORTID = b.ROW_ID AND a.ROW_ID = ?", articleid);
		if(articleTemp == null) msg.putMessage("所选文章不存在！"); 
	}
	
	if(msg.emptyMessage()){
		DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_message(ROW_ID,TITLE,CONTENT,STATE,WRITER,PHONE,MOBILE,CREATETIME,UPDATEID,UPDATETIME,ARTICLEID,DWID,HITS,PARENTID,ISOPEN,CATEGORY,ISDELETED) VALUES(" +
				"@ROW_ID,@TITLE,@CONTENT,0,@WRITER,@PHONE,@MOBILE,@CREATETIME,@UPDATEID,@UPDATETIME,@ARTICLEID,@DWID,0,0,@ISOPEN,'',0)");
		cmd.addInParameter("ROW_ID", Util.getUUID());
		cmd.addInParameter("TITLE", title);
		cmd.addInParameter("WRITER", writer);
		cmd.addInParameter("PHONE", phone);
		cmd.addInParameter("MOBILE", mobile);
		cmd.addInParameter("CONTENT", content);
		cmd.addInParameter("CREATETIME", Util.getDate());
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.addInParameter("ARTICLEID", articleid);
		cmd.addInParameter("DWID", current.get("DWID"));
		cmd.addInParameter("ISOPEN", Util.getInteger(isopen));
			
		cmd.executeNonQuery();
			
			
		addLog("添加留言：" + title , "2001");
			
		msg.setIsSuccess(true);
		msg.setMessage("您已经成功添加留言！");
	}
}else if ("search".equals(act)){
	session.removeAttribute("message_search");
	articleList = db.executeDbMapList("SELECT a.ROW_ID, a.TITLE FROM cms_article a, cms_assort b WHERE a.ASSORTID = b.ROW_ID AND b.[NO] = '" + assort_message + "' AND a.STATE = 1 ORDER BY a.PUBDATE DESC");
}else if ("dosearch".equals(act)){
	Parameters searchParams = new Parameters();
	if(!Util.empty(params.get("keyword"))) searchParams.set("keyword", params.get("keyword"));
	if(!Util.empty(params.get("stime"))) searchParams.set("kstime", params.get("stime"));
	if(!Util.empty(params.get("etime"))) searchParams.set("jstime", params.get("etime"));
	if(!Util.empty(params.get("articleid"))) searchParams.set("articleid", params.get("articleid"));
	
	session.setAttribute("message_search", searchParams);
	redirect("message.jsp?act=list&pid=" + params.get("pid"));
}
%>