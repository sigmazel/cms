<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
String act = params.get("act");
List<DbMap> itemList = new ArrayList<DbMap>();
List<DbMap> roleList = new ArrayList<DbMap>();
Pager pager = new Pager(params.get("cpid"));
DbMap entity = null;
DbMap parent = null;
String roleString = "|";
String dwid = params.get("dwid");
DbMap currentDw = null;
String userAssortIdString = "";

List<DbMap> crumbs = new ArrayList<DbMap>();
crumbs.clear();

if (Util.empty(dwid) || "0".equals(dwid)) dwid = "0";
else{
	parent = (DbMap)db.executeDbMap("SELECT * FROM cms_dw WHERE ROW_ID = ?", dwid);
	crumbs = db.executeDbMapList("SELECT * FROM cms_dw WHERE CHARINDEX(PATH, '" + parent.getString("PATH")+ "') > 0 ORDER BY DWBH ASC ");
}

List<DbMap> assortList = db.executeDbMapList("SELECT * FROM cms_assort WHERE ROW_ID IN (SELECT ASSORTID FROM cms_dw_assort WHERE DWID = ?) ORDER BY PARENTID ASC, NO ASC", dwid);
for(DbMap dbMap : assortList){
	dbMap.put("NAME", dbMap.getString("NAME").replaceAll("\"", "“"));
}

pager.setQueryName("cpid");
pager.setQueryString("act=" + params.get("act") + params.query("dwid,predwid,pid"));

Parameters search = (Parameters)session.getAttribute("usecms_search");
if(true || search == null) search = new Parameters();
String where = "";

if(!Util.empty(search.get("code"))) where += " AND CODE like '%" + search.get("code") + "%'";
if(!Util.empty(search.get("name"))) where += " AND NAME like '%" + search.get("name") + "%'";
if(!Util.empty(search.get("des"))) where += " AND DES like '%" + search.get("des") + "%'";

if("list".equals(act)){
	List<?> list = db.executeDbMapPager("cms_user,DWID = '" + dwid + "' " + where, pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		List<?> roles = db.executeDbMapList("SELECT a.NAME FROM cms_role a, cms_userrole b WHERE a.ROW_ID = b.ROLEID AND b.USERID = ?", item.get("ROW_ID"));
		String roleName = "";
		for(int x = 0; x < roles.size(); x++){
			DbMap roleItem = (DbMap)roles.get(x);
			roleName += roleItem.getString("NAME") + " ";
		}
		item.put("ROLES", roleName);
		itemList.add(item);
	}
}else if ("deletelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT * FROM cms_user WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("NAME");
			db.executeNonQuery("DELETE FROM cms_user WHERE ROW_ID = ?", checkbox);
		}
	}
	
	addLog("删除用户：" + itemMc, "300202");
	redirect("user_list.jsp?act=list" + params.query("dwid,predwid,pid,cpid"));
}else if ("search".equals(act)){
	session.removeAttribute("dw_search");
}else if ("dosearch".equals(act)){
	Parameters tempDbMap = new Parameters();
	if(!Util.empty(params.get("dwbh"))) tempDbMap.put("dwbh", params.get("dwbh"));
	if(!Util.empty(params.get("dwmc"))) tempDbMap.put("dwmc", params.get("dwmc"));
	session.setAttribute("dw_search", tempDbMap);
	redirect("dw_list.jsp?act=list&predw=" + params.get("predwid"));
}else if ("update".equals(act)){
	String rowid = params.get("rowid");
	entity = db.executeDbMap("SELECT * FROM cms_user WHERE ROW_ID = ?", rowid);
	
	roleList = db.executeDbMapList("SELECT * FROM cms_role ORDER BY NAME ASC");
	
	List<?> roles = db.executeDbMapList("SELECT * FROM cms_userrole WHERE USERID = ?", rowid);
	for(int i = 0 ; i < roles.size();i++){
		DbMap item = (DbMap)roles.get(i);
		roleString += item.getString("ROLEID") + "|";
	}
	
	List<DbMap> userAssortList = db.executeDbMapList("SELECT * FROM cms_user_assort WHERE USERID = ?", rowid);
	for(DbMap dbMap : userAssortList) userAssortIdString += "," + dbMap.getString("ASSORTID") + ",";
	for(DbMap dbMap : assortList){
		dbMap.put("CHECKED", userAssortIdString.indexOf("," + dbMap.getString("ROW_ID") + ",") != -1 ? "true" : "false");
	}
}else if ("doupdate".equals(act)){
	String rowid = params.get("rowid");
	String name = params.get("name");
	String code = params.get("code");
	String password = params.get("password1");
	String email = params.get("email");
	String phone = params.get("phone");
	
	DbCommand cmd = db.getSqlStringCommand("SELECT * FROM cms_user WHERE CODE = @CODE AND ROW_ID <> @ROW_ID");
	cmd.addInParameter("CODE", code);
	cmd.addInParameter("ROW_ID", rowid);
	DbMap tempMap = cmd.executeDbMap();
	if(tempMap != null) msg.putMessage("系统已经存在此登录名用户!!");
	else{
		cmd = db.getSqlStringCommand("UPDATE cms_user SET CODE=@CODE,NAME=@NAME,EMAIL=@EMAIL,PHONE=@PHONE," +
				"UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME WHERE ROW_ID =@ROW_ID");
		cmd.addInParameter("ROW_ID", rowid);
		cmd.addInParameter("CODE", code);
		cmd.addInParameter("NAME", name);
		cmd.addInParameter("EMAIL", email);
		cmd.addInParameter("PHONE", phone);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		
		cmd.executeNonQuery();
		
		if(!Util.empty(password)){
			db.executeNonQuery("UPDATE cms_user SET PASSWD = '" + Util.hashOfMd5(password) + "' WHERE ROW_ID = ?", rowid);
		}
		
		db.executeNonQuery("DELETE FROM cms_userrole WHERE USERID = ?", rowid);
		String[] checkboxs = request.getParameterValues("checkbox");
		if(checkboxs != null){
			for(String checkbox : checkboxs){
				if(!Util.isUUID(checkbox)) continue;
				
				cmd = db.getSqlStringCommand("INSERT INTO cms_userrole(ROW_ID,USERID,ROLEID) VALUES(@ROW_ID,@USERID,@ROLEID)");
				cmd.addInParameter("ROW_ID", Util.getUUID());
				cmd.addInParameter("USERID", rowid);
				cmd.addInParameter("ROLEID", checkbox);
				cmd.executeNonQuery();
			}
		}
		
		db.executeNonQuery("DELETE FROM cms_user_assort WHERE USERID = ?", rowid);
		
		String[] assortIds = params.get("userAssortIdString").split(",");
		for(int i = 0; i < assortIds.length; i++){
			if(!Util.isUUID(assortIds[i])) continue;
			
			cmd = db.getSqlStringCommand("INSERT INTO cms_user_assort(ROW_ID, USERID, ASSORTID) VALUES(@ROW_ID, @USERID, @ASSORTID)");
			cmd.addInParameter("ROW_ID", Util.getUUID());
			cmd.addInParameter("USERID", rowid);
			cmd.addInParameter("ASSORTID", assortIds[i]);
			cmd.executeNonQuery();
		}
		
		addLog("修改用户：" + name , "300202");
		
		msg.setIsSuccess(true);
	}
	
	entity = db.executeDbMap("SELECT * FROM cms_user WHERE ROW_ID = ?", rowid);
}else if ("add".equals(act)){
	roleList = db.executeDbMapList("SELECT * FROM cms_role ORDER BY NAME ASC");
}else if ("edit".equals(act)){
	currentDw = db.executeDbMap("SELECT * FROM cms_dw WHERE ROW_ID = ?", current.get("DWID"));
}else if ("doadd".equals(act)){
	String name = params.get("name");
	String code = params.get("code");
	String password = params.get("password1");
	String email = params.get("email");
	String phone = params.get("phone");
	
	DbMap tempMap = db.executeDbMap("SELECT * FROM cms_user WHERE CODE = ?", code);
	if(tempMap != null) msg.putMessage("系统已经存在此登录名用户!!");
	else {
		String uuid = Util.getUUID();
		
		DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_user(ROW_ID,NAME,CODE,PASSWD,EMAIL,PHONE,UPDATEID,UPDATETIME,STATE,CREATEID,CREATTIME,DWID,TIMES,LOGINIP,DES,LOGIN) VALUES(" +
				"@ROW_ID,@NAME,@CODE,@PASSWD,@EMAIL,@PHONE,@UPDATEID,@UPDATETIME,1,@CREATEID,@CREATTIME,@DWID,0,'','',0)");
		cmd.addInParameter("ROW_ID", uuid);
		cmd.addInParameter("NAME", name);
		cmd.addInParameter("CODE", code);
		cmd.addInParameter("PASSWD", Util.hashOfMd5(password));
		cmd.addInParameter("EMAIL", email);
		cmd.addInParameter("PHONE", phone);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.addInParameter("CREATEID", current.get("ROW_ID"));
		cmd.addInParameter("CREATTIME", Util.getDate());
		cmd.addInParameter("DWID", dwid);
		
		cmd.executeNonQuery();
		
		String[] checkboxs = request.getParameterValues("checkbox");
		if(checkboxs != null){
			for(String checkbox : checkboxs){
				if(!Util.isUUID(checkbox)) continue;
				
				cmd = db.getSqlStringCommand("INSERT INTO cms_userrole(ROW_ID,USERID,ROLEID) VALUES(@ROW_ID,@USERID,@ROLEID)");
				cmd.addInParameter("ROW_ID", Util.getUUID());
				cmd.addInParameter("USERID", uuid);
				cmd.addInParameter("ROLEID", checkbox);
				cmd.executeNonQuery();
			}
		}
		
		String[] assortIds = params.get("userAssortIdString").split(",");
		for(int i = 0; i < assortIds.length; i++){
			if(!Util.isUUID(assortIds[i])) continue;
			
			cmd = db.getSqlStringCommand("INSERT INTO cms_user_assort(ROW_ID, USERID, ASSORTID) VALUES(@ROW_ID, @USERID, @ASSORTID)");
			cmd.addInParameter("ROW_ID", Util.getUUID());
			cmd.addInParameter("USERID", uuid);
			cmd.addInParameter("ASSORTID", assortIds[i]);
			cmd.executeNonQuery();
		}
		
		addLog("添加用户：" + name , "300202");
		
		msg.setIsSuccess(true);
	}
}else if ("doaccount".equals(act)){
	String name = params.get("name");
	String password = params.get("password1");
	String email = params.get("email");
	String phone = params.get("phone");
	
	if("administrator".equals(current.getString("CODE"))){
		db.executeNonQuery("UPDATE cms_variable SET VAL = '" + email + "' WHERE [IDENTITY] = 'adminemail'");
		db.executeNonQuery("UPDATE cms_variable SET VAL = '" + phone + "' WHERE [IDENTITY] = 'adminphone'");
		if(!Util.empty(password)){
			db.executeNonQuery("UPDATE cms_variable SET VAL = '" + Util.hashOfMd5(password) + "' WHERE [IDENTITY] = 'adminpasswd'");
		}
		DbMap userMap = new DbMap();
		userMap.put("NAME", "系统管理员");
		userMap.set("CODE", "administrator");
		userMap.put("ROW_ID", "1");
		userMap.put("DWID", "1");
		userMap.put("DWMC", "默认");
		userMap.put("DWCONTENT", "");
		userMap.put("DWPATH", "");
		userMap.put("LOGINIP", request.getRemoteAddr());
		
		List vaList = db.executeDbMapList("SELECT * FROM cms_variable");
		for(int i = 0; i < vaList.size();i++){
			DbMap item = (DbMap)vaList.get(i);
			if("adminname".equals(item.getString("IDENTITY"))) userMap.put("NAME", item.getString("VAL"));
			if("adminemail".equals(item.getString("IDENTITY"))) userMap.put("EMAIL", item.getString("VAL"));
			if("adminphone".equals(item.getString("IDENTITY"))) userMap.put("PHONE", item.getString("VAL"));
		}
		
   		session.setAttribute("current", userMap);
   		
	}else{
		DbCommand cmd = db.getSqlStringCommand("UPDATE cms_user SET NAME=@NAME,EMAIL=@EMAIL,PHONE=@PHONE," +
				"UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME WHERE ROW_ID =@ROW_ID");
		cmd.addInParameter("ROW_ID", current.getString("ROW_ID"));
		cmd.addInParameter("NAME", name);
		cmd.addInParameter("EMAIL", email);
		cmd.addInParameter("PHONE", phone);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		
		cmd.executeNonQuery();
		
		if(!Util.empty(password)){
			db.executeNonQuery("UPDATE cms_user SET PASSWD = '" + Util.hashOfMd5(password) + "' WHERE ROW_ID = ?", current.getString("ROW_ID"));
		}
		
		db.executeNonQuery("UPDATE cms_dw SET CONTENT = '" + params.get("content") +"' WHERE ROW_ID = ?", current.get("DWID"));
		
		session.setAttribute("current", db.executeDbMap("SELECT a.*,b.DWMC,b.DWBH,b.CONTENT AS DWCONTENT,b.DWALIAS,b.PATH AS DWPATH FROM cms_user a, cms_dw b WHERE a.DWID = b.ROW_ID AND a.ROW_ID = ?", current.getString("ROW_ID")));
	}
	
	currentDw = db.executeDbMap("SELECT * FROM cms_dw WHERE ROW_ID = ?", current.get("DWID"));
	
	addLog("修改个人资料：" + name , "300202");
	
	msg.setIsSuccess(true);
	msg.putMessage("您已经成功更新个人资料！");
}
%>