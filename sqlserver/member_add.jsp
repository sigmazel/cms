<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_member.jsp" %>      
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>###</title>
	<link href="images/style.css" rel="stylesheet" type="text/css">
	<script>
    function ValidateForm(theform){
    	if(theform.username.value==""){
             alert("账号不能为空。");
             theform.username.focus();
             return false
        }
        
        if(theform.passwd.value==""){
            alert("密码不能为空。");
            theform.passwd.focus();
            return false
        }
    }
    </script>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0"
		bgcolor="#EDF4FD">

		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
					&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a> 
					-&gt; 用户
					 -&gt; 添加
				</td>
			</tr>
			<tr>
				<td height="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td">
					&nbsp;
					<a href="member_list.jsp?act=list">列表</a> |&nbsp;
					<font color="#FF0000">添加</font>
				</td>
			</tr>
		</table>
		<br>
		<form action="member_add.jsp?act=doadd" onsubmit="return ValidateForm(this)" method="post">
			<table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" class="title_td1">
						<br>
						<br>
						<table width="75%" align="center" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="160" height="30" align="right">
									账号：
								</td>
								<td align="left">
									<input type="text" name="username" class="inputbox1" size="20">
									<font color="#FF0000">* </font>
								</td>
							</tr>
							<tr>
								<td height="30" align="right">
									密码：
								</td>
								<td align="left">
									<input type="password" name="passwd" class="inputbox1" size="20">
									<font color="#FF0000">* </font>
								</td>
							</tr>
							<tr>
								<td height="30" align="right">
									姓名：
								</td>
								<td align="left">
									<input type="text" name="realname" class="inputbox1" size="10">
								</td>
							</tr>
							<tr>
								<td height="30" align="right">
									联系电话：
								</td>
								<td align="left">
									<input type="text" name="connect" class="inputbox1" size="30" value=""/>
								</td>
							</tr>
							<tr>
								<td height="30" align="right">
									Email：
								</td>
								<td align="left">
									<input type="text" name="email" class="inputbox1" size="30" value=""/>
								</td>
							</tr>
							<tr>
								<td align="right">
									备注：
								</td>
								<td align="left">
									<textarea name="remark" cols="35" rows="4" class="inputarea"></textarea>
								</td>
							</tr>
						</table>
						<br/>
						<table width="75%" align="center" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="160"></td>
								<td align="left">
								<%if(operations.indexOf("|2002,add|") != -1 || "1".equals(current_userid)){%>
								<input type="submit" class="inputbox2" value=" 提 交 ">
								<%} %>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="button" class="inputbox2" value=" 返 回 " onclick="location='member_list.jsp?act=list'" />
								</td>
							</tr>
						</table>
						<p>
							&nbsp;
							<br>
							<br>
						</p>
					</td>
				</tr>

			</table>
		</form>
		<%if(!msg.emptyMessage()){%>
		<script type="text/javascript">
		alert('<%=msg.getMessage()%>');
		</script>
		<%}%>
		<%if(msg.getIsSuccess()){ %>
		<script type="text/javascript">
		location.href='member_list.jsp?act=list';
		</script>
		<%} %>
	</body>

</html>
