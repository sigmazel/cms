<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
List<DbMap> dbMapList = db.executeDbMapList("SELECT * FROM cms_variable");
DbMap settings = new DbMap();
for(DbMap dbMap : dbMapList) settings.put(dbMap.getString("IDENTITY"), dbMap.getString("VAL"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%= current.getString("DWMC")%> - <%=settings.get("sysname") %></title>
</head>
<frameset id="rootFrame" name="rootFrame" rows="60,*" cols="*" framespacing="0" frameborder="no" border="0">
  <frame src="frame_top.jsp" name="top" scrolling="No" noresize />
  <frameset id="frmContent" name="frmContent" rows="*" cols="112,6,*" framespacing="0" frameborder="no" border="0">
    <frameset rows="17,*" cols="*" framespacing="0" frameborder="no" border="0">
	    <frame src="menu_line.jsp" name="line" noresize/>
	    <%if(operations.indexOf("|10,view|") != -1 || "1".equals(current_userid)){%>
	    <frame src="menu_10.jsp" name="menu" noresize/>
	    <%}else if(operations.indexOf("|20,view|") != -1 || "1".equals(current_userid)){%>
	    <frame src="menu_20.jsp" name="menu" noresize/>
	    <%}else if(operations.indexOf("|30,view|") != -1 || "1".equals(current_userid)){%>
	    <frame src="menu_30.jsp" name="menu" noresize/>
	    <%}else if(operations.indexOf("|40,view|") != -1 || "1".equals(current_userid)){%>
	    <frame src="menu_40.jsp" name="menu" noresize/>
	    <%}%>
  	</frameset>
  	<frame src="menu_middle.jsp" name="middle" noresize/>
    <frame src="article_index.jsp?act=ilist" name="main" id="main" noresize/>
  </frameset>
</frameset>
<noframes>
<body></body>
</noframes>
</html>