<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_notice.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<style type="text/css">
		.redTip{
			color:red;
		}
		.editTbl td{
			padding:2px 2px 2px 4px;
			height:25px;
		}
		.inputitem{
			width:150px;
		}
		</style>
		<script src="../editor/kindeditor.js" type="text/javascript"></script>
		<script src="../editor/lang/zh_CN.js" type="text/javascript"></script>
		<script type="text/javascript">
			KindEditor.ready(function(K){
				var kindEditor = K.create('#txt_content', {
					resizeType:1, 
					pasteType:1, 
					allowImageUpload:true,
					allowFileManager:false, 
					allowPreviewEmoticons:false, 
					uploadJson : 'upload_json.jsp', 
					items : ['source', 'bold', 'italic', 'underline', 'image', 'multiimage', 'flash', 'media', 'link', 'insertfile']
				});
			});
			
		    function ValidateForm(theform){
		        if(theform.title.value==""){
		            alert("通知公告标题不能为空!");
		            theform.title.focus();
		            return false
		        }
		    }
		</script>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a>
						-&gt; 通知公告 
						-&gt; 添加
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
				<tr>
					<td height="22" class="title_td">
						&nbsp;
						<a href="notice.jsp?act=list&pid=<%=params.get("pid")%>">列表</a> |&nbsp;
						<font color="#FF0000">添加</font>
					</td>
				</tr>
			</table>
			<table width="95%" border="0" align="center"
				cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="padding-top:5px">
		<br/>
		<form action="notice_add.jsp?act=doadd<%=params.query("pid,psize,ref")%>" method="post" onsubmit="return ValidateForm(this)">
			<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
			<tr>
				<td bgcolor="#CEDDF0" width="90" style="font-weight:bold;width:90px;" align="left">标题：</td>
				<td bgcolor="#ffffff" colspan="5"><input type="text" name="title" size="50" class="inputbox1" /></td>
			</tr>
			</table>
			<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
			<tr bgcolor="#CEDDF0">
				<td height="28" style="font-weight:bold;" align="left">
					内容：
				</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding:1px 3px 1px 1px;">
					<textarea name="content" id="txt_content" cols="60" rows="8" style="width:100%;height:400px;"></textarea>
				</td>
			</tr>
			</table>
			<br/>
			<br/>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align=center>
				<tr>
					<td colspan="3" align="center">
						&nbsp;&nbsp;&nbsp;&nbsp;
						<%if(operations.indexOf("|1001,add|") != -1 || "1".equals(current_userid)){%>
						<input type="submit" class="inputbox2" value=" 提 交 ">
						<%} %>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='notice.jsp?act=list&pid=<%=params.get("pid")%>'">
						<br>
						<br>
					</td>
				</tr>
			</table>
			</form>
			<br/>
			<br/>
			</td>
			</tr>
		</table>
		<%if(!msg.emptyMessage()){%>
		<script type="text/javascript">
		alert('<%=msg.getMessage()%>');
		</script>
		<%}%>
		<%if(msg.getIsSuccess()){ %>
		<script type="text/javascript">
		location.href='notice.jsp?act=list&pid=<%=params.get("pid")%>';
		</script>
		<%} %>
	</body>
</html>