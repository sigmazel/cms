<%@ page language="java" pageEncoding="utf-8"%>
<%@ page import="com.google.gson.*"%>
<%@ page import="jxl.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.text.*"%>
<%@ include file="init.jsp" %>
<%
current = (DbMap)session.getAttribute("current");
if(current == null || Util.empty(current.get("ROW_ID"))) {
	response.sendRedirect("index_error.jsp?n=1");
	return;
}else{
	operations = (String)session.getAttribute("operations");
	current_userid = current.getString("ROW_ID");
}
%>