<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
String act = params.get("act");
List<DbMap> itemList = new ArrayList<DbMap>();
Pager pager = new Pager(params.get("pid"));
DbMap entity = null;
DbMap parent = null;
String privilegeString = "|";

pager.setQueryString("act=" + params.get("act"));

if("list".equals(act)){
	List<?> list = db.executeDbMapPager("cms_role,1 = 1 ", pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT NAME FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		itemList.add(item);
	}
}else if ("deletelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT * FROM cms_role WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("NAME");
			db.executeNonQuery("DELETE FROM cms_role WHERE ROW_ID = ?", checkbox);
			db.executeNonQuery("DELETE FROM cms_rolepri WHERE ROLEID = ?", checkbox);
		}
	}
	
	addLog("删除角色：" + itemMc, "3003");
	redirect("role_list.jsp?act=list");
}else if ("setup".equals(act)){
	String rowid = params.get("rowid");
	entity = db.executeDbMap("SELECT * FROM cms_role WHERE ROW_ID = ?", rowid);
	
	itemList = db.executeDbMapList("SELECT * FROM cms_resource ORDER BY [IDENTITY] ASC, PATH ASC");
	for(int i = 0 ; i < itemList.size();i++){
		DbMap item = (DbMap)itemList.get(i);
		String space = "";
		String[] paths = item.getString("PATH").split(",");
		if(paths.length > 1){
			for(int j = 1; j < paths.length; j++) space += "&nbsp;&nbsp;";
			item.put("NAME", space + "&nbsp;└&nbsp;" + item.getString("NAME"));
		}
		item.put("priList", db.executeDbMapList("SELECT b.ROW_ID,a.NAME FROM cms_operation a,cms_privilege b " + 
				"WHERE a.ROW_ID=b.OPERATIONID AND b.RESOURCEID=? ORDER BY a.SORTNO ASC", item.getString("ROW_ID")));
	}
	
	List priList = db.executeDbMapList("SELECT * FROM cms_rolepri WHERE ROLEID = ?", rowid);
	for(int i = 0 ; i < priList.size();i++){
		DbMap item = (DbMap)priList.get(i);
		privilegeString += item.getString("PRIVILEGEID") + "|";
	}
	
}else if ("dosetup".equals(act)){
	String rowid = params.get("rowid");
	entity = db.executeDbMap("SELECT * FROM cms_role WHERE ROW_ID = ?", rowid);
	
	db.executeNonQuery("DELETE FROM cms_rolepri WHERE ROLEID = ?", rowid);
	String[] checkboxs = request.getParameterValues("checkbox");
	if(checkboxs != null){
		for(String checkbox : checkboxs){
			DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_rolepri(ROW_ID,ROLEID,PRIVILEGEID) VALUES(@ROW_ID,@ROLEID,@PRIVILEGEID)");
			cmd.addInParameter("ROW_ID", Util.getUUID());
			cmd.addInParameter("ROLEID", rowid);
			cmd.addInParameter("PRIVILEGEID", checkbox);
			cmd.executeNonQuery();
		}
	}
	
	addLog("修改权限：" + entity.get("NAME") , "3003");
	
	msg.setIsSuccess(true);
}else if ("update".equals(act)){
	String rowid = params.get("rowid");
	entity = db.executeDbMap("SELECT * FROM cms_role WHERE ROW_ID = ?", rowid);
}else if ("doupdate".equals(act)){
	String rowid = params.get("rowid");
	String name = params.get("name");
	String des = params.get("des");
	
	DbCommand cmd = db.getSqlStringCommand("SELECT * FROM cms_role WHERE [NAME] = @NAME AND ROW_ID <> @ROW_ID");
	cmd.addInParameter("NAME", name);
	cmd.addInParameter("ROW_ID", rowid);
	DbMap tempMap = cmd.executeDbMap();
	
	if(tempMap != null) msg.putMessage("系统已经存在此名称角色!!");
	else {
		cmd = db.getSqlStringCommand("UPDATE cms_role SET NAME=@NAME,DES=@DES," +
				"UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME WHERE ROW_ID =@ROW_ID");
		cmd.addInParameter("ROW_ID", rowid);
		cmd.addInParameter("NAME", name);
		cmd.addInParameter("DES", des);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		
		cmd.executeNonQuery();
		
		addLog("修改角色：" + name , "3003");
		
		msg.setIsSuccess(true);
	}
	
	entity = db.executeDbMap("SELECT * FROM cms_role WHERE ROW_ID = ?", rowid);
}else if ("doadd".equals(act)){
	String name = params.get("name");
	String des = params.get("des");
	
	DbMap tempMap = db.executeDbMap("SELECT * FROM cms_role WHERE [NAME] = ?", name);
	if(tempMap != null) msg.putMessage("系统已经存在此名称角色!!");
	else {
		String uuid = Util.getUUID();
		
		DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_role(ROW_ID,NAME,DES,UPDATEID,UPDATETIME,STATE) VALUES(" +
				"@ROW_ID,@NAME,@DES,@UPDATEID,@UPDATETIME,@STATE)");
		cmd.addInParameter("ROW_ID", uuid);
		cmd.addInParameter("NAME", name);
		cmd.addInParameter("DES", des);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.addInParameter("STATE", 0);
		
		cmd.executeNonQuery();
		
		addLog("添加角色：" + name , "3003");
		
		msg.setIsSuccess(true);
	}
}
%>