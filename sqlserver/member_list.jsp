<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_member.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>###</title>
	<link href="images/style.css" rel="stylesheet" type="text/css">
	<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
	<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
	<script language="javascript" type="text/javascript">
	    function del() {
	        if (0 == listcount) {
	            alert("请选择记录。")
	            return;
	        }
	        if (confirm("确认进行删除吗？")) {
	            document.forms[0].submit();
	        }
	    }
	    function delone(urls) {
	        if (confirm("确认进行删除吗？"))	{
				 location = urls;
			}
	    }
		</script>
</head>
	<body  topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
					&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a> 
					-&gt; 用户
					<%if(search.size() > 0){%>
	            	 -&gt; <span class="required">查询结果</span>
	            	<%} %>
				</td>
			</tr>
			<tr>
				<td height="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td">
					&nbsp;
					<a href="member_list.jsp?act=list"><font color="#FF0000">列表</font></a> |&nbsp;
					<%if(operations.indexOf("|2002,add|") != -1 || "1".equals(current_userid)){%>
					<a href="member_add.jsp?act=add">添加</a> |&nbsp;
					<%} %>
					<%if(operations.indexOf("|2002,delete|") != -1 || "1".equals(current_userid)){%>
					<a href="javascript:del()">删除</a> |&nbsp;
					<%} %>
					<%if(operations.indexOf("|2002,search|") != -1 || "1".equals(current_userid)){%>
					<a href="member_search.jsp?act=search">查询</a>
					<%} %>
				</td>
			</tr>
		</table>
		<br/>
		<form name="formlist" id="formlist"  action="member_list.jsp?act=deletelist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>" method="post">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<table width="95%" align="center" border="0" class="TableList" cellpadding="0" cellspacing="0">
						<tr align="center">
							<td width="30" height="21" class="biaot">选择</td>
							<td class="biaot">账号</td>
							<td width="100" class="biaot">姓名</td>
							<td class="biaot">联系电话</td>
							<td class="biaot">Email</td>
							<td width="80" class="biaot">
								更新人
							</td>
							<td width="100" class="biaot">
								更新时间
							</td>
							<td class="biaot1" width="70">
								操作
							</td>
						</tr>
						<%for(int i = 0; i < itemList.size();i++){
							DbMap item = (DbMap)itemList.get(i);
						%>
						<tr bgcolor="#FFFFFF" onMouseOver="overtable(this);"
								onMouseOut="outtable(this);" id="<%=i + 1%>">
							<td height="21" class="biaol" align="center">
								<input type="checkbox" name="checkbox" id="c<%=i + 1%>" value="<%=item.get("ROW_ID")%>" onClick="selectbox(this)">
							</td>
							<td height="21" class="biaol" align="center">
							<%=item.get("USERNAME")%>
							</td>
							<td class="biaol" align="center">
							<%=item.get("REALNAME")%>
							</td>
							<td class="biaol" align="center">
							&nbsp;<%=item.get("CONNECT")%>
							</td>
							<td class="biaol" align="center">
							&nbsp;<%=item.get("EMAIL")%>
							</td>
							<td class="biaol" align="center">
							<%=item.get("EDITER")%>
							</td>
							<td class="biaol" align="center">
							<%=Util.format(item.get("UPDATETIME"), "yyyy-MM-dd h:i")%>
							</td>
							<td class="biaol" align="center">&nbsp;
							<%if(operations.indexOf("|2002,update|") != -1 || "1".equals(current_userid)){%>
							<a href="member_update.jsp?act=update&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">修改</a>
							<%} %>
							<%if(operations.indexOf("|2002,delete|") != -1 || "1".equals(current_userid)){%>
							<a href="javascript:delone('member_list.jsp?checkbox=<%=item.get("ROW_ID")%>&act=deletelist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>')">删除</a>
							<%} %>
							</td>
						</tr>
					  <%} %>
					</table>
				</td>
			</tr>
		</table>
		<table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
            <td width="4%" height="25" valign="bottom">
            <input type="checkbox" name="toggleAll" onClick="ToggleAll(this);">
            </td>
            <td width="96%" valign="bottom">全选</td>
        </tr>
        </table>
		</form>
		<table width="95%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td height="30" colspan="2">
					<%=pager%>
				</td>
				<td align="right">
					每页：
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=10" <%if(pager.getPageSize() == 10){%>style="font-weight:bold; color:red;"<%}%>>10</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=20" <%if(pager.getPageSize() == 20){%>style="font-weight:bold; color:red;"<%}%>>20</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=30" <%if(pager.getPageSize() == 30){%>style="font-weight:bold; color:red;"<%}%>>30</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=50" <%if(pager.getPageSize() == 50){%>style="font-weight:bold; color:red;"<%}%>>50</a>
					<a href="?pid=<%=params.get("pid")%>&act=<%=params.get("act")%>&psize=100" <%if(pager.getPageSize() == 100){%>style="font-weight:bold; color:red;"<%}%>>100</a>
					条
				</td>
			</tr>
		</table>
</body>
</html>