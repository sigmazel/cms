<%@ page language="java" pageEncoding="utf-8"%>
<%
response.setContentType("text/html;charset=utf-8");
request.setCharacterEncoding("utf-8");
String n = request.getParameter("n");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>提示信息</title>
<style type="text/css">
<!--
@import url(images/style.css);
-->
</style>
</head>
<body bottommargin="0" bgcolor="#f7f7f7" >
<br>
<table height="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="middle" >
    <table width="540"  border="0" align="center" cellpadding="10" cellspacing="2" bgcolor="gray">
      <tr bgcolor="#CEDDF0">
        <td valign="top" align="center" style="font-size:16px;padding:20px;">
        <%if("0".equals(n)){%><div style="font-size:24px;color:red;">该系统未初始化，请联系管理员。</div><%}%>
        <%if("1".equals(n)){%>
        <div style="font-size:24px;color:red;">登录超时或您未登录系统，请重新登录！</div>
        <br/><a href="index.jsp" style="font-size:18px;">返回首页</a>
        <%}%>
        <%if("2".equals(n)){%>
        <div style="font-size:24px;color:red;">您已经成功退出系统！</div>
        <br/><a href="index.jsp" style="font-size:18px;">返回首页</a>
        <%}%>
        <%if("3".equals(n)){%>
        <div style="font-size:24px;color:red;">您已经成功登录系统！</div>
        <br/><a href="frame_index.jsp" style="font-size:24px;">进入后台</a>
        <script type="text/javascript">
        setTimeout("window.location.href ='frame_index.jsp';", 3000);
        </script>
        <%}%>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>