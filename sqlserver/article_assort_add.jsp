<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_article_assort.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
		<script>
	     function ValidateForm(theform){
	        if(theform.no.value==""){
	            alert("编号不能为空。");
	            theform.no.focus();
	            return false
	        }
	       if(theform.mc.value==""){
	            alert("名称不能为空。");
	            theform.mc.focus();
	            return false
	        }
	    }
	</script>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">

		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
					&nbsp;&nbsp;系统管理 
					-&gt; <a href="article_assort_list.jsp?act=list">文章分类</a> 
					<%for(int i = 0; i < crumbs.size();i++ ){
					DbMap crumb = (DbMap)crumbs.get(i);
					%>
					-&gt; <a href="article_assort_list.jsp?act=list&parentid=<%=crumb.get("ROW_ID")%>"><%=crumb.get("NAME")%>(<%=crumb.get("NO")%>)</a>
					<%} %>
					-&gt; 添加
				</td>
			</tr>
			<tr>
				<td height="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td">
					&nbsp;
					<a href="article_assort_list.jsp?act=list&parentid=<%=params.get("parentid")%>">列表</a> |&nbsp;
					<font color="#FF0000">添加</font>
				</td>
			</tr>
		</table>
		<br>
		<form action="article_assort_add.jsp?act=doadd&parentid=<%=params.get("parentid")%>" method="post" onsubmit="return ValidateForm(this)">
			<table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" class="title_td1">
						<br>
						<br>
						<br>
						<table width="75%" align="center" border="0" cellpadding="0"
							cellspacing="0">
							<tr>
								<td width="35%" height="30" align="right">
									编号：
								</td>
								<td width="65%" align="left">
									<input type="text" name="no" class="inputbox1" size="15">
									<font color="#FF0000">*由字符组成</font>
								</td>
							</tr>
							<tr>
								<td width="35%" height="30" align="right">
									名称：
								</td>
								<td width="65%" align="left">
									<input type="text" name="mc" class="inputbox1" size="50">
									<font color="#FF0000">*</font>
								</td>
							</tr>
							<tr>
								<td width="35%" height="30" align="right">
									备注：
								</td>
								<td width="65%" align="left">
									<textarea name="des" cols="60" rows="6" class="inputarea"></textarea>
								</td>
							</tr>
							<tr>
								<td width="35%" height="100" align="right">
									允许IP：
								</td>
								<td width="65%" align="left">
									<textarea name="iplimit" cols="60" rows="4" class="inputarea"><%=entity.get("IPLIMIT")%></textarea><br/>
									示例：(不填即不作限制！)<br/>
									单个IP：192.168.0.100<br/>
									IP段：192.168.0.100-192.168.0.200
								</td>
							</tr>
							<tr>
			                  <td height="25" align="right">审核：</td>
			                  <td align="left">
			                  	<input type="radio" name="state" value="0" checked="checked">审核后发布
			                  	<input type="radio" name="state" value="1">直接发布
			                  </td>
			                </tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
						</table>
						<p>
							<%if(operations.indexOf("|4003,add|") != -1 || "1".equals(current_userid)){%>
								<input type="submit" class="inputbox2" value=" 提 交 ">
							<%} %>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_assort_list.jsp?act=list&parentid=<%=params.get("parentid")%>'">
							<br>
							<br>
						</p>
						<p>
							&nbsp;
							<br>
							<br>
						</p>

					</td>
				</tr>

			</table>
		</form>
		<%if(!msg.emptyMessage()){%>
		<script type="text/javascript">
		alert('<%=msg.getMessage()%>');
		</script>
		<%}%>
		<%if(msg.getIsSuccess()){ %>
		<script type="text/javascript">
		location.href='article_assort_list.jsp?act=list&parentid=<%=params.get("parentid")%>';
		</script>
		<%} %>
	</body>

</html>
