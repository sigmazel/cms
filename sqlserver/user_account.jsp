<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_user.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>###</title>
<link href="images/style.css" rel="stylesheet" type="text/css">
<script>
     function ValidateForm(theform){
        if(theform.name.value==""){
            alert("姓名不能为空。");
            theform.name.focus();
            return false
        }
        if(theform.password1.value!=theform.password2.value){
            alert("两次输入的密码不一致。");
            theform.password1.focus();
            return false
        }
     }
</script>


<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
<form action="user_account.jsp?act=doaccount" method="post" onsubmit="return ValidateForm(this)">
<table width="95%" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="title">&nbsp;&nbsp;用户管理 -&gt; 个人信息 -&gt; <font color="#FF0000"><%=current.get("NAME") %></font></td>
  </tr>
  <tr>
    <td height="2"></td>
  </tr>
  
</table><br>
    <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td align="center" class="title_td1">
        <br>
        <br>
         <table width="75%" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr >
                  <td height="30" align="right">姓名：</td>
                  <td width="76%" align="left"><input type="text" name="name" class="inputbox1" size="15"  value="<%=current.get("NAME") %>"></td>
                </tr>
                <tr >
                  <td height="30" align="right">密码：</td>
                  <td width="76%" align="left"><input type="password" name="password1" class="inputbox1" size="20"  >
                  &nbsp;不修改密码不用填写</td>
                </tr>
                <tr >
                  <td height="30" align="right" >确认密码：</td>
                  <td width="76%" align="left"><input type="password" name="password2" class="inputbox1" size="20" ></td>
                </tr>
                <tr >
                  <td height="30" align="right">Email：</td>
                  <td width="76%" align="left"><input type="text" name="email" class="inputbox1" size="30"  value="<%=current.get("EMAIL") %>"></td>
                </tr>
                <tr >
                  <td height="30" align="right">电话：</td>
                  <td width="76%" align="left"><input type="text" name="phone" class="inputbox1" size="30"  value="<%=current.get("PHONE") %>"></td>
                </tr>
				<%if(!"1".equals(current_userid)){%>
				<tr>
					<td height="30" align="right">
						相关领导：
					</td>
					<td width="76%" align="left">
						<input type="text" name="content" class="inputbox1" size="30" value="<%=currentDw.get("CONTENT")%>">(以,号隔开)
					</td>
				</tr>
				<%} %>
            </table>
            <div style="clear:both;" align="center">
            <%if(operations.indexOf("|3001,update|") != -1 || "1".equals(current_userid)){%>
            <p>
          	<input type="submit" class="inputbox2" value=" 确 定 ">&nbsp;&nbsp;&nbsp;&nbsp;
          	</p>
          	<%} %>
          <br>
          <br>
          </div>
    </td>
  </tr>
</table>
</form>
<%if(!msg.emptyMessage()){%>
<script type="text/javascript">
alert('<%=msg.getMessage()%>');
</script>
<%}%>
<%if(msg.getIsSuccess()){ %>
<script type="text/javascript">
location.href='user_account.jsp?act=edit';
</script>
<%}%>
</body>

</html>
