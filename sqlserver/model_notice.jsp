<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
String act = params.get("act");
List<DbMap> itemList = new ArrayList<DbMap>();
Pager pager = new Pager(params.get("pid"), Util.getInteger(params.get("psize")) < 10 ? 10 : Util.getInteger(params.get("psize")));
DbMap entity = null;

pager.setQueryString("act=" + params.get("act"));

Parameters search = null;

if("list".equals(act)) search = (Parameters) session.getAttribute("notice_search");

String where = "";
if(search == null) search = new Parameters();

if (!Util.empty(search.get("keyword"))) where += " and a.TITLE + a.SUMMARY like '%" + search.get("keyword") + "%'";
if (!Util.empty(search.get("kstime"))) where += " and a.UPDATETIME >= '" + search.get("kstime") + "'";
if (!Util.empty(search.get("jstime"))) where += " and a.UPDATETIME <= '" + search.get("jstime") + "'";

if("list".equals(act)){
	List<?> list = db.executeDbMapPager("cms_notice,1 = 1 " + where, pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT [NAME] FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		itemList.add(item);
	}
}else if ("deletelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT TITLE FROM cms_notice WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("TITLE");
			db.executeNonQuery("DELETE FROM  cms_notice WHERE ROW_ID = ?", checkbox);
		}
	}
	
	addLog("删除通知公告：" + itemMc, "1001");
	redirect("notice.jsp?act=list&pid=" + params.get("pid"));
}else if ("update".equals(act) || "back".equals(act) || "view".equals(act)){
	String rowid = params.get("rowid");
	entity = db.executeDbMap("SELECT * from cms_notice WHERE ROW_ID = ?", rowid);
	if("view".equals(act)){
		db.executeNonQuery("update cms_notice set HITS = HITS + 1 WHERE ROW_ID = ?", rowid);
	}
}else if ("doupdate".equals(act)){
	String rowid = params.get("rowid");
	String title = params.get("title");
	String content = params.get("content");
	    
    String summary;
    if(content == null || "".equals(content) || content.length() <= 0) summary = "";
	else{
		String contentTemp = content;
    	contentTemp = contentTemp.replaceAll("<\\/?[^>]+>", "");
    	contentTemp = contentTemp.replaceAll("&nbsp;", "");
    	contentTemp = contentTemp.replaceAll("　", "");
    	summary = contentTemp.length() > 200 ? contentTemp.substring(0, 200) : contentTemp;  
	}
    
	DbCommand cmd = db.getSqlStringCommand("UPDATE cms_notice SET TITLE=@TITLE,CONTENT=@CONTENT," +
			"UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME,SUMMARY=@SUMMARY,STATE=0  WHERE ROW_ID=@ROW_ID");
	cmd.addInParameter("ROW_ID", rowid);
	cmd.addInParameter("TITLE", title);
	cmd.addInParameter("CONTENT", content);
	cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
	cmd.addInParameter("UPDATETIME", Util.getDate());
	cmd.addInParameter("SUMMARY", summary);
	cmd.addInParameter("DWID", current.get("DWID"));
		
	cmd.executeNonQuery();
		
		
	addLog("修改通知公告：" + title , "1001");
		
	msg.setIsSuccess(true);
	msg.setMessage("您已经成功修改通知公告！");
	
	entity = db.executeDbMap("SELECT * from cms_notice WHERE ROW_ID = ?", rowid);
	
}else if ("add".equals(act)){
}else if ("doadd".equals(act)){
	String title = params.get("title");
	String content = params.get("content");
	
    String summary;
    if(content == null || "".equals(content) || content.length() <= 0) summary = "";
	else{
		String contentTemp = content;
    	contentTemp = contentTemp.replaceAll("<\\/?[^>]+>", "");
    	contentTemp = contentTemp.replaceAll("&nbsp;", "");
    	contentTemp = contentTemp.replaceAll("　", "");
    	summary = contentTemp.length() > 200 ? contentTemp.substring(0, 200) : contentTemp; 
	}
    
	DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_notice(ROW_ID,TITLE,CONTENT,STATE,CREATEID,CREATETIME,UPDATEID,UPDATETIME,SUMMARY,DWID,HITS) VALUES(" +
			"@ROW_ID,@TITLE,@CONTENT,0,@CREATEID,@CREATETIME,@UPDATEID,@UPDATETIME,@SUMMARY,@DWID,0)");
	cmd.addInParameter("ROW_ID", Util.getUUID());
	cmd.addInParameter("TITLE", title);
	cmd.addInParameter("CONTENT", content);
	cmd.addInParameter("CREATEID", current.get("ROW_ID"));
	cmd.addInParameter("CREATETIME", Util.getDate());
	cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
	cmd.addInParameter("UPDATETIME", Util.getDate());
	cmd.addInParameter("SUMMARY", summary);
	cmd.addInParameter("DWID", current.get("DWID"));
		
	cmd.executeNonQuery();
		
		
	addLog("添加通知公告：" + title , "1001");
		
	msg.setIsSuccess(true);
	msg.setMessage("您已经成功添加通知公告！");
}else if ("search".equals(act)){
	session.removeAttribute("notice_search");
}else if ("dosearch".equals(act)){
	Parameters searchParams = new Parameters();
	if(!Util.empty(params.get("keyword"))) searchParams.set("keyword", params.get("keyword"));
	if(!Util.empty(params.get("stime"))) searchParams.set("kstime", params.get("stime"));
	if(!Util.empty(params.get("etime"))) searchParams.set("jstime", params.get("etime"));
	
	session.setAttribute("notice_search", searchParams);
	redirect("notice.jsp?act=list&pid=" + params.get("pid"));
}
%>