<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_article.jsp" %>
<%
String ref = params.get("ref");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<link href="images/datepicker.css" rel="stylesheet" type="text/css" />
		<script src="js/jquery-1.7.1.js" type="text/javascript"></script>
		<script src="laydate/laydate.js" type="text/javascript"></script>
		<style type="text/css">
		.redTip{
			color:red;
		}
		.editTbl td{
			padding:2px 2px 2px 4px;
			height:25px;
		}
		.inputitem{
			width:150px;
		}
		</style>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">	
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a> 
						-&gt; 信息统计 
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
				<tr>
					<td height="22" class="title_td">
						&nbsp;
						<font color="#FF0000">统计</font> |&nbsp;
					</td>
				</tr>
			</table>
			<table width="95%" border="0" align="center"
				cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="padding-top:5px">
		<br/>
		<form action="article_stat.jsp?act=stat" method="post">
			<table width="80%" border=0 cellpadding=0 cellspacing=1 align="center">
			<tr>
				<td>
				按时间搜索:
				<input type="text" name="pstarttime" size="10" class="inputbox1" value="<%=params.get("pstarttime")%>" onclick="laydate({istime:false, format:'YYYY-MM-DD'})"/>
				至
				<input type="text" name="pendtime" size="10" class="inputbox1" value="<%=params.get("pendtime")%>" onclick="laydate({istime:false, format:'YYYY-MM-DD'})"/>
				<%if(operations.indexOf("|1002,search|") != -1 || "1".equals(current_userid)){%>
				<input type="submit" value=" 搜 索 " class="inputbox2" />
				<%} %>
				</td>
			</tr>
			</table>
			<br/>
			<table width="90%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl">
			<tr bgcolor="#CEDDF0">
				<td width="30" height="21" class="biaot">
				序号
				</td>
				<td width="80" class="biaot">
				单位名称
				</td>
				<td width="80" class="biaot" align="center">
				已发布数
				</td>
				<td width="80" class="biaot" align="center">
				待审核数
				</td>
				<td width="80" class="biaot" align="center">
				不审核数
				</td>
				<td width="80" class="biaot" align="center">
				已审核数
				</td>
				<td width="80" class="biaot" align="center">
				已采用数
				</td>
			</tr>
			<%
			for(int i = 0 ; i < itemList.size(); i++){
				DbMap item = (DbMap)itemList.get(i);
			%>
				<tr bgcolor="#FFFFFF">
					<td height="21" class="biaol" align="center">
						<%=pager.getStart() + i + 1 %>
					</td>
					<td class="biaol" align="left">
					&nbsp;<%= item.get("DWMC")%>
					</td>
					<td class="biaol" align="center">
					<%=item.get("COUNT")%>
					</td>
					<td class="biaol" align="center">
					<%=item.get("NDCOUNT") %>
					</td>
					<td class="biaol" align="center">
					<%=item.get("NACOUNT") %>
					</td>
					<td class="biaol" align="center">
					<%=item.get("ACOUNT") %>
					</td>
					<td class="biaol" align="center">
					<%=item.get("RCOUNT") %>
					</td>
				</tr>
			<%
			}
			%>
			</table>
			<br/>
			<br/>
			</form>
			<br/>
			<br/>
			</td>
			</tr>
		</table>
	</body>
</html>