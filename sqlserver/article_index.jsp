<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_article.jsp" %>
<%
String ref = params.get("ref");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<style type="text/css">
		.redTip{
			color:red;
		}
		.editTbl td{
			padding:2px 2px 2px 4px;
			height:25px;
		}
		.inputitem{
			width:150px;
		}
		</style>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">	
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title" height="30">
						&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a> 
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
			</table>
			<table width="95%" border="0" align="center"
				cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="padding-top:5px">
			<table width="100%" border=0 cellpadding=0 cellspacing=0 align="center">
			<tr>
			<td>
			<div style="font-size:12px;font-weight:bold;">通知公告（前6条）</div>
			</td>
			<td width="80" align="right"><a href="notice.jsp?act=list">&gt;&gt;更多</a></td>
			</tr>
			</table>
			<table width="100%" align="center" border="0" class="TableList"
				cellpadding="0" cellspacing="0">
				<tr align="center">
					<td width="30" height="21" class="biaot">
						序号
					</td>
					<td class="biaot">
						标题
					</td>
					<td width="80" class="biaot">
						点击数
					</td>
					<td class="biaot1" width="40">
						操作
					</td>
				</tr>
				<%
				List noticeList = (List)entity.get("noticeList");
				for(int i = 0, j = noticeList.size(); i < j; i++){
					DbMap item = (DbMap)noticeList.get(i);
				%>
					<tr bgcolor="#FFFFFF" id="<%=i + 1%>">
						<td height="21" class="biaol" align="center">
							<%=pager.getStart() + i + 1 %>
						</td>
						<td class="biaol">
						<a href="notice_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=index"><%=item.get("TITLE") %></a>
						</td>
						<td class="biaol" align="center">
							<%=item.get("HITS") %>
						</td>
						<td class="biaol" align="left">&nbsp;
							<a href="notice_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=index">查看</a>
						</td>
					</tr>
				<%
				}
				%>
			</table>
			<br/>
			<hr size="1" color="#7190E0" />
			<br/>
			<table width="100%" border=0 cellpadding=0 cellspacing=0 align="center">
			<tr>
			<td>
			<div style="font-size:12px;font-weight:bold;">待审核文章（前6条）</div>
			</td>
			<td width="80" align="right"><a href="article_nodeal.jsp?act=ndlist">&gt;&gt;更多</a></td>
			</tr>
			</table>
			<table width="100%" align="center" border="0" class="TableList"
				cellpadding="0" cellspacing="0">
				<tr align="center">
					<td width="30" height="21" class="biaot">
						序号
					</td>
					<td width="120" class="biaot">
						分类
					</td>
					<td class="biaot">
						标题
					</td>
					<td width="140" class="biaot">
						所属单位
					</td>
					<td width="80" class="biaot">
						发布日期
					</td>
					<td class="biaot1" width="40">
						操作
					</td>
				</tr>
				<%
				List nodealList = (List)entity.get("nodealList");
				for(int i = 0, j = nodealList.size(); i < j; i++){
					DbMap item = (DbMap)nodealList.get(i);
				%>
					<tr bgcolor="#FFFFFF" id="<%=i + 1%>">
						<td height="21" class="biaol" align="center">
							<%=pager.getStart() + i + 1 %>
						</td>
						<td class="biaol" align="center">&nbsp;<%= item.get("NAME")%>
						</td>
						<td class="biaol">
						<a href="article_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=index"><%=item.get("TITLE") %></a>
						</td>
						<td class="biaol" align="center">
							<%=item.get("DWMC") %>
						</td>
						<td class="biaol" align="center">
							<%=item.get("PUBDATE") %>
						</td>
						<td class="biaol" align="left">&nbsp;
							<a href="article_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=index">查看</a>
						</td>
					</tr>
				<%
				}
				%>
			</table>
			<br/>
			<hr size="1" color="#7190E0" />
			<br/>
			<table width="100%" border=0 cellpadding=0 cellspacing=0 align="center">
			<tr>
			<td>
			<div style="font-size:12px;font-weight:bold;">不审核文章（前6条）</div>
			</td>
			<td width="80" align="right"><a href="article_noaudit.jsp?act=nalist">&gt;&gt;更多</a></td>
			</tr>
			</table>
			<table width="100%" align="center" border="0" class="TableList"
				cellpadding="0" cellspacing="0">
				<tr align="center">
					<td width="30" height="21" class="biaot">
						序号
					</td>
					<td width="120" class="biaot">
						分类
					</td>
					<td class="biaot">
						标题
					</td>
					<td width="140" class="biaot">
						所属单位
					</td>
					<td width="80" class="biaot">
						发布日期
					</td>
					<td class="biaot1" width="40">
						操作
					</td>
				</tr>
				<%
				List noauditList = (List)entity.get("noauditList");
				for(int i = 0, j = noauditList.size(); i < j; i++){
					DbMap item = (DbMap)noauditList.get(i);
				%>
					<tr bgcolor="#FFFFFF" id="<%=i + 1%>">
						<td height="21" class="biaol" align="center">
							<%=pager.getStart() + i + 1 %>
						</td>
						<td class="biaol" align="center">&nbsp;<%= item.get("NAME")%>
						</td>
						<td class="biaol">
						<a href="article_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=index"><%=item.get("TITLE") %></a>
						</td>
						<td class="biaol" align="center">
							<%=item.get("DWMC") %>
						</td>
						<td class="biaol" align="center">
							<%=item.get("PUBDATE") %>
						</td>
						<td class="biaol" align="left">&nbsp;
							<a href="article_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=index">查看</a>
						</td>
					</tr>
				<%
				}
				%>
			</table>
			<br/>
			<hr size="1" color="#7190E0" />
			<br/>
			<table width="100%" border=0 cellpadding=0 cellspacing=0 align="center">
			<tr>
			<td>
			<div style="font-size:12px;font-weight:bold;">已审核文章（前6条）</div>
			</td>
			<td width="80" align="right"><a href="article_audit.jsp?act=alist">&gt;&gt;更多</a></td>
			</tr>
			</table>
			<table width="100%" align="center" border="0" class="TableList"
				cellpadding="0" cellspacing="0">
				<tr align="center">
					<td width="30" height="21" class="biaot">
						序号
					</td>
					<td width="120" class="biaot">
						分类
					</td>
					<td class="biaot">
						标题
					</td>
					<td width="140" class="biaot">
						所属单位
					</td>
					<td width="80" class="biaot">
						发布日期
					</td>
					<td class="biaot1" width="40">
						操作
					</td>
				</tr>
				<%
				List auditList = (List)entity.get("auditList");
				for(int i = 0, j = auditList.size(); i < j; i++){
					DbMap item = (DbMap)auditList.get(i);
				%>
					<tr bgcolor="#FFFFFF" id="<%=i + 1%>">
						<td height="21" class="biaol" align="center">
							<%=pager.getStart() + i + 1 %>
						</td>
						<td class="biaol" align="center">&nbsp;<%= item.get("NAME")%>
						</td>
						<td class="biaol">
						<a href="article_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=index"><%=item.get("TITLE") %></a>
						</td>
						<td class="biaol" align="center">
							<%=item.get("DWMC") %>
						</td>
						<td class="biaol" align="center">
							<%=item.get("PUBDATE") %>
						</td>
						<td class="biaol" align="left">&nbsp;
							<a href="article_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=index">查看</a>
						</td>
					</tr>
				<%
				}
				%>
			</table>
			<br/>
			<hr size="1" color="#7190E0" />
			<br/>
			<table width="100%" border=0 cellpadding=0 cellspacing=0 align="center">
			<tr>
			<td>
			<div style="font-size:12px;font-weight:bold;">共享文章（前6条）</div>
			</td>
			<td width="80" align="right"><a href="article_share.jsp?act=slist">&gt;&gt;更多</a></td>
			</tr>
			</table>
			<table width="100%" align="center" border="0" class="TableList"
				cellpadding="0" cellspacing="0">
				<tr align="center">
					<td width="30" height="21" class="biaot">
						序号
					</td>
					<td width="120" class="biaot">
						分类
					</td>
					<td class="biaot">
						标题
					</td>
					<td width="140" class="biaot">
						所属单位
					</td>
					<td width="80" class="biaot">
						发布日期
					</td>
					<td class="biaot1" width="40">
						操作
					</td>
				</tr>
				<%
				List shareList = (List)entity.get("shareList");
				for(int i = 0, j = shareList.size(); i < j; i++){
					DbMap item = (DbMap)shareList.get(i);
				%>
					<tr bgcolor="#FFFFFF" id="<%=i + 1%>">
						<td height="21" class="biaol" align="center">
							<%=pager.getStart() + i + 1 %>
						</td>
						<td class="biaol" align="center">&nbsp;<%= item.get("NAME")%>
						</td>
						<td class="biaol">
						<a href="article_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=index"><%=item.get("TITLE") %></a>
						</td>
						<td class="biaol" align="center">
							<%=item.get("DWMC") %>
						</td>
						<td class="biaol" align="center">
							<%=item.get("PUBDATE") %>
						</td>
						<td class="biaol" align="left">&nbsp;
							<a href="article_view.jsp?act=view&rowid=<%=item.get("ROW_ID")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=index">查看</a>
						</td>
					</tr>
				<%
				}
				%>
			</table>
			<br/>
			</td>
			</tr>
		</table>
	</body>
</html>