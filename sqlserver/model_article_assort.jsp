<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
String act = params.get("act");
List<DbMap> itemList = new ArrayList<DbMap>();
DbMap entity = null;
DbMap parent = null;

String parentid = params.get("parentid");
List<DbMap> crumbs = new ArrayList<DbMap>();
crumbs.clear();

if (Util.empty(parentid) || "0".equals(parentid)) parentid = "0";
else{
	parent = (DbMap)db.executeDbMap("SELECT * FROM cms_assort WHERE ROW_ID = ?", parentid);
	crumbs = db.executeDbMapList("SELECT * FROM cms_assort WHERE CHARINDEX(PATH, '" + parent.getString("PATH")+ "') > 0 ORDER BY NO ASC");
}

if("list".equals(act)){
	List<?> list = db.executeDbMapList("SELECT * FROM cms_assort  WHERE PARENTID = '" + parentid + "' ORDER BY NO ASC");
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object editer = db.executeScalar("SELECT NAME FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		item.set("DES", Util.nlToBr(item.getString("DES")));
		
		itemList.add(item);
	}
}else if ("deletelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT * FROM cms_assort WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("NAME");
			db.executeNonQuery("DELETE FROM cms_assort WHERE ROW_ID = ?", checkbox);
			db.executeNonQuery("DELETE FROM cms_dw_assort WHERE ASSORTID = ?", checkbox);
			if(!"0".equals(tempDbMap.getString("PARENTID"))){
				db.executeNonQuery("UPDATE cms_assort SET CHILDREN = CHILDREN - 1 WHERE ROW_ID = ?", tempDbMap.getString("PARENTID"));
			}
		}
	}
	
	addLog("删除文章分类：" + itemMc, "4003");
	redirect("article_assort_list.jsp?act=list&parentid=" + params.get("parentid"));
}else if ("update".equals(act)){
	String rowid = params.get("rowid");
	entity = db.executeDbMap("SELECT * FROM cms_assort WHERE ROW_ID = ?", rowid);
}else if ("doupdate".equals(act)){
	String rowid = params.get("rowid");
	String mc = params.get("mc");
	String no = params.get("no");
	String des = params.get("des");
	String iplimit = params.get("iplimit");
	
	int rstate = Util.getInteger(params.get("state"));
	
	DbCommand cmd = db.getSqlStringCommand("SELECT * FROM cms_assort WHERE NO = @NO AND ROW_ID <> @ROW_ID");
	cmd.addInParameter("NO", no);
	cmd.addInParameter("ROW_ID", rowid);
	DbMap tempMap = cmd.executeDbMap();
	
	if(tempMap != null) msg.putMessage("系统已经存在此编号文章分类!!");
	else {
		cmd = db.getSqlStringCommand("UPDATE cms_assort SET NAME=@NAME,NO=@NO,DES=@DES,IPLIMIT=@IPLIMIT,STATE=@STATE," +
				"UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME WHERE ROW_ID =@ROW_ID");
		cmd.addInParameter("ROW_ID", rowid);
		cmd.addInParameter("NAME", mc);
		cmd.addInParameter("NO", no);
		cmd.addInParameter("DES", des);
		cmd.addInParameter("IPLIMIT", iplimit);
		cmd.addInParameter("STATE", rstate);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.executeNonQuery();
		
		addLog("修改文章分类：" + mc , "4003");
		
		msg.setIsSuccess(true);
	}
	
	entity = db.executeDbMap("SELECT * FROM cms_assort WHERE ROW_ID = ?", rowid);
}else if ("doadd".equals(act)){
	String mc = params.get("mc");
	String no = params.get("no");
	String des = params.get("des");
	String iplimit = params.get("iplimit");
	
	int rstate = Util.getInteger(params.get("state"));
	
	DbMap tempMap = db.executeDbMap("SELECT * FROM cms_assort WHERE NO = ?", no);
	if(tempMap != null) msg.putMessage("系统已经存在此编号文章分类!!");
	else {
		String uuid = Util.getUUID();
		
		DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_assort(ROW_ID,NO,NAME,PARENTID,UPDATEID,UPDATETIME,PATH,CHILDREN,DES,SERIAL,IPLIMIT,STATE) VALUES(" +
				"@ROW_ID,@NO,@NAME,@PARENTID,@UPDATEID,@UPDATETIME,@PATH,@CHILDREN,@DES,0,@IPLIMIT,@STATE)");
		cmd.addInParameter("ROW_ID", uuid);
		cmd.addInParameter("NO", no);
		cmd.addInParameter("NAME", mc);
		cmd.addInParameter("PARENTID", parent == null ? "0" : parent.getString("ROW_ID"));
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.addInParameter("PATH", parent == null ? uuid + "," : parent.getString("PATH") + uuid + ",");
		cmd.addInParameter("CHILDREN", 0);
		cmd.addInParameter("DES", des);
		cmd.addInParameter("IPLIMIT", iplimit);
		cmd.addInParameter("STATE", rstate);
		
		cmd.executeNonQuery();
		
		if(parent != null){
			db.executeNonQuery("UPDATE cms_assort SET CHILDREN = CHILDREN + 1 WHERE ROW_ID = ?", parent.getString("ROW_ID"));
		}
		
		addLog("添加文章分类：" + mc , "4003");
		
		msg.setIsSuccess(true);
	}
}else if("ajax".equals(act)){
	String where = "1".equals(current.get("DWID")) ? "" :  "AND da.DWID = '" + current.get("DWID") + "' AND a.ROW_ID IN(SELECT ASSORTID FROM cms_user_assort WHERE USERID = '" + current.get("ROW_ID") + "')";
	List<DbMap> list = db.executeDbMapList("SELECT a.* FROM cms_assort a, cms_dw_assort da WHERE a.ROW_ID = da.ASSORTID AND a.PARENTID = '" + parentid + "' " + where + " ORDER BY a.NO ASC");
	out.print(new Gson().toJson(list));
	return;
}
%>