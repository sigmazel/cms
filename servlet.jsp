﻿<%@ page language="java" pageEncoding="utf-8"%>
<%@ page import="com.google.gson.*"%>
<%@ page import="com.fairy.lib.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.io.*"%>
<%@ page import="javax.servlet.http.*"%>

<%@ page import="java.text.SimpleDateFormat"%>

<%@ page import="org.apache.velocity.VelocityContext"%>
<%@ page import="org.apache.velocity.app.Velocity"%>

<%!
private Database db;
private ActionMessage msg;
private Parameters params;

private HttpServletRequest pri_request;
private HttpServletResponse pri_response;

public String get_client_ip() { 
	String ip = pri_request.getHeader("x-forwarded-for"); 
	
	if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) ip = pri_request.getHeader("Proxy-Client-IP");
	if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) ip = pri_request.getHeader("WL-Proxy-Client-IP"); 
	if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) ip = pri_request.getRemoteAddr(); 
	
	return ip; 
}


public DbMap assort_id(String id) {
	return db.executeDbMap("SELECT * FROM cms_assort WHERE ROW_ID = ?", id);
}

public DbMap assort_no(String no) {
	return db.executeDbMap("SELECT * FROM cms_assort WHERE `NO` = ?", no);
}

public boolean check_ip(DbMap map){
	if(Util.empty(map.get("IPLIMIT"))) return true;
	
	boolean passed = false;
	String clientIP = this.get_client_ip();
	
	long ipOfLong = Util.ip2long(clientIP);
	String[] ipLimits = map.getString("IPLIMIT").split("\n");
	
	for(String ipLimit : ipLimits){
		if(Util.empty(ipLimit) || ipLimit.length() < 9) continue;
		
		ipLimit = ipLimit.replaceAll("[^\\w\\.]", "");
		
		String[] sections = ipLimit.split("-");
		if(sections.length == 2){
			if(ipOfLong >= Util.ip2long(sections[0]) && ipOfLong <= Util.ip2long(sections[1])){
				passed = true;
				break;
			}
		}else if(ipOfLong == Util.ip2long(sections[0])){
			passed = true;
			break;
		}
	}
	
	return passed;
}

public boolean check_new(DbMap map){
	if(Util.empty(map.get("PUBDATE")) || !Util.isDateString(map.getString("PUBDATE"))) return false;
	
	long subDays = 10;
	
	try{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		java.util.Date pubDate = format.parse(map.getString("PUBDATE"));
		java.util.Date nowDate = Calendar.getInstance().getTime();
		
		subDays = (nowDate.getTime() - pubDate.getTime()) / (1000 * 60 * 60 * 24);
	}catch(Exception ex){
		
	}
	
	return subDays <= 5 ? true : false;
}

public List<DbMap> assort_crumbs(String id) {
	DbMap assort = db.executeDbMap("SELECT * FROM cms_assort WHERE ROW_ID = ?", id);
	return db.executeDbMapList("SELECT * FROM cms_assort WHERE INSTR('" + assort.getString("PATH")+ "', PATH) = 1 ORDER BY `NO` ASC");
}

public List<List<DbMap>> array_chunk(List<DbMap> arrayList, int chunkSize){
	List<List<DbMap>> tempList = new ArrayList<List<DbMap>>();
	
	if(chunkSize <= 1 || chunkSize >= arrayList.size()) tempList.add(arrayList);
	else{
		int count = -1;
		List<DbMap> ctempList = new ArrayList<DbMap>();
		
		for(int i = 0; i < arrayList.size(); i++){
			if(i % chunkSize == 0){
				count++;
				if(i > 0) tempList.add(ctempList);
				ctempList = new ArrayList<DbMap>();
			}
			
			ctempList.add(arrayList.get(i));
		}
		
		tempList.add(ctempList);
	}
	
	return tempList;
}

public List<DbMap> assorts_id(String parentid) {
	return db.executeDbMapList("SELECT * FROM cms_assort WHERE PARENTID = ?  ORDER BY `NO` ASC", parentid);
}

public List<List<DbMap>> assorts_id(String parentid, int chunkSize){
	return array_chunk(db.executeDbMapList("SELECT * FROM cms_assort WHERE PARENTID = ?  ORDER BY `NO` ASC", parentid), chunkSize);
}


public List<DbMap> assorts_no(String no) {
	return db.executeDbMapList("SELECT a.* FROM cms_assort a, cms_assort b WHERE a.PARENTID = b.ROW_ID AND b.`NO` = ?  ORDER BY `NO` ASC", no);
}

public List<List<DbMap>> assorts_no(String no, int chunkSize){
	return assorts_no(no, chunkSize, "");
}

public List<List<DbMap>> assorts_no(String no, int chunkSize, String where){
	return array_chunk(db.executeDbMapList("SELECT a.* FROM cms_assort a, cms_assort b WHERE a.PARENTID = b.ROW_ID " + where + " AND b.`NO` = ?  ORDER BY `NO` ASC", no), chunkSize);
}

public List<DbMap> assorts_all() {
	return db.executeDbMapList("SELECT * FROM cms_assort ORDER BY PARENTID ASC, `NO` ASC");
}

public List<List<DbMap>> assorts_all(int chunkSize){
	return array_chunk(db.executeDbMapList("SELECT * FROM cms_assort ORDER BY PARENTID ASC, `NO` ASC"), chunkSize);
}

public DbMap assort_article(String no){
	return assort_article(no, "");
}

public DbMap assort_article(String no, String where){
	String nowDate = Util.format(Util.getDate(), "yyyy-MM-dd");
	where = Util.empty(no) ? where : where + " AND b.NO = '" + no + "'";
	DbMap article = db.executeDbMap("SELECT a.*, b.NO, b.NAME FROM cms_article a, cms_assort b WHERE a.ASSORTID = b.ROW_ID AND (a.INDATE = '1900-01-01' OR a.INDATE >= '" + nowDate + "') AND a.STATE = 1 AND a.ISDELETED = 0 " + where + " ORDER BY a.ISTOP DESC, a.PUBDATE DESC, a.CREATETIME DESC LIMIT 0, 1");
	return article_format(article);
}

public DbMap article_format(DbMap article){
	if(article == null) return null;
	
	String indate = article.getString("INDATE");
	if(indate.length() < 10) indate = "1900-01-01";
	else indate = indate.substring(0, 10);
	
	if("1900-01-01".equals(indate)) article.put("INDATE", "");
	else article.put("INDATE", indate);
	
	if(!Util.empty(article.getString("IMG"))){
		String imgPath = article.getString("IMG");
		imgPath = imgPath.replaceAll("\r\n", "");
		imgPath = imgPath.replaceAll("\r", "");
		imgPath = imgPath.replaceAll("\n", "");
		
		String fileExt = imgPath.substring(imgPath.lastIndexOf(".") + 1).toLowerCase();
		if(imgPath.indexOf(".n.") != -1) article.put("IMG_SRC", imgPath.substring(0, imgPath.lastIndexOf(".")- 2) + "." + fileExt);
		else article.put("IMG_SRC", imgPath);
	}else article.put("IMG_SRC", "");
	
	String[] imgList = Util.empty(article.getString("IMGLIST")) ? new String[]{} : article.getString("IMGLIST").split(",");
	
	for(int i = 0; i < imgList.length; i++){
		imgList[i] = imgList[i].replaceAll("\r\n", "");
		imgList[i] = imgList[i].replaceAll("\r", "");
		imgList[i] = imgList[i].replaceAll("\n", "");
	}
	
	article.put("IMGLIST", imgList);
	
	if(article.getString("SUBJECT").indexOf("/") != -1){
		String[] subjects = article.getString("SUBJECT").split("/");
		article.put("TITLE", "<span class='subject'>【" + subjects[1] + "】</span>" + article.getString("TITLE"));
		article.put("SUBJECT", subjects[0]);
	}
	
	return article;
}

public int article_count(String no, String where){
	String nowDate = Util.format(Util.getDate(), "yyyy-MM-dd");
	where = Util.empty(no) ? where : where + " AND b.`NO` = '" + no + "'";
	return Util.getInteger(db.executeScalar("SELECT COUNT(1) FROM cms_article a, cms_assort b WHERE a.ASSORTID = b.ROW_ID AND (a.INDATE = '1900-01-01' OR a.INDATE >= '" + nowDate + "') AND a.STATE = 1 AND a.ISDELETED = 0 " + where));
}

public List<DbMap> article_list(String no){
	return article_list(no, new Pager(1, 1000), "", "", false);
}

public List<DbMap> article_list(String no, int pageSize){
	return article_list(no, new Pager(1, pageSize), "", "", false);
}

public List<DbMap> article_list(String no, Pager pager){
	return article_list(no, pager, "", "", false);
}

public List<DbMap> article_list(String no, int pageSize, String where){
	return article_list(no, new Pager(1, pageSize), where, "", false);
}

public List<DbMap> article_list(String no, Pager pager, String where){
	return article_list(no, pager, where, "", false);
}

public List<DbMap> article_list(String no, int pageSize, String where, String order){
	return article_list(no, new Pager(1, pageSize), where, order, false);
}

public List<DbMap> article_list(String no, Pager pager, String where, String order){
	return article_list(no, pager, where, order, false);
}

public List<DbMap> article_list(String no, Pager pager, String where, String order, boolean fetchAll){
	String nowDate = Util.format(Util.getDate(), "yyyy-MM-dd");
	order = Util.empty(order) ? "ORDER BY a.ISTOP DESC, a.PUBDATE DESC, a.CREATETIME DESC" : order;
	
	where = where + " AND (a.INDATE = '1900-01-01' OR a.INDATE >= '" + nowDate + "') AND a.STATE = 1 AND a.ISDELETED = 0 ";
	where = Util.empty(no) ? where : where + " AND c.NO = '" + no + "'";
	
	String columns = "a.ROW_ID, a.SUMMARY, a.SUBJECT, a.TITLE, a.PUBDATE, a.IMG, a.LINK, a.SOURCE, a.HITS, a.WRITER, a.AUTHOR, c.NO, c.NAME";
	if(fetchAll) columns = "a.ROW_ID, a.SUMMARY, a.CONTENT, a.SUBJECT, a.TITLE, a.PUBDATE, a.IMG, a.LINK, a.SOURCE, a.HITS, a.WRITER, a.AUTHOR, c.NO, c.NAME";
	
	List<DbMap> articles = db.executeDbMapPager("SELECT " + columns + " FROM  cms_article a, cms_assort c WHERE a.ASSORTID = c.ROW_ID " + where + " " + order, pager);

	for(DbMap article : articles){
		article = article_format(article);
	}
	
	return articles;
}

public List<List<DbMap>> article_chunk(String no, Pager pager, int chunkSize){
	return array_chunk(article_list(no, pager, "", "", false), chunkSize);
}

public List<List<DbMap>> article_chunk(String no, Pager pager, String where, int chunkSize){
	return array_chunk(article_list(no, pager, where, "", false), chunkSize);
}

public List<List<DbMap>> article_chunk(String no, Pager pager, String where, String order, int chunkSize){
	return array_chunk(article_list(no, pager, where, order, false), chunkSize);
}

public List<List<DbMap>> article_chunk(String no, Pager pager, String where, String order, boolean fetchAll, int chunkSize){
	return array_chunk(article_list(no, pager, where, order, fetchAll), chunkSize);
}

public DbMap article_id(String id) {
	DbMap article = db.executeDbMap("SELECT a.*, b.NO, b.NAME FROM cms_article a, cms_assort b WHERE a.ASSORTID = b.ROW_ID AND a.ROW_ID = ?", id);
	return article_format(article);
}

public DbMap article_prev(DbMap article) throws Exception{
	String pubdate = article.getString("PUBDATE");
	DbMap article_prev = db.executeDbMap("SELECT a.*, b.NO, b.NAME FROM cms_article a, cms_assort b WHERE a.ASSORTID = b.ROW_ID AND a.ASSORTID = '" + article.getString("ASSORTID") + "' AND a.PUBDATE >= '" + pubdate + "' AND a.ROW_ID <> '" + article.getString("ROW_ID") + "' ORDER BY a.PUBDATE ASC, a.CREATETIME ASC LIMIT 0, 1");
	
	return article_format(article_prev);
}

public DbMap article_next(DbMap article){
	String pubdate = article.getString("PUBDATE");
	DbMap article_next = db.executeDbMap("SELECT a.*, b.NO, b.NAME FROM cms_article a, cms_assort b WHERE a.ASSORTID = b.ROW_ID AND a.ASSORTID = '" + article.getString("ASSORTID") + "' AND a.PUBDATE <= '" + pubdate + "' AND a.ROW_ID <> '" + article.getString("ROW_ID") + "' ORDER BY a.PUBDATE DESC, a.CREATETIME DESC LIMIT 0, 1");
	
	return article_format(article_next);
}

public void article_hits(String id) {
	db.executeNonQuery("UPDATE cms_article SET HITS = HITS + 1 WHERE ROW_ID = ?", id);
}

public List<DbMap> article_ranks(String wheresql){
	return article_ranks(wheresql, "dw");
}

public List<DbMap> article_ranks(String wheresql, String type){
	return article_ranks(wheresql, type, 1);
}

public List<DbMap> article_ranks(String wheresql, String type, int order){
	//type:dw, assort, source
	List<DbMap> rank_month = new ArrayList<DbMap>();
	List<DbMap> rank_year = new ArrayList<DbMap>();
	
	String sql_month = wheresql + " AND DATE_FORMAT(a.PUBDATE, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m')";
	String sql_year = wheresql + " AND DATE_FORMAT(a.PUBDATE, '%Y') = DATE_FORMAT(NOW(), '%Y')";
	
	if("dw".equals(type)){
		rank_month = db.executeDbMapList("SELECT * FROM (SELECT COUNT(1) CNT1, a.DWID, c.DWMC, c.DWBH FROM cms_article a, cms_assort b, cms_dw c WHERE a.STATE = 1 AND a.ISDELETED = 0 AND a.ASSORTID = b.ROW_ID AND a.DWID = c.ROW_ID " + sql_month + " GROUP BY a.DWID) AS tmp ORDER BY CNT1 DESC");
		rank_year = db.executeDbMapList("SELECT * FROM (SELECT COUNT(1) CNT2, a.DWID, c.DWMC, c.DWBH FROM cms_article a, cms_assort b, cms_dw c WHERE a.STATE = 1 AND a.ISDELETED = 0 AND a.ASSORTID = b.ROW_ID AND a.DWID = c.ROW_ID " + sql_year + " GROUP BY a.DWID) AS tmp ORDER BY CNT2 DESC");
		
		int imonth = 1;
		int iyear = 1;
		
		for(DbMap mitem : rank_month){
			mitem.put("RANK1", imonth);
			imonth++;
		}
		
		for(DbMap yitem : rank_year){
			yitem.put("RANK2", iyear);
			iyear++;
		}
		
		if(rank_month.size() == 0) order = 2;
		
		if(order == 1){
			for(DbMap mitem : rank_month){
				mitem.put("CNT2", 0);
				
				for(DbMap yitem : rank_year){
					if(yitem.getString("DWID").equals(mitem.getString("DWID"))){
						mitem.put("CNT2", yitem.get("CNT2"));
						mitem.put("RANK2", yitem.get("RANK2"));
						break;
					}
				}
			}
		}else{
			for(DbMap yitem : rank_year){
				yitem.put("CNT1", 0);
				
				for(DbMap mitem : rank_month){
					if(mitem.getString("DWID").equals(yitem.getString("DWID"))){
						yitem.put("CNT1", mitem.get("CNT1"));
						yitem.put("RANK1", mitem.get("RANK1"));
						break;
					}
				}
			}
		}
	}else if("assort".equals(type)){
		rank_month = db.executeDbMapList("SELECT * FROM (SELECT COUNT(1) CNT1, a.ASSORTID, b.NO, b.NAME FROM cms_article a, cms_assort b, cms_dw c WHERE a.STATE = 1 AND a.ISDELETED = 0 AND a.ASSORTID = b.ROW_ID AND a.DWID = c.ROW_ID " + sql_month + " GROUP BY a.ASSORTID) AS tmp ORDER BY CNT1 DESC");
		rank_year = db.executeDbMapList("SELECT * FROM (SELECT COUNT(1) CNT2, a.ASSORTID, b.NO, b.NAME FROM cms_article a, cms_assort b, cms_dw c WHERE a.STATE = 1 AND a.ISDELETED = 0 AND a.ASSORTID = b.ROW_ID AND a.DWID = c.ROW_ID " + sql_year + " GROUP BY a.ASSORTID) AS tmp ORDER BY CNT2 DESC");


		int imonth = 1;
		int iyear = 1;
		
		for(DbMap mitem : rank_month){
			mitem.put("RANK1", imonth);
			imonth++;
		}
		
		for(DbMap yitem : rank_year){
			yitem.put("RANK2", iyear);
			iyear++;
		}
		
		if(rank_month.size() == 0) order = 2;
		
		if(order == 1){
			for(DbMap mitem : rank_month){
				mitem.put("CNT2", 0);
				
				for(DbMap yitem : rank_year){
					if(yitem.getString("ASSORTID").equals(mitem.getString("ASSORTID"))){
						mitem.put("CNT2", yitem.get("CNT2"));
						mitem.put("RANK2", yitem.get("RANK2"));
						break;
					}
				}
			}
		}else{
			for(DbMap yitem : rank_year){
				yitem.put("CNT1", 0);
				
				for(DbMap mitem : rank_month){
					if(mitem.getString("ASSORTID").equals(yitem.getString("ASSORTID"))){
						yitem.put("CNT1", mitem.get("CNT1"));
						yitem.put("RANK1", mitem.get("RANK1"));
						break;
					}
				}
			}
		}
	}else if("source".equals(type)){
		rank_month = db.executeDbMapList("SELECT * FROM (SELECT COUNT(1) CNT1, a.SOURCE FROM cms_article a, cms_assort b, cms_dw c WHERE a.STATE = 1 AND a.ISDELETED = 0 AND a.ASSORTID = b.ROW_ID AND a.DWID = c.ROW_ID " + sql_month + " AND LENGTH(a.SOURCE) > 0 GROUP BY a.SOURCE) AS tmp ORDER BY CNT1 DESC");
		rank_year = db.executeDbMapList("SELECT * FROM (SELECT COUNT(1) CNT2, a.SOURCE FROM cms_article a, cms_assort b, cms_dw c WHERE a.STATE = 1 AND a.ISDELETED = 0 AND a.ASSORTID = b.ROW_ID AND a.DWID = c.ROW_ID " + sql_year + " AND LENGTH(a.SOURCE) > 0 GROUP BY a.SOURCE) AS tmp ORDER BY CNT2 DESC");


		int imonth = 1;
		int iyear = 1;
		
		for(DbMap mitem : rank_month){
			mitem.put("RANK1", imonth);
			imonth++;
		}
		
		for(DbMap yitem : rank_year){
			yitem.put("RANK2", iyear);
			iyear++;
		}
		
		if(rank_month.size() == 0) order = 2;
		
		if(order == 1){
			for(DbMap mitem : rank_month){
				mitem.put("CNT2", 0);
				
				for(DbMap yitem : rank_year){
					if(yitem.getString("SOURCE").equals(mitem.getString("SOURCE"))){
						mitem.put("CNT2", yitem.get("CNT2"));
						mitem.put("RANK2", yitem.get("RANK2"));
						break;
					}
				}
			}
		}else{
			for(DbMap yitem : rank_year){
				yitem.put("CNT1", 0);
				
				for(DbMap mitem : rank_month){
					if(mitem.getString("SOURCE").equals(yitem.getString("SOURCE"))){
						yitem.put("CNT1", mitem.get("CNT1"));
						yitem.put("RANK1", mitem.get("RANK1"));
						break;
					}
				}
			}
		}
	}
	
	return order == 1 ? rank_month : rank_year;
}

public DbMap user_id(String userid) {
	return db.executeDbMap("SELECT a.* FROM cms_user a WHERE a.ROW_ID = ?", userid);
}

public int message_count(String id, String where){
	where = Util.empty(id) ? where : where + " AND a.ARTICLEID = '" + id + "'";
	return Util.getInteger(db.executeScalar("SELECT COUNT(1) FROM cms_message a, cms_article b WHERE a.ARTICLEID = b.ROW_ID AND a.ISOPEN = 1 " + where));
}

private List<DbMap> message_list(String id, Pager pager){
	return message_list(id, pager, "", "");
}

public List<DbMap> message_list(String id, Pager pager, String where){
	return message_list(id, pager, where, "");
}

public List<DbMap> message_list(String id, Pager pager, String where, String order){
	order = Util.empty(order) ? "ORDER BY a.CREATETIME DESC" : order;
	
	where = where + " AND a.ISOPEN = 1 ";
	where = Util.empty(id) ? where : where + " AND a.ARTICLEID = '" + id + "'";
	
	return db.executeDbMapPager("SELECT a.* FROM cms_message a, cms_article b WHERE a.ARTICLEID = b.ROW_ID AND a.ISOPEN = 1 " + where + " " + order, pager);
}

public ActionMessage message_add() throws IOException{
	params = new Parameters(pri_request);
	msg = new ActionMessage();
	
	if(Util.empty(params.get("title"))) msg.setMessage("标题不能为空！");
	else if(Util.empty(params.get("writer"))) msg.setMessage("联系人不能为空！");
	else if(Util.empty(params.get("phone"))) msg.setMessage("联系电话不能为空！");
	else if(Util.empty(params.get("content"))) msg.setMessage("信件内容不能为空！");
	else{
		DbMap dw = db.executeDbMap("SELECT * FROM cms_dw WHERE PREDWID = '0'");
		
		DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_message(ROW_ID,TITLE,CONTENT,STATE,WRITER,PHONE,MOBILE,CREATETIME,UPDATEID,UPDATETIME,ARTICLEID,DWID,HITS,PARENTID,ISOPEN) VALUES(" +
		"@ROW_ID,@TITLE,@CONTENT,0,@WRITER,@PHONE,@MOBILE,@CREATETIME,@UPDATEID,@UPDATETIME,@ARTICLEID,@DWID,0,0,0)");
		cmd.addInParameter("ROW_ID", Util.getUUID());
		cmd.addInParameter("TITLE", Util.utf8Substring(Util.stripTags(params.get("title")), 50));
		cmd.addInParameter("WRITER", Util.utf8Substring(Util.stripTags(params.get("writer")), 20));
		cmd.addInParameter("PHONE", Util.utf8Substring(Util.stripTags(params.get("phone")), 30));
		cmd.addInParameter("MOBILE", Util.utf8Substring(Util.stripTags(params.get("mobile")), 30));
		cmd.addInParameter("CONTENT", Util.stripTags(params.get("content")));
		cmd.addInParameter("CREATETIME", Util.getDate());
		cmd.addInParameter("UPDATEID", 0);
		cmd.addInParameter("UPDATETIME", Util.getDate());
		cmd.addInParameter("ARTICLEID", params.get("id"));
		cmd.addInParameter("DWID", dw.get("ROW_ID"));
			
		cmd.executeNonQuery();
		
		msg.setMessage("您已经成功提交信件，我们收到您的信件后会及时回复！");
	}
	
	return msg;
}

public void href(String url) throws IOException{
	pri_response.getWriter().println("<script type=\"text/javascript\">location.href='" + url + "';</script>");
}

public void redirect(String url) throws IOException{
	pri_response.sendRedirect(url);
}

public void forward(String url) throws ServletException, IOException{
	getServletContext().getRequestDispatcher(pri_request.getContextPath() + url).forward(pri_request, pri_response);
}

public void assign(String key, Object value){
	pri_request.setAttribute(key, value);
}

public DbMap setting(){
	List<DbMap> dbMapList = db.executeDbMapList("SELECT * FROM cms_variable");
	DbMap settings = new DbMap();
	for(DbMap dbMap : dbMapList) settings.put(dbMap.getString("IDENTITY"), dbMap.getString("VAL"));
	
	return settings;
}

public DbMap flash_view(DbMap setting){
	String now_date = Util.getDateString();
	Object views = db.executeScalar("SELECT VIEWS FROM cms_view WHERE DATELINE = '" + now_date.substring(0, 10) + "'");
	
	if(views == null) db.executeNonQuery("INSERT INTO cms_view(DATELINE, VIEWS) VALUES('" + now_date.substring(0, 10) + "', 1)");
	else db.executeNonQuery("UPDATE cms_view SET VIEWS = VIEWS + 1 WHERE DATELINE = '" + now_date.substring(0, 10) + "'");
	
	if(!Util.empty(setting.get("counter"))){
		setting.set("counter", setting.getInteger("counter") + 1);
		db.executeNonQuery("UPDATE cms_variable SET VAL = " + setting.get("counter") + " WHERE IDENTITY = 'counter'");
	}
	
	setting.set("counter_today", Util.getInteger(views) + 1);
	
	return setting;
}
%>
<%
response.setContentType("text/html;charset=utf-8");
request.setCharacterEncoding("utf-8");

String _rootpath = request.getAttribute("_ROOT_").toString();
String _filepath = request.getAttribute("_FILE_").toString();

ConnectSetting connectSetting = new ConnectSetting("cms_java");

connectSetting.setHost("localhost");
connectSetting.setPort("3306");
connectSetting.setUser("root");
connectSetting.setPassword("");
connectSetting.setPool("50");
connectSetting.setDatabaseName("cms");
connectSetting.setProvider("mysql");

db = DatabaseFactory.create(connectSetting);

msg = new ActionMessage();
params = new Parameters(request);

pri_request = request;
pri_response = response;

DbMap settingMap = flash_view(this.setting());

String velocityResult = "";

VelocityContext executor = new VelocityContext();
StringWriter writer = new StringWriter();

executor.put("fn", this);
executor.put("util", new Util());
executor.put("pager", new Pager(params.get("pid")));

executor.put("out", out);
executor.put("request", request);
executor.put("response", response);
executor.put("params", params);
executor.put("session", session);
executor.put("current", (DbMap)session.getAttribute("current"));
executor.put("page_title", settingMap.getString("sysname"));
executor.put("setting", settingMap);

executor.put("THEME", request.getContextPath() + "/tpl/sc" + _filepath.substring(0, _filepath.lastIndexOf("/") + 1) + "$THEME");
Velocity.evaluate(executor, writer, "_parsed", Util.getFileContents(_rootpath + "tpl/sc" + _filepath));

velocityResult = writer.toString();

out.print(velocityResult);
%>