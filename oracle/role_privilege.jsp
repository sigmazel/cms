<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_role.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
		<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
		<form name="formlist" id="formlist" action="role_privilege.jsp?act=dosetup&rowid=<%=params.get("rowid")%>" method="post">
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;用户管理 -&gt; 角色 -&gt;
						<font color="#FF0000"><%=entity.get("NAME") %></font>
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
			</table>
			<br>
			<table width="95%" align="center" border="0" class="TableList" cellpadding="0" cellspacing="0">
				<tr align="center">
					<td height="21" width="30" class="biaot">
						编号
					</td>
					<td width="160" class="biaot">
						资源
					</td>
					<td class="biaot">
						权限
					</td>

				</tr>
					<%for(int i = 0; i < itemList.size();i++){
						DbMap item = (DbMap)itemList.get(i);
						List priList = (List)item.get("priList");
					%>
					<tr bgcolor="#FFFFFF" >
						<td height="21" class="biaol" align="center">
							<%=i+1 %>
						</td>
						<td class="biaol">
							<%=item.get("NAME")%>
						</td>
						<td class="biaol">
							<%for(int j = 0; j < priList.size();j++){
								DbMap priitem = (DbMap)priList.get(j);
							%>
							<input type="checkbox" name="checkbox" id="c<%=priitem.get("ROW_ID")%>" value="<%=priitem.get("ROW_ID")%>" <%if(privilegeString.indexOf("|" + priitem.getString("ROW_ID") + "|") != -1){%>checked="checked"<%}%>>&nbsp;<%=priitem.get("NAME")%>&nbsp;&nbsp;
                			<%} %>
						</td>
					</tr>
					<%} %>
			</table>
			<table width="95%" align="center" border="0" cellpadding="0"
				cellspacing="0">
				<tr>
					<td width="4%" height="25" valign="bottom">
						<input type="checkbox" name="toggleAll" onClick="ToggleAll(this);">
					</td>
					<td width="96%" valign="bottom">
						全选
					</td>

				</tr>
			</table>
			<table width="95%" align="center" border="0" cellpadding="0"
				cellspacing="0">
				<tr>
					<td width="100%" colspan="2" align="center">
						<%if(operations.indexOf("|3003,setup|") != -1 || "1".equals(current_userid)){%>
						<input type="submit" class="inputbox2" value=" 提 交 ">
						<%} %>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" value=" 返 回 " onClick="location='role_list.jsp?act=list'" class="inputbox2">
					</td>
				</tr>
			</table>
			<br />
		</form>
		<%if(!msg.emptyMessage()){%>
		<script type="text/javascript">
		alert('<%=msg.getMessage()%>');
		</script>
		<%}%>
		<%if(msg.getIsSuccess()){ %>
		<script type="text/javascript">
		location.href='role_list.jsp?act=list';
		</script>
		<%} %>
	</body>
</html>
