<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_dw.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">

		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
					&nbsp;&nbsp;用户管理 
					-&gt; <a href="dw_list.jsp?act=list">单位</a> 
					<%for(int i = 0; i < crumbs.size();i++ ){
					DbMap crumb = (DbMap)crumbs.get(i);
					%>
					-&gt; <a href="dw_list.jsp?act=list&predwid=<%=crumb.get("ROW_ID")%>"><%=crumb.get("DWMC")%></a>
					<%} %>
					-&gt; 搜索
				</td>
			</tr>
			<tr>
				<td height="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td">
					&nbsp;
					<a href="dw_list.jsp?act=list&predwid=<%=params.get("predwid")%>">列表</a> |&nbsp;
					<font color="#FF0000">搜索</font>
				</td>
			</tr>
		</table>
		<br>
		<form action="dw_list.jsp?act=dosearch&predwid=<%=params.get("predwid")%>" method="post">
			<table width="95%" align="center" border="0" cellpadding="0"
				cellspacing="0">
				<tr>
					<td align="center" class="title_td1">
						<br>
						<br>
						<br>
						<table width="75%" align="center" border="0" cellpadding="0"
							cellspacing="0">
							<tr>
								<td width="35%" height="30" align="right">
									编号：
								</td>
								<td width="65%" align="left">
									<input type="text" name="dwbh" class="inputbox1" size="10">
								</td>
							</tr>
							<tr>
								<td width="35%" height="30" align="right">
									名称：
								</td>
								<td width="65%" align="left">
									<input type="text" name="dwmc" class="inputbox1" size="50">
								</td>
							</tr>
						</table>

						<p>
							<%if(operations.indexOf("|3002,search|") != -1 || "1".equals(current_userid)){%>
								<input type="submit" class="inputbox2" value=" 提 交 ">
							<%} %>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" class="inputbox2" value=" 返 回 " onclick="location='dw_list.jsp?act=list&predwid=<%=params.get("predwid")%>'">
							<br>
							<br>
						</p>
						<p>
							&nbsp;
							<br>
							<br>
						</p>

					</td>
				</tr>

			</table>
		</form>
	</body>

</html>
