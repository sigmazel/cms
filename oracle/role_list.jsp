<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_role.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>###</title>
	<link href="images/style.css" rel="stylesheet" type="text/css">
	<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
	<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
	<script language="javascript" type="text/javascript">
	    function del() {
	        if (0 == listcount) {
	            alert("请选择记录。")
	            return;
	        }
	        if (confirm("确认进行删除吗？")) {
	            document.forms[0].submit();
	        }
	    }
	    function delone(urls) {
	        if (confirm("确认进行删除吗？"))	{
				 location = urls;
			}
	    }
		</script>
</head>
	<body  topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
					&nbsp;&nbsp;用户管理 -&gt; 角色管理
				</td>
			</tr>
			<tr>
				<td height="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td">
					&nbsp;
					<a href="role_list.jsp?act=list"><font color="#FF0000">列表</font></a> |&nbsp;
					<%if(operations.indexOf("|3003,add|") != -1 || "1".equals(current_userid)){%>
					<a href="role_add.jsp?act=add">添加</a> |&nbsp;
					<%} %>
					<%if(operations.indexOf("|3003,delete|") != -1 || "1".equals(current_userid)){%>
					<a href="javascript:del()">删除</a> |&nbsp;
					<%} %>
				</td>
			</tr>
		</table>
		<br/>
		<form name="formlist" id="formlist"  action="role_list.jsp?act=deletelist" method="post">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<table width="95%" align="center" border="0" class="TableList"
						cellpadding="0" cellspacing="0">
						<tr align="center">
							<td width="30"  height="21" class="biaot">选择</td>
							<td width="30"  height="21" class="biaot">序号</td>
							<td width="120" class="biaot">
								名称
							</td>
							<td class="biaot">
								备注
							</td>
							<td width="120" class="biaot">
								更新人
							</td>
							<td width="100" class="biaot">
								更新时间
							</td>
							<td class="biaot1" width="120">
								操作
							</td>
						</tr>
						<%for(int i = 0; i < itemList.size();i++){
							DbMap item = (DbMap)itemList.get(i);
						%>
							<tr bgcolor="#FFFFFF" onMouseOver="overtable(this);"
									onMouseOut="outtable(this);" id="<%=i + 1%>">
								<td height="21" class="biaol" align="center">
									<input type="checkbox" name="checkbox" id="c<%=i + 1%>" value="<%=item.get("ROW_ID")%>" onClick="selectbox(this)">
								</td>
								<td height="21" class="biaol" align="center">
								<%=i + 1%>
								</td>
								<td class="biaol">
								<%=item.get("NAME")%>
								</td>
								<td class="biaol">
								&nbsp;<%=item.get("DES")%>
								</td>
								<td class="biaol" align="center">
								<%=item.get("EDITER")%>
								</td>
								<td class="biaol" align="center">
								<%=Util.format(item.get("UPDATETIME"), "yyyy-MM-dd h:i")%>
								</td>
								<td class="biaol" align="center">&nbsp;
								<%if(operations.indexOf("|3003,update|") != -1 || "1".equals(current_userid)){%>
								<a href="role_update.jsp?act=update&rowid=<%=item.get("ROW_ID")%>">修改</a>
								<%} %>
								<%if(operations.indexOf("|3003,setup|") != -1 || "1".equals(current_userid)){%>
								<a href="role_privilege.jsp?act=setup&rowid=<%=item.get("ROW_ID")%>">权限</a>
								<%} %>
								<%if(operations.indexOf("|3003,delete|") != -1 || "1".equals(current_userid)){%>
								<a href="javascript:delone('role_list.jsp?checkbox=<%=item.get("ROW_ID")%>&act=deletelist')">删除</a>
								<%} %>
								</td>
							</tr>
					  <%} %>
					</table>
				</td>
			</tr>

		</table>
		<table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
            <td width="4%" height="25" valign="bottom">
            <input type="checkbox" name="toggleAll" onClick="ToggleAll(this);">
            </td>
            <td width="96%" valign="bottom">全选</td>
        </tr>
        </table>
		</form>

</body>
</html>