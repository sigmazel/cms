<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
String act = params.get("act");
List<DbMap> itemList = new ArrayList<DbMap>();
Pager pager = new Pager(params.get("pid"), Util.getInteger(params.get("psize")) < 10 ? 10 : Util.getInteger(params.get("psize")));
DbMap entity = null;
DbMap parent = null;
pager.setQueryString("act=" + params.get("act"));


Parameters search = new Parameters();
if("list".equals(act)){
	search = (Parameters) session.getAttribute("member_search");

	String where = "";
	if(search == null) search = new Parameters();
	
	if (!Util.empty(search.get("keyword"))) where += " AND USERNAME || REALNAME || EMAIL LIKE '%" + search.get("keyword") + "%'";
	if (!Util.empty(search.get("kstime"))) where += " AND CREATETIME >= '" + search.get("kstime") + "'";
	if (!Util.empty(search.get("jstime"))) where += " AND CREATETIME <= '" + search.get("jstime") + "'";
	
	List<?> list = db.executeDbMapPager("SELECT * FROM cms_member WHERE 1 = 1" + where + " ORDER BY CREATETIME DESC", pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		
		Object editer = db.executeScalar("SELECT NAME FROM cms_user WHERE ROW_ID = ?", item.get("UPDATEID"));
		if("1".equals(item.getString("UPDATEID"))) item.put("EDITER", "系统管理员");
		else{
			item.put("EDITER", "");
			if(editer != null) item.put("EDITER", editer);
		}
		
		itemList.add(item);
	}
}else if ("deletelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	String itemMc = "";
	for (int i = 0; i < checkboxs.length; i++) {
		String checkbox = checkboxs[i];
		DbMap tempDbMap = db.executeDbMap("SELECT * FROM cms_member WHERE ROW_ID = ?", checkbox);
		if(tempDbMap != null){
			itemMc += " " + tempDbMap.getString("NAME");
			
			db.executeNonQuery("DELETE FROM cms_member WHERE ROW_ID = ?", checkbox);
		}
	}
	
	addLog("删除用户：" + itemMc, "2002");
	redirect("member_list.jsp?act=list&pid=" + params.get("pid"));
}else if ("update".equals(act)){
	String rowid = params.get("rowid");
	entity = db.executeDbMap("SELECT * FROM cms_member WHERE ROW_ID = ?", rowid);
}else if ("doupdate".equals(act)){
	
	String rowid = params.get("rowid");
	String username = params.get("username");
	String passwd = params.get("passwd");
	String realname = params.get("realname");
	String connect = params.get("connect");
	String email = params.get("email");
	String remark = params.get("remark");
	
	entity = db.executeDbMap("SELECT * FROM cms_member WHERE ROW_ID = ?", rowid);
	
	DbCommand cmd = db.getSqlStringCommand("SELECT * FROM cms_member WHERE USERNAME = @USERNAME AND ROW_ID <> @ROW_ID");
	cmd.addInParameter("USERNAME", username);
	cmd.addInParameter("ROW_ID", rowid);
	DbMap tempMap = cmd.executeDbMap();
	
	if(tempMap != null) msg.putMessage("系统已经存在此账号的用户!!");
	else {
		cmd = db.getSqlStringCommand("UPDATE cms_member SET USERNAME=@USERNAME,PASSWD=@PASSWD,REALNAME=@REALNAME, " + 
		"\"CONNECT\"=@CONNECT,EMAIL=@EMAIL,REMARK=@REMARK,UPDATEID=@UPDATEID,UPDATETIME=@UPDATETIME WHERE ROW_ID =@ROW_ID");
		cmd.addInParameter("ROW_ID", rowid);
		cmd.addInParameter("USERNAME", username);
		cmd.addInParameter("PASSWD", Util.empty(passwd) ? entity.get("PASSWD") : Util.md5(passwd));
		cmd.addInParameter("REALNAME", realname);
		cmd.addInParameter("CONNECT", connect);
		cmd.addInParameter("EMAIL", email);
		cmd.addInParameter("REMARK", remark);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		
		cmd.executeNonQuery();
		
		addLog("修改用户：" + username , "2002");
		
		msg.setIsSuccess(true);
	}
	
	entity = db.executeDbMap("SELECT * FROM cms_member WHERE ROW_ID = ?", rowid);
}else if ("doadd".equals(act)){
	String username = params.get("username");
	String passwd = params.get("passwd");
	String realname = params.get("realname");
	String connect = params.get("connect");
	String email = params.get("email");
	String remark = params.get("remark");
	
	DbMap tempMap = db.executeDbMap("SELECT * FROM cms_member WHERE USERNAME = ?", username);
	if(tempMap != null) msg.putMessage("系统已经存在此账号的用户!!");
	else {
		String uuid = Util.getUUID();
		
		DbCommand cmd = db.getSqlStringCommand("INSERT INTO cms_member(ROW_ID,USERNAME,PASSWD,REALNAME,\"CONNECT\",EMAIL,CREATETIME,REMARK,UPDATEID,UPDATETIME) VALUES(" + 
		"@ROW_ID,@USERNAME,@PASSWD,@REALNAME,@CONNECT,@EMAIL,@CREATETIME,@REMARK,@UPDATEID,@UPDATETIME)");
		
		cmd.addInParameter("ROW_ID", uuid);
		cmd.addInParameter("USERNAME", username);
		cmd.addInParameter("PASSWD", Util.md5(passwd));
		cmd.addInParameter("REALNAME", realname);
		cmd.addInParameter("CONNECT", connect);
		cmd.addInParameter("EMAIL", email);
		cmd.addInParameter("CREATETIME", Util.getDate());
		cmd.addInParameter("REMARK", remark);
		cmd.addInParameter("UPDATEID", current.get("ROW_ID"));
		cmd.addInParameter("UPDATETIME", Util.getDate());
		
		cmd.executeNonQuery();
		
		addLog("添加用户：" + username , "2002");
		
		msg.setIsSuccess(true);
	}
}else if ("search".equals(act)){
	session.removeAttribute("member_search");
}else if ("dosearch".equals(act)){
	Parameters searchParams = new Parameters();
	if(!Util.empty(params.get("keyword"))) searchParams.set("keyword", params.get("keyword"));
	if(!Util.empty(params.get("stime"))) searchParams.set("kstime", params.get("stime"));
	if(!Util.empty(params.get("etime"))) searchParams.set("jstime", params.get("etime"));
	
	session.setAttribute("member_search", searchParams);
	redirect("member_list.jsp?act=list&pid=" + params.get("pid"));
}
%>