<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_user.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
		<script language="javascript" type="text/javascript">
	    function del() {
	        if (0 == listcount) {
	            alert("请选择记录。")
	            return;
	        }
	        if (confirm("确认进行删除吗？")) {
	            document.forms[0].submit();
	        }
	    }
	    function delone(urls) {
	        if (confirm("确认进行删除吗？"))	{
				 location = urls;
			}
	    }
		</script>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
		
		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
				&nbsp;&nbsp;用户管理
				-&gt; <a href="dw_list.jsp?act=list">单位</a> 
				<%for(int i = 0; i < crumbs.size();i++ ){
					DbMap crumb = (DbMap)crumbs.get(i);
				%>
				<%if(i < crumbs.size() - 1){%>
				-&gt; <a href="dw_list.jsp?act=list&predwid=<%=crumb.get("ROW_ID")%>"><%=crumb.get("DWMC")%></a>
				<%}else{ %>
				-&gt; <a href="user_list.jsp?act=list&dwid=<%=crumb.get("ROW_ID")%><%=params.query("predwid,pid")%>"><%=crumb.get("DWMC")%>用户列表</a>
				<%} %>
				<%} %>
				<%if(search.size() > 0){%> -&gt; <font color=red>搜索结果</font><%} %>
				</td>
				<td class="title" width="60" align="center">
				<a href="dw_list.jsp?act=list<%=params.query("predwid,pid")%>">返回上页</a>
				</td>
			</tr>
			<tr>
				<td height="2" colspan="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td" colspan="2">
					&nbsp;
					<a href="user_list.jsp?act=list<%=params.query("dwid,predwid,pid,cpid")%>"><font color="#FF0000">列表</font></a> |&nbsp;
					<%if(operations.indexOf("|300202,add|") != -1 || "1".equals(current_userid)){%>
					<a href="user_add.jsp?act=add<%=params.query("dwid,predwid,pid,cpid")%>">添加</a> |&nbsp;
					<%} %>
					<%if(operations.indexOf("|300202,delete|") != -1 || "1".equals(current_userid)){%>
					<a href="javascript:del()">删除</a> |&nbsp;
					<%} %>
				</td>
			</tr>
		</table>
		<br/>
		<form name="formlist" id="formlist" action="user_list.jsp?act=deletelist<%=params.query("dwid,predwid,pid,cpid")%>" method="post">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table width="95%" align="center" border="0" class="TableList" cellpadding="0" cellspacing="0">
							<tr align="center">
								<td width="30" height="21" class="biaot">
									选择
								</td>
								<td width="30" height="21" class="biaot">
									编号
								</td>
								<td width="120" class="biaot">
									登录名
								</td>
								<td width="100" class="biaot">
									姓名
								</td>
								<td class="biaot">
									角色
								</td>
								<td class="biaot1" width="80">
									操作
								</td>
							</tr>
							<%for(int i = 0; i < itemList.size();i++){
								DbMap item = (DbMap)itemList.get(i);
							%>
								<tr bgcolor="#FFFFFF" onMouseOver="overtable(this);" id="<%=pager.getStart() + i + 1%>" onMouseOut="outtable(this);">
									<td height="21" class="biaol" align="center">
									<input type="checkbox" name="checkbox" id="c<%=pager.getStart() + i + 1%>" value="<%=item.get("ROW_ID")%>" onClick="selectbox(this)" >
									</td>
									<td height="21" class="biaol" align="center">
									<%=pager.getStart() + i + 1%>
									</td>
									<td class="biaol" align="center">
									<%=item.get("CODE")%>
									</td>
									<td class="biaol" align="center">
									<%=item.get("NAME")%>
									</td>
									<td class="biaol" >
									&nbsp;<%=item.get("ROLES")%>
									</td>
									<td class="biaol" align="left">
										<%if(operations.indexOf("|300202,update|") != -1 || "1".equals(current_userid)){%>
										<a href="user_update.jsp?act=update&rowid=<%=item.get("ROW_ID")%><%=params.query("dwid,predwid,pid,cpid")%>">修改</a>
										<%} %>
										<%if(operations.indexOf("|300202,delete|") != -1 || "1".equals(current_userid)){%>
										<a href="javascript:delone('user_list.jsp?act=deletelist&checkbox=<%=item.get("ROW_ID")%><%=params.query("dwid,predwid,pid,cpid")%>')">删除</a>
										<%} %>
									</td>
								</tr>
							<%} %>
						</table>
					</td>
				</tr>

			</table>
			<table width="95%" align="center" border="0" cellpadding="0"
				cellspacing="0">
				<tr>
					<td width="4%" height="25" valign="bottom">
						<input type="checkbox" name="toggleAll" onClick="ToggleAll(this);">
					</td>
					<td width="96%" valign="bottom">
						全选
					</td>
				</tr>
			</table>
			<table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td height="30" colspan="2"><%=pager%></td>
			</tr>
		</table>
		</form>
	</body>
</html>
