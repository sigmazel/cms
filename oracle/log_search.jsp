<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_log.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<link href="images/datepicker.css" rel="stylesheet" type="text/css" />
		<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
		<script src="laydate/laydate.js" type="text/javascript"></script>
		<script>
	     function ValidateForm(theform){
	        if(theform.content.value==""){
	            alert("关键不能为空。");
	            theform.content.focus();
	            return false
	        }
	    }
		</script>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0"
		bgcolor="#EDF4FD">
		<form action="?act=dosearch" method="post" onsubmit="return ValidateForm(this)">
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;系统管理 -&gt; 日志 -&gt; 查询
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
				<tr>
					<td height="22" class="title_td">
						&nbsp;
						<a href="log_list.jsp?act=list">列表</a> |&nbsp;
						<font color="#FF0000">查询</font>

					</td>
				</tr>
			</table>
			<br>
			<table width="95%" align="center" border="0" cellpadding="0"
				cellspacing="0" class="title_td1">
				<tr>
					<td align="center">
						<br>
						<br>
						<table width="70%" border="0" align="center" cellpadding="0"
							cellspacing="0" class="TableList1">
							<tr>
								<td height="25" align="right" class="biaot">
									关键字：
								</td>
								<td width="76%" class="biaoright" align="left">
									<input type="text" name="content" class="inputbox1" size="20">
								</td>
							</tr>
							<tr>
								<td align="right" class="biaot" height="25">
									操作时间
								</td>
								<td class="biaoright" align="left">
									<table width="100%" border="0" align="center" cellpadding="0"
										cellspacing="0">
										<tr>
											<td>
												<input type="text" name="stime" class="inputbox1" size="12" onclick="laydate({istime:false, format:'YYYY-MM-DD'})"/>
												至
												<input type="text" name="etime" class="inputbox1" size="12" onclick="laydate({istime:false, format:'YYYY-MM-DD'})"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>

						</table>
						<br>
					</td>
				</tr>
				<tr>
					<td height="30" align="center">
						<%if(operations.indexOf("|4002,search|") != -1 || "1".equals(current_userid)){%>
							<input class="inputbox2" type="submit" name="Submit"
								value=" 确 定 ">
						<%}%>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input class="inputbox2" type="button" name="Submit2"
							value=" 返 回 " onclick="javascript:location.href='log_list.jsp?act=list';" />
					</td>
				</tr>
			</table>

		</form>
	</body>
</html>
