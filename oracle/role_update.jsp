<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_role.jsp" %>      
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>###</title>
	<link href="images/style.css" rel="stylesheet" type="text/css">
	<script src="js/jquery-1.7.1.js" language="javascript" type="text/javascript"></script>
		<script src="js/jquery.list.js" language="javascript" type="text/javascript"></script>
	<script>
     function ValidateForm(theform){
        if(theform.name.value==""){
            alert("名称不能为空。");
            theform.name.focus();
            return false
        }
       
    }
</script>

	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0"
		bgcolor="#EDF4FD">

		<table width="95%" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td class="title">
					&nbsp;&nbsp;用户管理 -&gt; 角色 -&gt; 修改角色
				</td>
			</tr>
			<tr>
				<td height="2"></td>
			</tr>
			<tr>
				<td height="22" class="title_td">
					&nbsp;
					<a href="role_list.jsp?act=list">列表</a> |&nbsp;
					<font color="#FF0000">修改</font>
				</td>
			</tr>
		</table>
		<br>
		<form action="role_update.jsp?act=doupdate&rowid=<%=params.get("rowid")%>" onsubmit="return ValidateForm(this)" method="post">
			<table width="95%" align="center" border="0" cellpadding="0"
				cellspacing="0">
				<tr>
					<td align="center" class="title_td1">
						<br>
						<br>
						<table width="75%" align="center" border="0" cellpadding="0"
							cellspacing="0">
							<tr>
								<td width="25%" height="30" align="right">
									名称：
								</td>
								<td width="75%" align="left">
									<input type="text" name="name" class="inputbox1" size="20" value="<%=entity.get("NAME")%>" >
									<font color="#FF0000">* </font>
								</td>
							</tr>
							<tr>
								<td align="right">
									备注：
								</td>
								<td align="left">
									<textarea name="des" cols="35" rows="4" class="inputarea"><%=entity.get("DES")%></textarea>
								</td>
							</tr>


						</table>
						<p>
							<%if(operations.indexOf("|3003,update|") != -1 || "1".equals(current_userid)){%>
							<input type="submit" class="inputbox2" value=" 提 交 ">
							<%} %>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" class="inputbox2" value=" 返 回 " onclick="location='role_list.jsp?act=list'" />
							<br>
							<br>
						</p>
						<p>
							&nbsp;
							<br>
							<br>
						</p>

					</td>
				</tr>

			</table>
		</form>
		<%if(!msg.emptyMessage()){%>
		<script type="text/javascript">
		alert('<%=msg.getMessage()%>');
		</script>
		<%}%>
		<%if(msg.getIsSuccess()){ %>
		<script type="text/javascript">
		location.href='role_list.jsp?act=list';
		</script>
		<%} %>
	</body>

</html>
