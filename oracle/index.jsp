<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init.jsp" %>
<%
DbMap settings = db.executeDbMap("SELECT * FROM cms_variable WHERE IDENTITY = 'adminpasswd'");

String act = params.get("act");
if("login".equals(act)){
	String seccode = (String) session.getAttribute("seccode");
	String code = params.get("code");
	String passwd = Util.hashOfMd5( params.get("passwd"));
	
	if (seccode == null || !seccode.equals(params.get("seccode"))) msg.putMessage("验证码不正确!!");
	else {
		DbMap userMap = null;
		if("administrator".equals(code)){
			userMap = db.executeDbMap("SELECT * FROM cms_variable WHERE IDENTITY = 'adminpasswd'");
			if (passwd.equals(userMap.get("VAL"))) {
				DbCommand cmd = db.getSqlStringCommand("UPDATE cms_variable SET VAL = @VAL WHERE IDENTITY = 'adminlastlogin'");
				cmd.addInParameter("VAL", Util.getDateString());
				cmd.executeNonQuery();
		   		
				userMap.put("NAME", "系统管理员");
				userMap.set("CODE", "administrator");
				userMap.put("ROW_ID", "1");
				userMap.put("DWID", "1");
				userMap.put("DWBH", "");
				userMap.put("DWMC", "默认");
				userMap.put("DWCONTENT", "");
				userMap.put("DWPATH", "");
				userMap.put("LOGINIP", request.getRemoteAddr());
				
				List vaList = db.executeDbMapList("SELECT * FROM cms_variable");
				for(int i = 0; i < vaList.size();i++){
					DbMap item = (DbMap)vaList.get(i);
					if("adminname".equals(item.getString("IDENTITY"))) userMap.put("NAME", item.getString("VAL"));
					if("adminemail".equals(item.getString("IDENTITY"))) userMap.put("EMAIL", item.getString("VAL"));
					if("adminphone".equals(item.getString("IDENTITY"))) userMap.put("PHONE", item.getString("VAL"));
				}
				
		   		session.setAttribute("current", userMap);
		   		
		   		String sql = "SELECT b.IDENTITY AS BIDENTITY, c.IDENTITY AS CIDENTITY " +
		   		"FROM cms_privilege a, cms_resource b, cms_operation c " +
		   		"WHERE a.RESOURCEID = b.ROW_ID AND a.OPERATIONID = c.ROW_ID " +
		   		"ORDER BY b.IDENTITY ASC";
		   		
		    	List optList = db.executeDbMapList(sql);
		    	String optString = "|";
		    	for(int i= 0, j = optList.size(); i < j; i++){
		    		DbMap item = (DbMap)optList.get(i);
		    		optString = optString + item.getString("BIDENTITY") + "," + item.getString("CIDENTITY") + "|";
		    	}
		    	
		    	session.setAttribute("operations", optString);
		    	
		   		msg.setIsSuccess(true);
		   		msg.putMessage("");
			}else msg.putMessage("用户名或密码不正确!!");
		}else {
			userMap = db.executeDbMap("SELECT a.*,b.DWMC,b.DWBH,b.CONTENT AS DWCONTENT,b.DWALIAS,b.PATH AS DWPATH FROM cms_user a, cms_dw b WHERE a.DWID = b.ROW_ID AND a.CODE = ?", code);
			if (userMap != null) {
	    		if (passwd.equals(userMap.get("PASSWD"))) {
	    			DbCommand cmd = db.getSqlStringCommand("UPDATE cms_user SET LOGINTIME=@LOGINTIME, TIMES=TIMES + 1 WHERE ROW_ID = @ROW_ID");
	    			cmd.addInParameter("LOGINTIME", Util.getDate());
	    			cmd.addInParameter("ROW_ID", userMap.getString("ROW_ID"));
	    			cmd.executeNonQuery();
		       		
	    			DbMap useractiveMap = (DbMap)db.executeDbMap("SELECT * FROM cms_useractive WHERE USERID = '" + userMap.getString("ROW_ID") + "'");
		       		if(useractiveMap == null){
		       			cmd = db.getSqlStringCommand("INSERT INTO cms_useractive(ROW_ID,ACTIVETIME,DWID,USERID,USERNAME) values" + 
		       			"(@ROW_ID,@ACTIVETIME,@DWID,@USERID,@USERNAME)");
		       			cmd.addInParameter("ROW_ID", Util.getUUID());
		       			cmd.addInParameter("ACTIVETIME", Util.getDate());
		       			cmd.addInParameter("DWID", userMap.getString("DWID"));
		       			cmd.addInParameter("USERID", userMap.getString("ROW_ID"));
		       			cmd.addInParameter("USERNAME", userMap.getString("NAME"));
		       			cmd.executeNonQuery();
		       		}else{
		       			cmd = db.getSqlStringCommand("UPDATE cms_useractive SET ACTIVETIME=@ACTIVETIME,USERNAME=@USERNAME WHERE ROW_ID = @ROW_ID");
			   			cmd.addInParameter("ROW_ID", userMap.getString("ROW_ID"));
			   			cmd.addInParameter("ACTIVETIME", Util.getDate());
			   			cmd.addInParameter("USERNAME", userMap.getString("NAME"));
			   			cmd.executeNonQuery();
		       		}
		       		
		       		session.setAttribute("current", userMap);
			   		
			   		String sql = "SELECT b.IDENTITY AS BIDENTITY, c.IDENTITY AS CIDENTITY " +
			   		"FROM cms_privilege a, cms_resource b, cms_operation c, cms_userrole d, cms_rolepri e " +
			   		"WHERE a.RESOURCEID = b.ROW_ID AND a.OPERATIONID = c.ROW_ID AND d.ROLEID = e.ROLEID " +
			   		"AND e.PRIVILEGEID = a.ROW_ID AND d.USERID = ? " +
			   		"ORDER BY b.IDENTITY ASC";
			   		
			    	List optList = db.executeDbMapList(sql, userMap.getString("ROW_ID"));
			    	String optString = "|";
			    	for(int i= 0, j = optList.size(); i < j; i++){
			    		DbMap item = (DbMap)optList.get(i);
			    		optString = optString + item.getString("BIDENTITY") + "," + item.getString("CIDENTITY") + "|";
			    	}
			    	
			    	session.setAttribute("operations", optString);
			    	
			    	userMap.put("DWID", userMap.getString("DWID"));
					userMap.put("LOGINIP", request.getRemoteAddr());
					
		       		msg.setIsSuccess(true);
		       		msg.putMessage("");
	    		}else msg.putMessage("用户名或密码不正确!!");
			}else msg.putMessage("用户名或密码不正确!!");
		}
	}
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>文章发布系统</title>
<style type="text/css">
table.width60		{width:60%;border:solid 1px #759FD2;}
table.per100		{width:100%;}
td 					{font-family:arial,helvetica,sans-serif;font-size:10pt;padding:4px;text-align:left;}
body 				{background:#fff;color:#000;font-family:arial,helvetica,sans-serif;font-size:12px;margin:0px;padding:0px;}
p 					{font-family:arial,helvetica,sans-serif;}
address 			{font-family:arial,helvetica,sans-serif;font-size:11px;}
a					{color:black;text-decoration:none;}
a:active 			{color:black;text-decoration:none;}
a:link    			{color:black;text-decoration:none;}
a:visited 			{color:black;text-decoration:none;}
a:hover				{color:red;}
a.subtle			{color:blue;text-decoration:none;}
a.link-no-line		{text-decoration:none;}
form				{display:inline;}

.txt {
	font:normal 12px Arial, Helvetica, sans-serif;
	height:22px;
	border:1px solid #ccc;
	padding:1px;
}
.btn{
	cursor:pointer;
	margin:0px;
	padding:0px;
	height:30px;
	line-height:30px;
	font:normal 12px Arial, Helvetica, sans-serif;
	background:url(../pages/images/btn_100.jpg) no-repeat;
}
</style>
<script language="javascript">
     function ValidateForm(theform){
        if(theform.code.value==""){
            alert("用户名不能为空。");
            theform.code.focus();
            return false
        }else if(theform.passwd.value==""){
            alert("密码不能为空。");
            theform.passwd.focus();
            return false
        }else if(theform.seccode.value==""){
             alert("校验码不能为空。");
             theform.seccode.focus();
             return false
        }
    }
</script>
</head>
<body bottommargin="0" bgcolor="#F6F7EF" >
<br />
<br />
<br />
<div align="center">
	<div style="display:none;">
    </div>
</div>
<br />
<div align="center">
	<form action="?act=login" method="post" onsubmit="return ValidateForm(this)">
	<table class="width60" cellspacing="1" style="width: 380px;">
        <tr>
            <td>
                <table class="per100" bgcolor="#759FD2" cellspacing="0" style="border-bottom:2px solid #96bae5;">
                    <tr>
                        <td height="30" style="color: White; font-size: 20px;">
                            <div align="center"><strong><%=settings.get("sysname")%>后台管理</strong></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table class="per100" cellspacing="0" id="frm_login">
                    <tr class="line">
                        <td width="80" height="28" style="font-size:14px;">用户名</td>
                        <td>
                        &nbsp;<input type="text" name="code" id="code" size="25" maxlength="20" class="txt" value="" />
                        </td>
                    </tr>
                    <tr class="line" height="28">
                        <td style="font-size:14px;">密　码</td>
                        <td>
                        &nbsp;<input type="password" name="passwd" id="passwd" size="20" maxlength="20" class="txt"  />
                        </td>
                    </tr>
                    <tr class="line" height="28">
                        <td style="font-size:14px;">验证码</td>
                        <td>
                        	<table cellpadding="0" cellspacing="0" border="0" align="left">
                        	<tr>
                        	<td>
                            <input name="seccode" type="text" class="txt" size="5" style="width:90px;"/>
                            </td>
                            <td>
                            <img src="seccode.jsp?rnd=<%=Util.getUUID()%>" />
                            </td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="60">
                        </td>
                        <td>
                            <input type="submit" class="btn" value=" 提 交 " />
                            &nbsp;
                            <input type="reset" class="btn" value=" 重 置 ">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <br/><br/>
</div>
<script type="text/javascript">
<%if(!msg.emptyMessage()){%>
alert("<%=msg.getMessage()%>");
<%}%>
document.forms[0].code.focus();
<%if(msg.getIsSuccess()){ %>
location.href = "index_error.jsp?n=3";
<%}%>
if(top != self) top.location.href = "index.jsp";
</script>
</body>
</html>