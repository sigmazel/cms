<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
List<DbMap> dbMapList = db.executeDbMapList("SELECT * FROM cms_variable");
DbMap settings = new DbMap();
for(DbMap dbMap : dbMapList) settings.put(dbMap.getString("IDENTITY"), dbMap.getString("VAL"));

String logoTitle = current.getString("DWMC") + " - " + settings.getString("sysname") + "后台管理";
Object v_logo = db.executeScalar("SELECT VAL FROM cms_variable WHERE IDENTITY ='logo'");
if(v_logo != null && v_logo.toString().length() > 0) logoTitle = v_logo.toString();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=settings.get("sysname") %></title>
<style type="text/css">
<!--
@import url(images/style.css);
#div_show_message{
	position:absolute;
	width:240px;
	height:45px;
	border:2px solid #88B7E5;
	right:-16px;
	bottom:16px;
	background:#EDF4FD;
	text-align:center;
	padding:0px;
	font-size:12px;
	font-weight:bold;
	line-height:22px;
	display:none;
	color:white;
}
#div_show_message #div_message{
	margin-top:10px;
}
#div_show_message #div_close{
	position:absolute;
	top:0px;
	right:8px;
}
#div_show_message #div_angle{
	position:absolute;
	right:10px;
	bottom:-16px;
	width:20px;
	height:15px;
	background:url(/images/msg_a.gif);
}
#td_msg_txt a{
	color:white;
}
a.awhite{
	color:white;
}
a.awhite :link{
	color:white;
}
-->
</style>
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/jquery.extend.js"></script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bgcolor="#EDF4FD">
<table width="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td height="60" background="images/top_bg.jpg">
  <div style="font-size:26px;font-weight:bold;color:white;">
  <%= logoTitle%>
  </div>
  </td>
	<td width="410" background="images/top_new_right.jpg" align="right">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr><td align="right"></td></tr>
	<tr>
	<td align="left" valign="bottom" style="padding-bottom:10px;">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td valign="bottom" width="46">&nbsp;</td>
		<td valign="bottom"><font color=white><%=Util.format(Util.getDate(), "yyyy年MM月dd日 w")%></font></td>
		<td valign="bottom"><a href="article_index.jsp?act=ilist" target="main"><img src="images/iepic.gif" width="18" height="18" border="0"></a></td>
		<td valign="bottom"><a href="../" target="_blank"><font color=white>查看网站</font></a></td>
		<td valign="bottom"><a href="article_index.jsp?act=ilist" target="main"><img src="images/dot-03.gif" width="18" height="18" border="0"></a></td>
		<td valign="bottom"><a href="article_index.jsp?act=ilist" target="main"><font color=white>返回桌面</font></a></td>
		<td valign="bottom"><a href="index_logout.jsp" target="_top"><img src="images/dot-04.gif" width="18" height="18" border="0"></a></td>
		<td valign="bottom"><a href="index_logout.jsp" target="_top"><font color=white>退出系统</font></a>&nbsp;</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
  </tr>
</table>
</body>
</html>