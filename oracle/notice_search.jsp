<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_notice.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<link href="images/datepicker.css" rel="stylesheet" type="text/css" />
		<script src="js/jquery-1.7.1.js" type="text/javascript"></script>
		<script src="laydate/laydate.js" type="text/javascript"></script>
		<style type="text/css">
		.redTip{
			color:red;
		}
		.editTbl td{
			padding:2px 2px 2px 4px;
			height:25px;
		}
		.inputitem{
			width:150px;
		}
		</style>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">	
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a> 
						-&gt; 通知公告 
						-&gt; 查询
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
				<tr>
					<td height="22" class="title_td">
						&nbsp;
						<a href="notice.jsp?act=list&pid=<%=params.get("pid")%>">列表</a> |&nbsp;
						<font color="#FF0000">查询</font>
					</td>
				</tr>
			</table>
			<table width="95%" border="0" align="center"
				cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="padding-top:5px">
		<br/>
		<form action="notice_search.jsp?act=dosearch<%=params.query("pid,psize,ref")%>" method="post">
			<table width="500" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl">
			<tr bgcolor="#CEDDF0">
				<td width="120" style="font-weight:bold;" align="left" id="tr1_1">
				关键词：
				</td>
				<td>
				<input type="text" name="keyword" class="inputbox1" size="12" />
				</td>
			</tr>
			<tr bgcolor="#CEDDF0">
			<td style="font-weight:bold;" align="left">发布时间：</td>
			<td>
			<input type="text" name="stime" class="inputbox1" size="12" onclick="laydate({istime:false, format:'YYYY-MM-DD'})"/>
			至
			<input type="text" name="etime" class="inputbox1" size="12" onclick="laydate({istime:false, format:'YYYY-MM-DD'})"/>
			</td>
			</tr>
			</table>
			<br/>
			<br/>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align=center>
				<tr>
					<td colspan="3" align="center">
						&nbsp;&nbsp;&nbsp;&nbsp;
						<%if(operations.indexOf("|1001,search|") != -1 || "1".equals(current_userid)){%>
						<input type="submit" class="inputbox2" value=" 提 交 ">
						<%} %>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='notice.jsp?act=list&pid=<%=params.get("pid")%>'">
						<br>
						<br>
					</td>
				</tr>
			</table>
			</form>
			<br/>
			<br/>
			</td>
			</tr>
		</table>
	</body>
</html>