<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="init_logined.jsp" %>
<%
String act = params.get("act");
List<DbMap> itemList = new ArrayList<DbMap>();
Pager pager = new Pager(params.get("pid"), Util.getInteger(params.get("psize")) < 10 ? 10 : Util.getInteger(params.get("psize")));

Parameters search = (Parameters) session.getAttribute("log_search");
String where = "";
if(search == null) search = new Parameters();

if (!Util.empty(search.get("createid"))) where += " and CREATEID = '" + search.get("createid")+ "'";
if (!Util.empty(search.get("content"))) where += " and CONTENT like '%" + search.get("content") + "%'";
if (!Util.empty(search.get("ip"))) where += " and CREATEIP like '%" + search.get("ip") + "%'";
if (!Util.empty(search.get("kstime"))) where += " and CREATETIME >= '" + search.get("kstime") + "'";
if (!Util.empty(search.get("jstime"))) where += " and CREATETIME <= '" + search.get("jstime") + "'";
if (!Util.empty(search.get("resourceid"))) where += " and RESOURCEID = '" + search.get("resourceid")+"'";

pager.setQueryString("act=" + params.get("act"));

if("list".equals(act)){
	List<DbMap> list = db.executeDbMapPager("SELECT * FROM cms_log WHERE 1 = 1 " + where + " ORDER BY CREATETIME DESC", pager);
	for(int i = 0, j = list.size(); i < j; i++){
		DbMap item = (DbMap)list.get(i);
		Object username = db.executeScalar("SELECT NAME FROM cms_user WHERE ROW_ID = ?", item.get("CREATEID"));
		if("1".equals(item.getString("CREATEID"))) item.put("USERNAME", "系统管理员");
		else{
			item.put("USERNAME", "");
			if(username != null) item.put("USERNAME", username);
		}
		
		Object resourcename = db.executeScalar("SELECT NAME FROM cms_resource WHERE ROW_ID = ?", item.get("RESOURCEID"));
		item.put("RESOURCENAME", "");
		if(resourcename != null) item.put("RESOURCENAME", resourcename);
		itemList.add(item);
	}
}else if("deleteall".equals(act)){
	db.executeNonQuery("DELETE FROM cms_log");
	addLog("清空日志", "4002");
	redirect("log_list.jsp?act=list&pid=" + pager.getCurrentIndex());
}else if ("deletelist".equals(act)){
	String[] checkboxs = request.getParameterValues("checkbox");
	for (int i = 0; i < checkboxs.length; i++) {
		db.executeNonQuery("DELETE FROM cms_log WHERE ROW_ID = ?", checkboxs[i]);
	}
	
	addLog("删除部分日志", "4002");
	redirect("log_list.jsp?act=list&pid=" + pager.getCurrentIndex());
}else if("search".equals(act)){
	session.setAttribute("log_search", null);
}else if("dosearch".equals(act)){
	Parameters research = new Parameters();
	
	if (!Util.empty(params.get("createid"))) research.put("createid", params.get("createid"));
	if (!Util.empty(params.get("content"))) research.put("content", params.get("content"));
	if (!Util.empty(params.get("ip"))) research.put("ip", params.get("ip"));
	if (!Util.empty(params.get("kstime"))) research.put("kstime", params.get("kstime"));
	if (!Util.empty(params.get("jstime"))) research.put("jstime", params.get("jstime"));
	if (!Util.empty(params.get("resourceid"))) research.put("resourceid", params.get("resourceid"));
	 
	session.setAttribute("log_search", research);
	
	redirect("log_list.jsp?act=list&pid=" + pager.getCurrentIndex());
}
%>