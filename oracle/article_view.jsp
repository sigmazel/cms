<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_article.jsp" %>
<%
String ref = params.get("ref");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>###</title>
		<link href="images/style.css" rel="stylesheet" type="text/css">
		<style type="text/css">
		.redTip{
			color:red;
		}
		.editTbl td{
			padding:2px 2px 2px 4px;
			height:25px;
		}
		.inputitem{
			width:150px;
		}
		</style>
		<script src="js/jquery-1.7.1.js" type="text/javascript"></script>
	</head>
	<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">	
			<table width="95%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="title">
						&nbsp;&nbsp;<a href="article_index.jsp?act=ilist"><a href="article_index.jsp?act=ilist"><%=current.get("DWMC")%></a></a> 
						<%if("nodeal".equals(ref)){%>
						-&gt; 待审核信息 
						-&gt; 查看
						<%}else if("noaudit".equals(ref)){%>
						-&gt; 未通过信息 
						-&gt; 查看
						<%}else if("audit".equals(ref)){%>
						-&gt; 已审核信息 
						-&gt; 查看
						<%}else if("temp".equals(ref)){%>
						-&gt; 草稿箱 
						-&gt; 查看
						<%}else if("deleted".equals(ref)){%>
						-&gt; 回收站 
						-&gt; 查看
						<%}else if("share".equals(ref)){%>
						-&gt; 共享信息 
						-&gt; 查看
						<%}else{%>
						-&gt; 查看
						<%} %>
					</td>
				</tr>
				<tr>
					<td height="2"></td>
				</tr>
				<tr>
					<td height="22" class="title_td">
						&nbsp;
						<%if("nodeal".equals(ref)){%>
						<a href="article_nodeal.jsp?act=ndlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">查看</font>
						<%}else if("noaudit".equals(ref)){%>
						<a href="article_noaudit.jsp?act=nalist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">查看</font>
						<%}else if("audit".equals(ref)){%>
						<a href="article_audit.jsp?act=alist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">查看</font>
						<%}else if("temp".equals(ref)){%>
						<a href="article_temp.jsp?act=tlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">查看</font>
						<%}else if("deleted".equals(ref)){%>
						<a href="article_deleted.jsp?act=dlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">查看</font>
						<%}else if("share".equals(ref)){%>
						<a href="article_share.jsp?act=slist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>">列表</a> |&nbsp;
						<font color="#FF0000">查看</font>
						<%}else if("index".equals(ref)){%>
						<a href="article_index.jsp?act=ilist">列表</a> |&nbsp;
						<font color="#FF0000">查看</font>
						<%}else{%>
						<font color="#FF0000">查看</font>
						<%} %>
					</td>
				</tr>
			</table>
			<table width="95%" border="0" align="center"
				cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" style="padding-top:5px">
		<br/>
		<form action="#" method="post">
			<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
			<tr>
				<td bgcolor="#CEDDF0" width="90" style="font-weight:bold;width:90px;" align="left">信息分类：</td>
				<td bgcolor="#ffffff" colspan="5"><%=entity.get("assortCrumbs")%></td>
			</tr>
			<tr>
				<td bgcolor="#CEDDF0" style="font-weight:bold;width:90px;" align="left"><span id="spn_title">标题</span>：</td>
				<td bgcolor="#ffffff" colspan="3"><%=entity.get("TITLE")%></td>
				<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left" width="90">置顶：</td>
				<td bgcolor="#ffffff" width="160">
					<%if("0".equals(entity.getString("ISTOP"))){%>否<%}%>
					<%if("1".equals(entity.getString("ISTOP"))){%>是<%}%>
				</td>
			</tr>
			<tr>
				<td bgcolor="#CEDDF0" style="font-weight:bold;width:90px;" align="left">小标题/专题：</td>
				<td bgcolor="#ffffff" colspan="5"><%=entity.get("SUBJECT")%></td>
			</tr>
			<tr>
			<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">链接：</td>
			<td bgcolor="#ffffff"><%=entity.get("LINK")%></td>
			<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left" width="90"><span id="spn_source">来源</span>：</td>
			<td bgcolor="#ffffff" width="180" style="width:250px;"><%=entity.get("SOURCE")%></td>
			<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left" width="90"><span id="spn_author">作者</span>：</td>
			<td bgcolor="#ffffff"><%=entity.get("AUTHOR")%></td>
			</tr>
			<tr>
			<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left">是否共享：</td>
			<td bgcolor="#ffffff">
			<%if("0".equals(entity.getString("ISOPEN"))){%>否<%}%>
			<%if("1".equals(entity.getString("ISOPEN"))){%>是<%}%>
			</td>
			<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left" width="90">发布时间：</td>
			<td bgcolor="#ffffff" width="180" style="width:150px;"><%=entity.get("PUBDATE")%></td>
			<td bgcolor="#CEDDF0" style="font-weight:bold;" align="left" width="90">过期时间：</td>
			<td bgcolor="#ffffff"><%=entity.get("INDATE")%></td>
			</tr>
			</table>
			<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
			<tr bgcolor="#CEDDF0">
				<td height="28" style="font-weight:bold;" align="left">
					内容：
				</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="left" style="word-break:break-all; word-wrap:break-word;">
				<%
				String[] imglist = (String[])entity.get("IMGLIST");
				for(String img : imglist){
				%>
				<p><img src="<%=img%>"/></p>
				<%}%>
				<%=entity.get("CONTENT")%>
				</td>
			</tr>
			</table>
			<%if(!Util.empty(entity.getString("BACKDES"))){%>
			<table width="100%" border=0 cellpadding=0 cellspacing=1 align="center" bgcolor="#809BB9" class="editTbl" style="margin-top:2px;">
			<tr bgcolor="#CEDDF0">
				<td height="28" style="font-weight:bold;" align="left">
					退回理由：
				</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="left" style="word-break:break-all; word-wrap:break-word;">
				<%=Util.nlToBr(entity.getString("BACKDES"))%>
				</td>
			</tr>
			</table>
			<%} %>
			<br/>
			<br/>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align=center>
				<tr>
					<td colspan="3" align="center">
						<%if(operations.indexOf("|1004,update|") != -1 || operations.indexOf("|1005,update|") != -1 || operations.indexOf("|1006,update|") != -1 || operations.indexOf("|1007,update|") != -1 || operations.indexOf("|1008,update|") != -1 || "1".equals(current_userid)){%>
						<input type="button" class="inputbox2" value=" 修改 " onclick="location='article_update.jsp?act=update&rowid=<%=params.get("rowid")%>&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>&ref=<%=params.get("ref")%>'"/>&nbsp;&nbsp;
						<%}%>
						<%if("nodeal".equals(ref)){%>
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_nodeal.jsp?act=ndlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
						<%}else if("noaudit".equals(ref)){%>
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_noaudit.jsp?act=nalist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
						<%}else if("audit".equals(ref)){%>
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_audit.jsp?act=alist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
						<%}else if("temp".equals(ref)){%>
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_temp.jsp?act=tlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
						<%}else if("deleted".equals(ref)){%>
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_deleted.jsp?act=dlist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
						<%}else if("share".equals(ref)){%>
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_share.jsp?act=slist&pid=<%=params.get("pid")%>&psize=<%=params.get("psize")%>'">
						<%}else if("index".equals(ref)){%>
						<input type="button" class="inputbox2" value=" 返 回 " onclick="location='article_index.jsp?act=ilist'">
						<%} %>
						<br>
						<br>
					</td>
				</tr>
			</table>
			</form>
			<br/>
			<br/>
			</td>
			</tr>
		</table>
	</body>
</html>