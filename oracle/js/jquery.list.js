    var save = new Array ();
    var listcount =0;

    function ToggleAll(e) {
        if (e.checked) {
            CheckAll();
        }
        else {
            ClearAll();
        }
    }

    function CheckAll()  {
		var ml = document.getElementById('formlist');

		var len = ml.elements.length;
		for (var i = 0; i < len; i++) {
			var e = ml.elements[i];
			if (e.name == "checkbox") {
			    if(e.checked==false)
                    Check(e);
			}
		}
		ml.toggleAll.checked = true;
    }

    function ClearAll()  {
	   var ml = document.getElementById('formlist');
	   var len = ml.elements.length;
	   for (var i = 0; i < len; i++) {
		  var e = ml.elements[i];
		  if (e.name == "checkbox") {
		    if(e.checked==true)
                Clear(e);
		  }
		}
		ml.toggleAll.checked = false;
		listcount=0;
    }

    function Check(e)   {
        if(!!e.disabled)return;
        e.checked = true;
        var tmp = removeLeadingChar(e.id,"c");
            if (save[tmp]!=document.getElementById(tmp))    {
                save[tmp]=document.getElementById(tmp)
                listcount=listcount+1
                if(document.getElementById(tmp)) document.getElementById(tmp).className="Selected"
            }
    }

    function Clear(e)  {
        var ml = document.getElementById('formlist');
        e.checked = false;
        ml.toggleAll.checked = false;

        var tmp = removeLeadingChar(e.id,"c");
            if (save[tmp]==document.getElementById(tmp))    {
                save[tmp]=""
                listcount=listcount-1
                if(document.getElementById(tmp)) document.getElementById(tmp).className="DataDetailItem"
            }
    }

    function removeLeadingChar (inputString, removeChar)  {
        var returnString = inputString;
        if (removeChar.length)  {
            while(''+returnString.charAt(0)==removeChar)    {
                returnString=returnString.substring(1,returnString.length);
            }
        }
        return returnString;
    }

    function selectbox(obj) {
        var tmp = removeLeadingChar(obj.id,"c")

        if (save[tmp] == document.getElementById(tmp))    {
            document.getElementById(tmp).className ="DataDetailItem";
            Clear(obj);
        }
        else   {
            Check(obj);
            document.getElementById(tmp).className="Selected";
            document.getElementById('formlist').toggleAll.checked = AllChecked();
        }
    }

    function AllChecked()   {
        ml = document.getElementById('formlist');
        len = ml.elements.length;
        for(var i = 0 ; i < len ; i++) {
            if (ml.elements[i].name == "checkbox" && !ml.elements[i].checked) {
                return false;
            }
        }
        return true;
    }

    function overtable(obj)   {
        if(obj!=save[obj.id]) {
            obj.className="onSelect";
        }
    }
    function outtable(obj) {
        if(obj!=save[obj.id])  {
            obj.className="DataDetailItem"
        }
    }
    function havenoNumber(str) {
  		return str.match(/\D/)==null
	}
	function outputExcel(tablename, dirid){
		var width = 150, height = 40, url = 'outputExcel.do?tbl='+tablename+'&dirid='+dirid;
		var tmpTop=(window.screen.availHeight-30-height )/2;
		var tmpLeft=(window.screen.availWidth-10-width)/2;
		var objwin = window.open(url,'out','fullscreen=0,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,width=' + width+ ',height=' + height+ ',top='+tmpTop+',left='+tmpLeft,true);
		objwin.focus(); 
	}