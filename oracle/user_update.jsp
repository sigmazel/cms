<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="model_user.jsp" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>###</title>
	<link href="images/style.css" rel="stylesheet" type="text/css">
	<link href="zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="js/jquery.ztree.excheck.js"></script>
	<script language="javascript" type="text/javascript">
	var zTreeObj;
	var zNodes;
	
    function ValidateForm(theform){
        if(theform.code.value==""){
            alert("登录名不能为空。");
            theform.code.focus();
            return false;
        }
        
        if(theform.name.value==""){
            alert("姓名不能为空。");
            theform.name.focus();
            return false;
        }
        
        if(theform.password1.value!=""){
	        if(theform.password1.value!=theform.password2.value){
	            alert("两次输入的密码不一致。");
	            theform.password1.focus();
	            return false;
	        }
        }
        
        var userAssortIdString = theform.userAssortIdString;
		userAssortIdString.value = '';
		var nodes = zTreeObj.getCheckedNodes(true);
		for(var i = 0 ; i < nodes.length; i++) if(nodes[i].checked) userAssortIdString.value = userAssortIdString.value + ',' + nodes[i].id;
		
        return true;
    }
	</script>
<body topmargin="20" leftmargin="0" rightmargin="0" bottommargin="0" bgcolor="#EDF4FD">
<form action="user_update.jsp?act=doupdate<%=params.query("rowid,dwid,predwid,pid,cpid")%>" method="post" onsubmit="return ValidateForm(this)">
<input type="hidden" name="userAssortIdString" value="<%=userAssortIdString%>"/>
<table width="95%" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="title">
	&nbsp;&nbsp;用户管理 
	-&gt; <a href="dw_list.jsp?act=list">单位</a> 
				<%for(int i = 0; i < crumbs.size();i++ ){
					DbMap crumb = (DbMap)crumbs.get(i);
				%>
				<%if(i < crumbs.size() - 1){%>
				-&gt; <a href="dw_list.jsp?act=list&predwid=<%=crumb.get("ROW_ID")%>"><%=crumb.get("DWMC")%></a>
				<%}else{ %>
				-&gt; <a href="user_list.jsp?act=list&dwid=<%=crumb.get("ROW_ID")%><%=params.query("predwid,pid")%>"><%=crumb.get("DWMC")%>用户列表</a>
				<%} %>
				<%} %>
	-&gt; 修改用户
	</td>
  </tr>
  <tr>
    <td height="2"></td>
  </tr>
  <tr>
      <td height="22" class="title_td">&nbsp;
        <a href="user_list.jsp?act=list<%=params.query("rowid,dwid,predwid,pid,cpid")%>">列表</a> |&nbsp;
        <font color="#FF0000">修改</font>
    </td>
  </tr>
</table><br>
    <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
        <tr><td align="center" class="title_td1"><br><br>
         <table width="75%" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr >
                  <td height="30" align="right">登录名：</td>
                  <td width="76%" align="left"><input type="text" name="code" class="inputbox1" size="15" value="<%=entity.get("CODE")%>"></td>
                </tr>
                <tr >
                  <td height="30" align="right">姓名：</td>
                  <td width="76%" align="left"><input type="text" name="name" class="inputbox1" size="30"  value="<%=entity.get("NAME")%>"></td>
                </tr>
                <tr >
                  <td height="30" align="right">密码：</td>
                  <td width="76%" align="left"><input type="password" name="password1" class="inputbox1" size="20"   ></td>
                </tr>
                <tr >
                  <td height="30" align="right" >确认密码：</td>
                  <td width="76%" align="left"><input type="password" name="password2" class="inputbox1" size="20" ></td>
                </tr>
                <tr >
                  <td height="30" align="right">Email：</td>
                  <td width="76%" align="left"><input type="text" name="email" class="inputbox1" size="30"  value="<%=entity.get("EMAIL")%>"></td>
                </tr>
                <tr >
                  <td height="30" align="right">电话：</td>
                  <td width="76%" align="left"><input type="text" name="phone" class="inputbox1" size="30"  value="<%=entity.get("PHONE")%>"></td>
                </tr>
                <tr >
                  <td height="30" align="right" >角色：</td>
                  <td width="76%" align="left">
                      <select name="checkbox" multiple="multiple">
                      <%for(int i = 0; i< roleList.size(); i++){
                      	DbMap item = (DbMap)roleList.get(i);
                      %>
                      <option value="<%= item.get("ROW_ID")%>" <%if(roleString.indexOf("|" + item.getString("ROW_ID") + "|") != -1){ %>selected="selected"<%} %>><%= item.get("NAME")%></option>
                      <%} %>
                      </select>
                  </td>
                </tr>
                <tr>
                 <td height="30" align="right" valign="top" style="padding-top:10px;"><br/>分类：</td>
                  <td align="left">
	                  <br/>
	                  <div><input type="checkbox" name="toggleAll" onClick="toggle_ztree_all(this);">全部选中？</div>
	                  <div id="divTree" class="ztree"></div>
                  </td>
                </tr>
            </table>

            <p>
          <%if(operations.indexOf("|300202,update|") != -1 || "1".equals(current_userid)){%>
          <input type="submit" class="inputbox2" value=" 确 定 ">
          <%} %>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <input type="reset" class="inputbox2" value=" 返 回 " onclick="window.location='user_list.jsp?act=list<%=params.query("dwid,predwid,pid,cpid")%>'">
          <br>
          <br>
        </p>
    </td>
  </tr>
</table>
</form>
<script type="text/javascript">
var setting = {
	check: {
		enable: true
	},
	data: {
		simpleData: {
			enable: true
		}
	}
};

setting.check.chkboxType = {Y:'Y',N:''};

zNodes =[<%for(DbMap dbMap : assortList){%>
	{id:'<%=dbMap.getString("ROW_ID")%>', pId:'<%=dbMap.getString("PARENTID")%>', name:"<%=dbMap.getString("NAME")%>", checked:<%=dbMap.getString("CHECKED")%>},<%}%>
	{}
];


$.fn.zTree.init($("#divTree"), setting, zNodes);
zTreeObj = $.fn.zTree.getZTreeObj("divTree");

function toggle_ztree_all(cbx){
	zTreeObj.checkAllNodes(cbx.checked);
}
</script>
<%if(!msg.emptyMessage()){%>
<script type="text/javascript">
alert('<%=msg.getMessage()%>');
</script>
<%}%>
<%if(msg.getIsSuccess()){ %>
<script type="text/javascript">
location.href='user_list.jsp?act=list<%=params.query("dwid,predwid,pid,cpid")%>';
</script>
<%}%>
</body>
</html>