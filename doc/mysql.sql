-- ----------------------------
-- Table structure for cms_article
-- ----------------------------
CREATE TABLE `cms_article` (
  `ROW_ID` varchar(32) NOT NULL,
  `TITLE` varchar(200) NOT NULL default '',
  `SUBJECT` varchar(100) NOT NULL default '',
  `IPLIMIT` text,
  `CONTENT` longtext,
  `IMGLIST` longtext,
  `WRITER` varchar(100) NOT NULL default '',
  `PUBDATE` varchar(50) NOT NULL default '',
  `INDATE` datetime default NULL,
  `STATE` int(1) NOT NULL default '0',
  `CREATEID` varchar(32) NOT NULL default '',
  `CREATETIME` datetime default NULL,
  `UPDATEID` varchar(32) NOT NULL default '',
  `UPDATETIME` datetime default NULL,
  `IMG` varchar(200) NOT NULL default '',
  `SOURCEID` bigint(20) NOT NULL default '0',
  `SUMMARY` varchar(250) NOT NULL default '',
  `ASSORTID` varchar(32) NOT NULL default '',
  `DWID` varchar(32) NOT NULL default '', 
  `DWBH` varchar(32) NOT NULL default '',
  `DUTY` varchar(50) NOT NULL default '',
  `ISOPEN` int(11) NOT NULL default '0',
  `ISDELETED` int(11) NOT NULL default '0',
  `BACKDES` varchar(200) NOT NULL default '',
  `REMARK` varchar(200) NOT NULL default '',
  `HITS` bigint(20) NOT NULL default '0',
  `AUTHOR` varchar(50) NOT NULL default '',
  `SOURCE` varchar(50) NOT NULL default '',
  `LINK` varchar(100) NOT NULL default '',
  `ISTOP` tinyint(1) NOT NULL default '0',
  `ISINDEX` tinyint(1) NOT NULL default '0', 
  `ORDERNO` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_assort
-- ----------------------------
CREATE TABLE `cms_assort` (
  `ROW_ID` varchar(32) NOT NULL default '',
  `NO` varchar(50) NOT NULL default '',
  `NAME` varchar(20) NOT NULL default '',
  `DES` varchar(200) NOT NULL default '',
  `IPLIMIT` text,
  `PARENTID` varchar(32) NOT NULL default '',
  `SERIAL` bigint(20) NOT NULL default '0',
  `UPDATEID` varchar(32) NOT NULL default '',
  `UPDATETIME` datetime default NULL,
  `PATH` varchar(200) NOT NULL default '',
  `CHILDREN` int(11) NOT NULL default '0',
  `STATE` int(1) NOT NULL default '0',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_dw
-- ----------------------------
CREATE TABLE `cms_dw` (
  `ROW_ID` varchar(32) NOT NULL,
  `DWBH` varchar(10) NOT NULL default '',
  `DWMC` varchar(100) NOT NULL default '',
  `CONTENT` varchar(200) NOT NULL default '',
  `UPDATEID` varchar(32) NOT NULL default '',
  `UPDATETIME` datetime default NULL,
  `PREDWID` varchar(32) NOT NULL default '',
  `STATE` int(1) NOT NULL default '0',
  `ACTIVETIME` datetime default NULL,
  `PATH` varchar(250) NOT NULL default '',
  `CHILDREN` int(11) NOT NULL default '0',
  `CHILDACTIVETIME` datetime default NULL,
  `TEMPLATE` int(1) NOT NULL default '0',
  `DWALIAS` varchar(100) NOT NULL default '',
  `STBNOCOUNT` int(8) NOT NULL default '0',
  `TEMPLATE_HB` int(1) NOT NULL default '0',
  PRIMARY KEY  (`ROW_ID`),
  KEY `I_T_DW_DIRBH` (`DWBH`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_dw_assort
-- ----------------------------
CREATE TABLE `cms_dw_assort` (
  `ROW_ID` varchar(32) NOT NULL,
  `DWID` varchar(32) NOT NULL default '',
  `ASSORTID` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_log
-- ----------------------------
CREATE TABLE `cms_log` (
  `ROW_ID` varchar(32) NOT NULL,
  `RESOURCEID` varchar(32) NOT NULL default '',
  `CONTENT` text,
  `CREATEID` varchar(32) NOT NULL default '',
  `CREATETIME` datetime default NULL,
  `CREATEIP` varchar(20) NOT NULL default '',
  `DWID` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_member
-- ----------------------------
CREATE TABLE `cms_member` (
  `ROW_ID` varchar(32) NOT NULL default '',
  `USERNAME` varchar(30) NOT NULL default '',
  `PASSWD` varchar(128) NOT NULL default '',
  `REALNAME` varchar(10) NOT NULL default '',
  `CONNECT` varchar(20) NOT NULL default '',
  `EMAIL` varchar(100) NOT NULL default '',
  `CREATETIME` datetime default NULL,
  `UPDATEID` varchar(32) NOT NULL default '',
  `UPDATETIME` datetime default NULL,
  `REMARK` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_message
-- ----------------------------
CREATE TABLE `cms_message` (
  `ROW_ID` varchar(32) NOT NULL,
  `PARENTID` varchar(32) NOT NULL default '',
  `ARTICLEID` varchar(32) NOT NULL default '',
  `TITLE` varchar(200) NOT NULL default '',
  `CONTENT` text,
  `WRITER` varchar(100) NOT NULL default '',
  `PHONE` varchar(100) NOT NULL default '',
  `MOBILE` varchar(100) NOT NULL default '',
  `STATE` int(11) NOT NULL default '0',
  `CREATETIME` datetime default NULL,
  `UPDATEID` varchar(32) NOT NULL default '',
  `UPDATETIME` datetime default NULL,
  `CATEGORY` varchar(50) NOT NULL default '',
  `DWID` varchar(32) NOT NULL default '',
  `ISOPEN` int(11) NOT NULL default '0',
  `ISDELETED` int(11) NOT NULL default '0',
  `REPLY` text,
  `HITS` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_notice
-- ----------------------------
CREATE TABLE `cms_notice` (
  `ROW_ID` varchar(32) NOT NULL,
  `TITLE` varchar(200) NOT NULL default '',
  `CONTENT` longtext,
  `STATE` int(1) NOT NULL default '0',
  `CREATEID` varchar(32) NOT NULL default '',
  `CREATETIME` datetime default NULL,
  `UPDATEID` varchar(32) NOT NULL default '',
  `UPDATETIME` datetime default NULL,
  `SUMMARY` varchar(250) NOT NULL default '',
  `DWID` varchar(32) NOT NULL,
  `HITS` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_operation
-- ----------------------------
CREATE TABLE `cms_operation` (
  `ROW_ID` varchar(32) NOT NULL,
  `NAME` varchar(20) NOT NULL default '',
  `IDENTITY` varchar(50) NOT NULL default '',
  `SORTNO` int(11) NOT NULL default '0',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_privilege
-- ----------------------------
CREATE TABLE `cms_privilege` (
  `ROW_ID` varchar(32) NOT NULL,
  `RESOURCEID` varchar(32) NOT NULL default '',
  `OPERATIONID` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_resource
-- ----------------------------
CREATE TABLE `cms_resource` (
  `ROW_ID` varchar(32) NOT NULL,
  `NAME` varchar(20) NOT NULL default '',
  `IDENTITY` varchar(100) NOT NULL default '',
  `CREATETIME` datetime default NULL,
  `SYSID` varchar(32) NOT NULL default '',
  `CHILDREN` int(11) NOT NULL default '0',
  `PATH` varchar(250) NOT NULL default '',
  `PARENTID` varchar(32) NOT NULL default '',
  `EDITUSER` varchar(32) NOT NULL default '',
  `EDITTIME` datetime default NULL,
  `COMMENT` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_role
-- ----------------------------
CREATE TABLE `cms_role` (
  `ROW_ID` varchar(32) NOT NULL,
  `NAME` varchar(20) NOT NULL default '',
  `DES` varchar(200) NOT NULL default '',
  `STATE` int(1) NOT NULL default '0',
  `UPDATEID` varchar(32) NOT NULL default '',
  `UPDATETIME` datetime default NULL,
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_rolepri
-- ----------------------------
CREATE TABLE `cms_rolepri` (
  `ROW_ID` varchar(32) NOT NULL,
  `ROLEID` varchar(32) NOT NULL default '',
  `PRIVILEGEID` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_user
-- ----------------------------
CREATE TABLE `cms_user` (
  `ROW_ID` varchar(32) NOT NULL,
  `NAME` varchar(20) NOT NULL default '',
  `CODE` varchar(20) NOT NULL default '',
  `PASSWD` varchar(100) NOT NULL default '',
  `EMAIL` varchar(100) NOT NULL default '',
  `PHONE` varchar(100) NOT NULL default '',
  `DES` varchar(200) NOT NULL default '',
  `STATE` int(1) NOT NULL default '0',
  `UPDATEID` varchar(32) NOT NULL default '',
  `UPDATETIME` datetime default NULL,
  `LOGIN` bigint(20) NOT NULL default '0',
  `CREATEID` varchar(32) NOT NULL default '',
  `CREATTIME` datetime default NULL,
  `DWID` varchar(32) NOT NULL default '',
  `LOGINTIME` datetime default NULL,
  `TIMES` int(11) NOT NULL default '0',
  `LOGINIP` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_user_assort
-- ----------------------------
CREATE TABLE `cms_user_assort` (
  `ROW_ID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL default '',
  `ASSORTID` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_useractive
-- ----------------------------
CREATE TABLE `cms_useractive` (
  `ROW_ID` varchar(32) NOT NULL default '',
  `USERID` varchar(32) default NULL,
  `USERNAME` varchar(30) default NULL,
  `ACTIVETIME` datetime default NULL,
  `DWID` varchar(32) default NULL,
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_userrole
-- ----------------------------
CREATE TABLE `cms_userrole` (
  `ROW_ID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL default '',
  `ROLEID` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`ROW_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Table structure for cms_variable
-- ----------------------------
CREATE TABLE `cms_variable` (
  `IDENTITY` varchar(50) NOT NULL default '',
  `VAL` text,
  PRIMARY KEY  (`IDENTITY`)
) ENGINE=MyISAM DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `cms_dw` VALUES ('4cbb677c282d417980f4a6295fb9c045', '10', '网站单位', '', '42ef5fe9706b4c98bebdae12186ba089', '2013-07-18 17:29:43', '0', '0', null, '4cbb677c282d417980f4a6295fb9c045,', '0', null, '0', '', '0', '0');

INSERT INTO `cms_operation` VALUES ('1', '查看', 'view', '1');
INSERT INTO `cms_operation` VALUES ('3', '添加', 'add', '3');
INSERT INTO `cms_operation` VALUES ('4', '修改', 'update', '4');
INSERT INTO `cms_operation` VALUES ('5', '删除', 'delete', '5');
INSERT INTO `cms_operation` VALUES ('6', '审核', 'audit', '6');
INSERT INTO `cms_operation` VALUES ('8', '设置', 'setup', '8');
INSERT INTO `cms_operation` VALUES ('10', '回复', 'reply', '10');
INSERT INTO `cms_operation` VALUES ('9', '批示', 'postil', '9');
INSERT INTO `cms_operation` VALUES ('2', '查询', 'search', '2');
INSERT INTO `cms_operation` VALUES ('13', '还原', 'restore', '13');
INSERT INTO `cms_operation` VALUES ('14', '复制', 'copy', '14');
INSERT INTO `cms_operation` VALUES ('15', '发布', 'report', '15');
INSERT INTO `cms_operation` VALUES ('16', '退回', 'back', '16');
INSERT INTO `cms_operation` VALUES ('17', '回复', 'reply', '17');
INSERT INTO `cms_operation` VALUES ('18', '排序', 'order', '18');

INSERT INTO `cms_privilege` VALUES ('a70f77e82ebf4236a6d6a8db6e389a83', 'f553f0a0aef4439d90818b9c44f6367d', '1');
INSERT INTO `cms_privilege` VALUES ('2da553303e934d6597075918a462d056', 'ab204c8692704e3298ae893ad7c78a6a', '8');
INSERT INTO `cms_privilege` VALUES ('a667504481fb4f7fad326edc81a6dc34', 'ab204c8692704e3298ae893ad7c78a6a', '1');
INSERT INTO `cms_privilege` VALUES ('b1267a73c3964c9897b90b5d505eb505', '9dd18ae2ef1b4dc29f8025b45145f5c9', '8');
INSERT INTO `cms_privilege` VALUES ('cb88caea75f84c5599ea86c91a9d33c5', '3022b187dfde4be6a64fa0e961125bc7', '1');
INSERT INTO `cms_privilege` VALUES ('da970e9c3c9445349eb0566da1359c70', '01301550c2564c14a507adc2a75e5007', '1');
INSERT INTO `cms_privilege` VALUES ('76232a60dd5e4369b7751e09b30ef121', '01301550c2564c14a507adc2a75e5007', '2');
INSERT INTO `cms_privilege` VALUES ('14a731804cd34b3ea923f4e641c9179c', '01301550c2564c14a507adc2a75e5007', '3');
INSERT INTO `cms_privilege` VALUES ('68c2a608b3a941f49123f2d51290cb2b', '01301550c2564c14a507adc2a75e5007', '4');
INSERT INTO `cms_privilege` VALUES ('feb352123a434826993e03a232763ff9', '01301550c2564c14a507adc2a75e5007', '5');
INSERT INTO `cms_privilege` VALUES ('8a6278b0c4e24ab68ef13d44c7e5b44b', '861d134cf1f041beace2406875e6aada', '5');
INSERT INTO `cms_privilege` VALUES ('6c5ec58daaf84b479970b2351a2a7369', '861d134cf1f041beace2406875e6aada', '2');
INSERT INTO `cms_privilege` VALUES ('d50f7bcac4a64342a744e2f76fcbd946', '861d134cf1f041beace2406875e6aada', '1');
INSERT INTO `cms_privilege` VALUES ('517b1851c8974a11a844b734f8ea7bd4', '0f0cec4345884fd1996508a141c2d73c', '1');
INSERT INTO `cms_privilege` VALUES ('adfbe47d65144e02a90f99121d35f1d6', '0f0cec4345884fd1996508a141c2d73c', '3');
INSERT INTO `cms_privilege` VALUES ('bb5bc48b4d534c23b4c2b14f12361a25', '0f0cec4345884fd1996508a141c2d73c', '4');
INSERT INTO `cms_privilege` VALUES ('1890ac344ee54312951235bd9dce43f3', '0f0cec4345884fd1996508a141c2d73c', '5');
INSERT INTO `cms_privilege` VALUES ('e9ddbfc98c034e75a6499ffb992fc700', '22019cbadb7a45a6bf963f9ef2ffd184', '4');
INSERT INTO `cms_privilege` VALUES ('a04c70445ed241e7aed2a6530f3401c7', '22019cbadb7a45a6bf963f9ef2ffd184', '1');
INSERT INTO `cms_privilege` VALUES ('88016ba6bb3f4652b6aeb68d20390861', '42225b0c4cba48ba8f1472dd69ad6245', '2');
INSERT INTO `cms_privilege` VALUES ('36e0aa3fbbd8471c97fd9ad0cb5e13c5', '42225b0c4cba48ba8f1472dd69ad6245', '1');
INSERT INTO `cms_privilege` VALUES ('b387b115a6c04f46962d8e914914ae07', '8379f3c71547434e927d63181a6e7137', '15');
INSERT INTO `cms_privilege` VALUES ('c24e1199d44041aa8ec509daab688a4b', '8379f3c71547434e927d63181a6e7137', '13');
INSERT INTO `cms_privilege` VALUES ('14284c682df64aed9bcaaab268debfd9', '8379f3c71547434e927d63181a6e7137', '10');
INSERT INTO `cms_privilege` VALUES ('c30fc056aaf74ae3965f292af0042ca8', '8379f3c71547434e927d63181a6e7137', '6');
INSERT INTO `cms_privilege` VALUES ('2fb3040d65bc40c492888093aebc2d35', '8379f3c71547434e927d63181a6e7137', '5');
INSERT INTO `cms_privilege` VALUES ('2bb8371a4da6456dbcf0bfcc7dc1e449', '8379f3c71547434e927d63181a6e7137', '4');
INSERT INTO `cms_privilege` VALUES ('081aae34edda498a8081d542312bebd9', '8379f3c71547434e927d63181a6e7137', '3');
INSERT INTO `cms_privilege` VALUES ('fb4bbfd89512448bb6db9236d6782f28', '8379f3c71547434e927d63181a6e7137', '2');
INSERT INTO `cms_privilege` VALUES ('74863c9e72834f98bbb152a8d57f0450', '8379f3c71547434e927d63181a6e7137', '1');
INSERT INTO `cms_privilege` VALUES ('c6ad9850fe7a47c4834643068b6b3b28', 'b3ae04f8727449949325a3d6e77771d0', '5');
INSERT INTO `cms_privilege` VALUES ('8fb28d2b5dfc4e339160736dd6d9f6ee', '5e2b03024339434385e50b1947baa070', '5');
INSERT INTO `cms_privilege` VALUES ('42a631fee9cc465484a1af9da2b094ae', '5e2b03024339434385e50b1947baa070', '4');
INSERT INTO `cms_privilege` VALUES ('a51ca3bf927841dab559c693beb02a67', '5e2b03024339434385e50b1947baa070', '3');
INSERT INTO `cms_privilege` VALUES ('df4d1210369c42aaadeb6c9dc60d5d3d', '5e2b03024339434385e50b1947baa070', '1');
INSERT INTO `cms_privilege` VALUES ('c8e9debcc5664f33be932ee62a559497', '9dd18ae2ef1b4dc29f8025b45145f5c9', '1');
INSERT INTO `cms_privilege` VALUES ('d79b7d31a43f4a169cf502ff52dd3f4d', '91df2329d3d042548961a28a3be5160a', '1');
INSERT INTO `cms_privilege` VALUES ('99db0e42ba874ad0a80677597e0ba87a', '91df2329d3d042548961a28a3be5160a', '3');
INSERT INTO `cms_privilege` VALUES ('622aec9560c0424b99fac7c3170e76d6', '91df2329d3d042548961a28a3be5160a', '4');
INSERT INTO `cms_privilege` VALUES ('62e842a78193404fb217389fa1728e9e', '91df2329d3d042548961a28a3be5160a', '5');
INSERT INTO `cms_privilege` VALUES ('15820503da304ce3bd661d88f645ed17', 'b3ae04f8727449949325a3d6e77771d0', '4');
INSERT INTO `cms_privilege` VALUES ('ae4a96c05f3e40b58189fe155d81e123', 'b3ae04f8727449949325a3d6e77771d0', '3');
INSERT INTO `cms_privilege` VALUES ('df8046432e4d4ce7a6cf7c4af6123ecb', 'b3ae04f8727449949325a3d6e77771d0', '1');
INSERT INTO `cms_privilege` VALUES ('02bbae4e22c64eebac4d04add5aed1f3', 'f553f0a0aef4439d90818b9c44f6367d', '8');
INSERT INTO `cms_privilege` VALUES ('beb42fdc46c74aab8bcf0345b5c0e908', '6feca7d9680b4ec5b9f6723e1695d9a5', '1');
INSERT INTO `cms_privilege` VALUES ('4e4e8fd268dc44b5aeb2a0d19ddfdeb6', '6feca7d9680b4ec5b9f6723e1695d9a5', '2');
INSERT INTO `cms_privilege` VALUES ('2f165deb07324995a31a133d0c3395f5', '6feca7d9680b4ec5b9f6723e1695d9a5', '3');
INSERT INTO `cms_privilege` VALUES ('f5cff47b728e4ba3bcd4d36813996bc3', '6feca7d9680b4ec5b9f6723e1695d9a5', '4');
INSERT INTO `cms_privilege` VALUES ('2241cf4df0c94fdea6ad0a17f48ce6ea', '6feca7d9680b4ec5b9f6723e1695d9a5', '5');
INSERT INTO `cms_privilege` VALUES ('853eb5ebf459446b9fd36272c995e42d', '8379f3c71547434e927d63181a6e7137', '16');
INSERT INTO `cms_privilege` VALUES ('45d4d9c3057f456d98c3eab718df1769', 'be8cb65ac7534f95be8a5eb635064f2e', '1');
INSERT INTO `cms_privilege` VALUES ('720c056805414dffa9bc46860d2672da', '0f352f97bd9245348e6f8964174bfe05', '1');
INSERT INTO `cms_privilege` VALUES ('e70b5436f88a48f8ac2fd3743f286153', '1759f3ed11c44f899aa059190dab6647', '1');
INSERT INTO `cms_privilege` VALUES ('ee0391be728343a7b38b85e17f39e01d', '791f75a3541a4d808973c3b6b07ee607', '1');
INSERT INTO `cms_privilege` VALUES ('e6c92e1fd41245649b5f91954de45cf4', '9d9463ac0fec4cd29eb81806464195b9', '5');
INSERT INTO `cms_privilege` VALUES ('18b8c00a09ff4ab39352da3562693e9d', '9d9463ac0fec4cd29eb81806464195b9', '4');
INSERT INTO `cms_privilege` VALUES ('737a19eb1de1440bab5dfda2769b4c4b', '9d9463ac0fec4cd29eb81806464195b9', '3');
INSERT INTO `cms_privilege` VALUES ('816cd43d63a54abc9ff382b3a1f2f272', '9d9463ac0fec4cd29eb81806464195b9', '2');
INSERT INTO `cms_privilege` VALUES ('17dade74081f415db361fc94c2bd8bec', '9d9463ac0fec4cd29eb81806464195b9', '1');
INSERT INTO `cms_privilege` VALUES ('bbb0ff859830492da4043e3f46eeb232', '02a2608fa68848c78e08f3395c565555', '1');
INSERT INTO `cms_privilege` VALUES ('32e0b981eec9452280b66e4648e31948', '02a2608fa68848c78e08f3395c565555', '2');
INSERT INTO `cms_privilege` VALUES ('56a97061cbd4402d91c37d0d5fad9572', '25c058c54d0b417c8412376696051ffd', '1');
INSERT INTO `cms_privilege` VALUES ('a384773a82024076932fd2a96a10be1a', '25c058c54d0b417c8412376696051ffd', '3');
INSERT INTO `cms_privilege` VALUES ('b4538e4af70546b690b8cc12b0f2cda0', '291fd69816de4362a4262b727e5553f4', '8');
INSERT INTO `cms_privilege` VALUES ('f4f5d09a19d4487489306f8fec5d49a9', '022e31e9f1134a5ca1af2abdfb8065e9', '6');
INSERT INTO `cms_privilege` VALUES ('5e483c7c626b4364a85bf0ef0ae6aa42', '022e31e9f1134a5ca1af2abdfb8065e9', '5');
INSERT INTO `cms_privilege` VALUES ('096a375443f641638aec821fb3bd51b0', '022e31e9f1134a5ca1af2abdfb8065e9', '4');
INSERT INTO `cms_privilege` VALUES ('fb36cd7579a94f2d81730c41944d4dd8', '022e31e9f1134a5ca1af2abdfb8065e9', '3');
INSERT INTO `cms_privilege` VALUES ('b793b95bd92147eb9e42eb274dd25d32', '022e31e9f1134a5ca1af2abdfb8065e9', '2');
INSERT INTO `cms_privilege` VALUES ('f4a4cf0a0367460fafee0b5c0f8f1f08', '022e31e9f1134a5ca1af2abdfb8065e9', '1');
INSERT INTO `cms_privilege` VALUES ('c7c8229b695a4cd780974a8a9d4281e0', 'c53405ba38244f529515fe4bb1b96a7b', '5');
INSERT INTO `cms_privilege` VALUES ('e9a132c01a11497291a8301d38eaaa1f', 'c53405ba38244f529515fe4bb1b96a7b', '4');
INSERT INTO `cms_privilege` VALUES ('285342ea6d054aa8adca503c799f561d', 'c53405ba38244f529515fe4bb1b96a7b', '3');
INSERT INTO `cms_privilege` VALUES ('10dfe625a74d4131918013a66a6405e6', 'c53405ba38244f529515fe4bb1b96a7b', '2');
INSERT INTO `cms_privilege` VALUES ('2fa12d1dc46c42c29dd5c20f8a6cbb9a', 'c53405ba38244f529515fe4bb1b96a7b', '1');
INSERT INTO `cms_privilege` VALUES ('284aea685eb7429997dab0a0f91d0d0c', '8dcc5d34750d4d359480425d13c5d760', '16');
INSERT INTO `cms_privilege` VALUES ('200c258410784957b140f14875bcd1a7', '8dcc5d34750d4d359480425d13c5d760', '5');
INSERT INTO `cms_privilege` VALUES ('bc26d58ff0f24c278da3baa2a9606909', '8dcc5d34750d4d359480425d13c5d760', '4');
INSERT INTO `cms_privilege` VALUES ('4e05556478c94685a495bdfa28a27915', '8dcc5d34750d4d359480425d13c5d760', '3');
INSERT INTO `cms_privilege` VALUES ('4f732e0dc8bb4541a568e4f512c625eb', '8dcc5d34750d4d359480425d13c5d760', '2');
INSERT INTO `cms_privilege` VALUES ('a75a65a6b58a45869bb81d8eb46e48ed', '8dcc5d34750d4d359480425d13c5d760', '1');
INSERT INTO `cms_privilege` VALUES ('a07277b1ad0f4ff1ae16c9525d2db6a1', 'c53405ba38244f529515fe4bb1b96a7b', '6');
INSERT INTO `cms_privilege` VALUES ('2321fce09869418fbdcdecffc0154750', 'fe1c05ac952c412aa8b577396b96edf5', '1');
INSERT INTO `cms_privilege` VALUES ('5533094c92aa4e3c801f181f9be23ee1', 'fe1c05ac952c412aa8b577396b96edf5', '2');
INSERT INTO `cms_privilege` VALUES ('2004d1f6327c479fae471a4f8d890b3d', '453963e656284e85a1ceee080b4fa529', '1');
INSERT INTO `cms_privilege` VALUES ('d333441d87b5442d8ad718b31b384642', '453963e656284e85a1ceee080b4fa529', '2');
INSERT INTO `cms_privilege` VALUES ('6b93b45f40424494b8714b6ac9ccf758', '453963e656284e85a1ceee080b4fa529', '5');
INSERT INTO `cms_privilege` VALUES ('203b633107014db1b422ce40dbb8aa34', '453963e656284e85a1ceee080b4fa529', '13');
INSERT INTO `cms_privilege` VALUES ('6d883930be1641d4b9a2713a4ec56447', '89921d9180a2434b98ad3d2d03a177a6', '1');
INSERT INTO `cms_privilege` VALUES ('aa1da01788d2410c932f382f66d7b9be', '89921d9180a2434b98ad3d2d03a177a6', '4');
INSERT INTO `cms_privilege` VALUES ('5035c49f69644c5d8fde1af22f615d9d', '291fd69816de4362a4262b727e5553f4', '5');
INSERT INTO `cms_privilege` VALUES ('2d7d2a3b40e54efe8afb57d431013b01', '4e40622bec5241fd9d878e076149f79c', '5');
INSERT INTO `cms_privilege` VALUES ('426d2e8fe0fe4613aca8d2539db5bbff', '4e40622bec5241fd9d878e076149f79c', '4');
INSERT INTO `cms_privilege` VALUES ('4fe6628897564fa4a93e4a94ca70e4ce', '4e40622bec5241fd9d878e076149f79c', '3');
INSERT INTO `cms_privilege` VALUES ('00f1280244ca4c36bae92b40eae00d6c', '4e40622bec5241fd9d878e076149f79c', '2');
INSERT INTO `cms_privilege` VALUES ('cc028a4e70c44e35b0e2df67d8ddea6e', '4e40622bec5241fd9d878e076149f79c', '1');
INSERT INTO `cms_privilege` VALUES ('f879e500f5fb410dbb2d6bdcbc7064b1', '291fd69816de4362a4262b727e5553f4', '4');
INSERT INTO `cms_privilege` VALUES ('c80f04bc52a24d3e8dfbf0aed5d40349', '291fd69816de4362a4262b727e5553f4', '3');
INSERT INTO `cms_privilege` VALUES ('7a92f4bbd7724f7ea7daf35ac2970ebf', '291fd69816de4362a4262b727e5553f4', '1');
INSERT INTO `cms_privilege` VALUES ('85b18580ec7b4e65b3fc3a4541c799d4', '6c6bc932d8074503bfb4de75cfa4503c', '1');
INSERT INTO `cms_privilege` VALUES ('de738f0e2420413f84a00278c93c150e', '6c6bc932d8074503bfb4de75cfa4503c', '3');
INSERT INTO `cms_privilege` VALUES ('682c9ecbb1834298aa6ce154b6e82bb6', '6c6bc932d8074503bfb4de75cfa4503c', '4');
INSERT INTO `cms_privilege` VALUES ('48308251ce624eaebcdd4ccce2301375', '6c6bc932d8074503bfb4de75cfa4503c', '5');
INSERT INTO `cms_privilege` VALUES ('f4d893bd497245299af3a8880e435544', '5ba1dede632240428a7c6b09d070e6b5', '1');
INSERT INTO `cms_privilege` VALUES ('df5ac44d905941808e300f725738d450', '5ba1dede632240428a7c6b09d070e6b5', '8');
INSERT INTO `cms_privilege` VALUES ('91da09a2751942b0beab08bda305c9e5', '31796c8ad25a482a84dd47e22a6409d1', '1');
INSERT INTO `cms_privilege` VALUES ('b941a74427c54a48b65fe0a197f9c9e0', '31796c8ad25a482a84dd47e22a6409d1', '3');
INSERT INTO `cms_privilege` VALUES ('ab952c459791426bb98810f83944af59', '31796c8ad25a482a84dd47e22a6409d1', '4');
INSERT INTO `cms_privilege` VALUES ('ccad3695da964bbaa0e53595f514985a', '31796c8ad25a482a84dd47e22a6409d1', '5');
INSERT INTO `cms_privilege` VALUES ('b45e5303ba0d4a84b3f889ace7682f95', 'ff35c1fbed1a4e4aab8ed53162993e2b', '1');
INSERT INTO `cms_privilege` VALUES ('1ed935c9fc71459589062f5dd063bd70', 'ff35c1fbed1a4e4aab8ed53162993e2b', '8');
INSERT INTO `cms_privilege` VALUES ('9ec33b86fc2a43d799eb4e61f9fc75f7', '8f82702014494a4ba8c23299a8554fec', '1');
INSERT INTO `cms_privilege` VALUES ('43abd33dde83442d9ef0d6ed0668e22b', '8f82702014494a4ba8c23299a8554fec', '2');
INSERT INTO `cms_privilege` VALUES ('9fac8ca5cbfc478795b6a533bea00bff', '8f82702014494a4ba8c23299a8554fec', '5');
INSERT INTO `cms_privilege` VALUES ('fbc9ec03c6cb49f3bff66ecf948e14e1', '9baf5cfda18f4b42836857f2e5097c73', '1');
INSERT INTO `cms_privilege` VALUES ('5e38ca10515048f9b502e4cef8b26c44', '9baf5cfda18f4b42836857f2e5097c73', '3');
INSERT INTO `cms_privilege` VALUES ('76eb781ddeb94a38aa25090e32f47aa0', '9baf5cfda18f4b42836857f2e5097c73', '4');
INSERT INTO `cms_privilege` VALUES ('c2b01ea60d3d4612ae6a3cdf6bb086fc', '9baf5cfda18f4b42836857f2e5097c73', '5');
INSERT INTO `cms_privilege` VALUES ('c033396876d2441e89cd4a5eeb0cc80c', '9ae25f7ea1cf44eaa25036a66e0bf131', '10');
INSERT INTO `cms_privilege` VALUES ('30646d6c7c534de48b10b40181ec61c9', '9ae25f7ea1cf44eaa25036a66e0bf131', '5');
INSERT INTO `cms_privilege` VALUES ('7d2bbd7684164447af1170f9a688b1ad', '9ae25f7ea1cf44eaa25036a66e0bf131', '4');
INSERT INTO `cms_privilege` VALUES ('e77cd48a0498446e9c89f64ce95adb8a', '9ae25f7ea1cf44eaa25036a66e0bf131', '3');
INSERT INTO `cms_privilege` VALUES ('01c2ba9201a94d91a6c96817b4c124e7', '9ae25f7ea1cf44eaa25036a66e0bf131', '2');
INSERT INTO `cms_privilege` VALUES ('674cc9e6d84344d79787d0fe7a0f13e2', '9ae25f7ea1cf44eaa25036a66e0bf131', '1');
INSERT INTO `cms_privilege` VALUES ('57fed44afb644f8cbc20aeec1a385eec', '47a72df968634ffdb179d8b70092d552', '1');
INSERT INTO `cms_privilege` VALUES ('4c7a712ea0744cd4ae487d5632276a28', '47a72df968634ffdb179d8b70092d552', '2');
INSERT INTO `cms_privilege` VALUES ('113c69c68b9549e99dca335110f99004', '47a72df968634ffdb179d8b70092d552', '3');
INSERT INTO `cms_privilege` VALUES ('6051563466694150a757a61bdac07fbf', '47a72df968634ffdb179d8b70092d552', '4');
INSERT INTO `cms_privilege` VALUES ('40d445df2cdc413c8af83541e89cb1fe', '47a72df968634ffdb179d8b70092d552', '5');
INSERT INTO `cms_privilege` VALUES ('479486e7e79e4f8ab8e7410707d2eff5', '18c89bc3366647b69d0871dd2bbf0a49', '1');
INSERT INTO `cms_privilege` VALUES ('2918285549b44bbdae10d206e3341245', '18c89bc3366647b69d0871dd2bbf0a49', '4');
INSERT INTO `cms_privilege` VALUES ('ee64d01c925f44559cafa9eb69a7b316', '18c89bc3366647b69d0871dd2bbf0a49', '5');
INSERT INTO `cms_privilege` VALUES ('a625074f4396468f9c1742f4ecc9bf29', '9edbaee7c9a14fa8971cd7ccb268728e', '8');
INSERT INTO `cms_privilege` VALUES ('258acfaf15e942e482ae188336775cbb', '8572181c7c5344b2b32337ffa1be93a7', '1');
INSERT INTO `cms_privilege` VALUES ('470a224d93014ed997cd4d76db86eaa0', '8572181c7c5344b2b32337ffa1be93a7', '3');
INSERT INTO `cms_privilege` VALUES ('77a4d7faa6de48c1962d580e355588d7', '8572181c7c5344b2b32337ffa1be93a7', '4');
INSERT INTO `cms_privilege` VALUES ('40d80c92f1ac40818b8caaa6034087c5', '8572181c7c5344b2b32337ffa1be93a7', '5');

INSERT INTO `cms_resource` VALUES ('be8cb65ac7534f95be8a5eb635064f2e', '信息发布', '10', '2013-07-17 16:00:02', '', '8', 'be8cb65ac7534f95be8a5eb635064f2e,', '0', '', null, '');
INSERT INTO `cms_resource` VALUES ('0f352f97bd9245348e6f8964174bfe05', '系统管理', '40', '2013-07-17 16:00:41', '', '3', '0f352f97bd9245348e6f8964174bfe05,', '0', '', null, '');
INSERT INTO `cms_resource` VALUES ('1759f3ed11c44f899aa059190dab6647', '用户管理', '30', '2013-07-17 16:00:50', '', '4', '1759f3ed11c44f899aa059190dab6647,', '0', '', null, '');
INSERT INTO `cms_resource` VALUES ('791f75a3541a4d808973c3b6b07ee607', '插件功能', '20', '2013-07-17 16:01:18', '', '2', '791f75a3541a4d808973c3b6b07ee607,', '0', '', null, '');
INSERT INTO `cms_resource` VALUES ('9d9463ac0fec4cd29eb81806464195b9', '通知公告', '1001', '2013-07-17 16:03:08', '', '0', 'be8cb65ac7534f95be8a5eb635064f2e,9d9463ac0fec4cd29eb81806464195b9,', 'be8cb65ac7534f95be8a5eb635064f2e', '', null, '');
INSERT INTO `cms_resource` VALUES ('02a2608fa68848c78e08f3395c565555', '文章统计', '1002', '2013-07-17 16:03:50', '', '0', 'be8cb65ac7534f95be8a5eb635064f2e,02a2608fa68848c78e08f3395c565555,', 'be8cb65ac7534f95be8a5eb635064f2e', '', null, '');
INSERT INTO `cms_resource` VALUES ('25c058c54d0b417c8412376696051ffd', '快速发布', '1003', '2013-07-17 16:04:04', '', '0', 'be8cb65ac7534f95be8a5eb635064f2e,25c058c54d0b417c8412376696051ffd,', 'be8cb65ac7534f95be8a5eb635064f2e', '', null, '');
INSERT INTO `cms_resource` VALUES ('022e31e9f1134a5ca1af2abdfb8065e9', '待审核文章', '1004', '2013-07-17 16:05:05', '', '0', 'be8cb65ac7534f95be8a5eb635064f2e,022e31e9f1134a5ca1af2abdfb8065e9,', 'be8cb65ac7534f95be8a5eb635064f2e', '', null, '');
INSERT INTO `cms_resource` VALUES ('c53405ba38244f529515fe4bb1b96a7b', '未通过文章', '1005', '2013-07-17 16:05:50', '', '0', 'be8cb65ac7534f95be8a5eb635064f2e,c53405ba38244f529515fe4bb1b96a7b,', 'be8cb65ac7534f95be8a5eb635064f2e', '', null, '');
INSERT INTO `cms_resource` VALUES ('8dcc5d34750d4d359480425d13c5d760', '已审核文章', '1006', '2013-07-17 16:06:49', '', '0', 'be8cb65ac7534f95be8a5eb635064f2e,8dcc5d34750d4d359480425d13c5d760,', 'be8cb65ac7534f95be8a5eb635064f2e', '', null, '');
INSERT INTO `cms_resource` VALUES ('fe1c05ac952c412aa8b577396b96edf5', '共享文章', '1007', '2013-07-17 16:07:40', '', '0', 'be8cb65ac7534f95be8a5eb635064f2e,fe1c05ac952c412aa8b577396b96edf5,', 'be8cb65ac7534f95be8a5eb635064f2e', '', null, '');
INSERT INTO `cms_resource` VALUES ('453963e656284e85a1ceee080b4fa529', '回收站', '1008', '2013-07-17 16:08:13', '', '0', 'be8cb65ac7534f95be8a5eb635064f2e,453963e656284e85a1ceee080b4fa529,', 'be8cb65ac7534f95be8a5eb635064f2e', '', null, '');
INSERT INTO `cms_resource` VALUES ('18c89bc3366647b69d0871dd2bbf0a49', '草稿箱', '1009', '2013-07-17 16:08:13', '', '0', 'be8cb65ac7534f95be8a5eb635064f2e,18c89bc3366647b69d0871dd2bbf0a49,', 'be8cb65ac7534f95be8a5eb635064f2e', '', null, '');
INSERT INTO `cms_resource` VALUES ('89921d9180a2434b98ad3d2d03a177a6', '个人信息', '3001', '2013-07-17 16:10:03', '', '0', '1759f3ed11c44f899aa059190dab6647,89921d9180a2434b98ad3d2d03a177a6,', '1759f3ed11c44f899aa059190dab6647', '', null, '');
INSERT INTO `cms_resource` VALUES ('4e40622bec5241fd9d878e076149f79c', '单位', '3002', '2013-07-17 16:10:50', '', '2', '1759f3ed11c44f899aa059190dab6647,4e40622bec5241fd9d878e076149f79c,', '1759f3ed11c44f899aa059190dab6647', '', null, '');
INSERT INTO `cms_resource` VALUES ('291fd69816de4362a4262b727e5553f4', '角色', '3003', '2013-07-17 16:11:23', '', '0', '1759f3ed11c44f899aa059190dab6647,291fd69816de4362a4262b727e5553f4,', '1759f3ed11c44f899aa059190dab6647', '', null, '');
INSERT INTO `cms_resource` VALUES ('6c6bc932d8074503bfb4de75cfa4503c', '权限', '3004', '2013-07-17 16:11:35', '', '0', '1759f3ed11c44f899aa059190dab6647,6c6bc932d8074503bfb4de75cfa4503c,', '1759f3ed11c44f899aa059190dab6647', '', null, '');
INSERT INTO `cms_resource` VALUES ('5ba1dede632240428a7c6b09d070e6b5', '分类', '300201', '2013-07-17 16:11:52', '', '0', '1759f3ed11c44f899aa059190dab6647,4e40622bec5241fd9d878e076149f79c,5ba1dede632240428a7c6b09d070e6b5,', '', null, '');
INSERT INTO `cms_resource` VALUES ('31796c8ad25a482a84dd47e22a6409d1', '用户', '300202', '2013-07-17 16:12:11', '', '0', '1759f3ed11c44f899aa059190dab6647,4e40622bec5241fd9d878e076149f79c,31796c8ad25a482a84dd47e22a6409d1,', '', null, '');
INSERT INTO `cms_resource` VALUES ('ff35c1fbed1a4e4aab8ed53162993e2b', '系统参数', '4001', '2013-07-17 16:12:49', '', '0', '0f352f97bd9245348e6f8964174bfe05,ff35c1fbed1a4e4aab8ed53162993e2b,', '0f352f97bd9245348e6f8964174bfe05', '', null, '');
INSERT INTO `cms_resource` VALUES ('8f82702014494a4ba8c23299a8554fec', '日志', '4002', '2013-07-17 16:13:01', '', '0', '0f352f97bd9245348e6f8964174bfe05,8f82702014494a4ba8c23299a8554fec,', '0f352f97bd9245348e6f8964174bfe05', '', null, '');
INSERT INTO `cms_resource` VALUES ('9baf5cfda18f4b42836857f2e5097c73', '文章分类', '4003', '2013-07-17 16:13:29', '', '0', '0f352f97bd9245348e6f8964174bfe05,9baf5cfda18f4b42836857f2e5097c73,', '0f352f97bd9245348e6f8964174bfe05', '', null, '');
INSERT INTO `cms_resource` VALUES ('9ae25f7ea1cf44eaa25036a66e0bf131', '留言板', '2001', '2013-07-17 16:15:01', '', '0', '791f75a3541a4d808973c3b6b07ee607,9ae25f7ea1cf44eaa25036a66e0bf131,', '791f75a3541a4d808973c3b6b07ee607', '', null, '');
INSERT INTO `cms_resource` VALUES ('47a72df968634ffdb179d8b70092d552', '用户', '2002', '2013-12-10 15:36:51', '', '0', '791f75a3541a4d808973c3b6b07ee607,47a72df968634ffdb179d8b70092d552,', '791f75a3541a4d808973c3b6b07ee607', '', null, '');
INSERT INTO `cms_resource` VALUES ('8572181c7c5344b2b32337ffa1be93a7', '模板', '2003', '2013-12-03 21:53:15', '', '0', '791f75a3541a4d808973c3b6b07ee607,8572181c7c5344b2b32337ffa1be93a7,', '791f75a3541a4d808973c3b6b07ee607', '', null, '');
INSERT INTO `cms_resource` VALUES ('9edbaee7c9a14fa8971cd7ccb268728e', 'IP限制', '1010', '2015-07-01 16:43:26', '', '0', 'be8cb65ac7534f95be8a5eb635064f2e,9edbaee7c9a14fa8971cd7ccb268728e,', 'be8cb65ac7534f95be8a5eb635064f2e', '', null, '');

INSERT INTO `cms_role` VALUES ('5adaf0ad9ad54703a975c536a94202e3', '站点管理员', '', '0', '', null);
INSERT INTO `cms_role` VALUES ('4640f8c6fe5248c38adcfaa01a55babb', '文章发布员', '', '0', '', null);
INSERT INTO `cms_role` VALUES ('b9d8dc92128e46bca71757d31cbc8aff', '文章审核员', '', '0', '', null);

INSERT INTO `cms_rolepri` VALUES ('c1983b73e1324b18acc8272e95d38c32', '4640f8c6fe5248c38adcfaa01a55babb', '6d883930be1641d4b9a2713a4ec56447');
INSERT INTO `cms_rolepri` VALUES ('532868e523254fcabf22bf841db9c125', '4640f8c6fe5248c38adcfaa01a55babb', 'e70b5436f88a48f8ac2fd3743f286153');
INSERT INTO `cms_rolepri` VALUES ('3b2d34e0668341daa7c2b15658df243d', '4640f8c6fe5248c38adcfaa01a55babb', '01c2ba9201a94d91a6c96817b4c124e7');
INSERT INTO `cms_rolepri` VALUES ('65c835f357bd43518d2343544d9980d3', '5adaf0ad9ad54703a975c536a94202e3', 'fbc9ec03c6cb49f3bff66ecf948e14e1');
INSERT INTO `cms_rolepri` VALUES ('efcbe68cfccc44daab032ad8ce2801cd', '5adaf0ad9ad54703a975c536a94202e3', '9fac8ca5cbfc478795b6a533bea00bff');
INSERT INTO `cms_rolepri` VALUES ('f48b1b9767e34f34a7b8712145252d46', '5adaf0ad9ad54703a975c536a94202e3', '43abd33dde83442d9ef0d6ed0668e22b');
INSERT INTO `cms_rolepri` VALUES ('d35ea383bc694edc8f10c9c01aef7f3e', '5adaf0ad9ad54703a975c536a94202e3', '9ec33b86fc2a43d799eb4e61f9fc75f7');
INSERT INTO `cms_rolepri` VALUES ('33b6d8af3de545218fc11473da86643e', '5adaf0ad9ad54703a975c536a94202e3', '1ed935c9fc71459589062f5dd063bd70');
INSERT INTO `cms_rolepri` VALUES ('5eae04f998874b98a5864fc4660af6e0', '5adaf0ad9ad54703a975c536a94202e3', 'b45e5303ba0d4a84b3f889ace7682f95');
INSERT INTO `cms_rolepri` VALUES ('413837e12c054881ac49ea0ecb359b7b', '5adaf0ad9ad54703a975c536a94202e3', '720c056805414dffa9bc46860d2672da');
INSERT INTO `cms_rolepri` VALUES ('5c6e6cfd32e149d2b985e3808a79c3e8', '5adaf0ad9ad54703a975c536a94202e3', '48308251ce624eaebcdd4ccce2301375');
INSERT INTO `cms_rolepri` VALUES ('5b485a689a2249e39a9de2a8004e429b', '5adaf0ad9ad54703a975c536a94202e3', '682c9ecbb1834298aa6ce154b6e82bb6');
INSERT INTO `cms_rolepri` VALUES ('43176241fff04e42a7fc4a5de15fa6c8', '5adaf0ad9ad54703a975c536a94202e3', 'de738f0e2420413f84a00278c93c150e');
INSERT INTO `cms_rolepri` VALUES ('06bbc9fb4a8a419291cdf090ec7b72ef', '5adaf0ad9ad54703a975c536a94202e3', '85b18580ec7b4e65b3fc3a4541c799d4');
INSERT INTO `cms_rolepri` VALUES ('b3c7e89ccc6246f2b8d9c3b3686ac640', '5adaf0ad9ad54703a975c536a94202e3', 'b4538e4af70546b690b8cc12b0f2cda0');
INSERT INTO `cms_rolepri` VALUES ('5e805522064b4457b96722de0955ac61', '5adaf0ad9ad54703a975c536a94202e3', '5035c49f69644c5d8fde1af22f615d9d');
INSERT INTO `cms_rolepri` VALUES ('028c9d912e2142faaa24e78212f1c4d7', '5adaf0ad9ad54703a975c536a94202e3', 'f879e500f5fb410dbb2d6bdcbc7064b1');
INSERT INTO `cms_rolepri` VALUES ('6ff7b49312084bdabbee2b040a5bbf4a', '5adaf0ad9ad54703a975c536a94202e3', 'c80f04bc52a24d3e8dfbf0aed5d40349');
INSERT INTO `cms_rolepri` VALUES ('15cd697d150046b7a9ff76b48be057a1', '5adaf0ad9ad54703a975c536a94202e3', '7a92f4bbd7724f7ea7daf35ac2970ebf');
INSERT INTO `cms_rolepri` VALUES ('06c0b797de6c4d859eff7aa0b9c3b9d0', '5adaf0ad9ad54703a975c536a94202e3', 'ccad3695da964bbaa0e53595f514985a');
INSERT INTO `cms_rolepri` VALUES ('26f9a70d84d24bae8d5aa3fc067d2369', '5adaf0ad9ad54703a975c536a94202e3', 'ab952c459791426bb98810f83944af59');
INSERT INTO `cms_rolepri` VALUES ('d56b5eccca6b46588d50bd3c1fdd3aaf', '5adaf0ad9ad54703a975c536a94202e3', 'b941a74427c54a48b65fe0a197f9c9e0');
INSERT INTO `cms_rolepri` VALUES ('3cc0da1affcd48b58eebe795528fe368', '5adaf0ad9ad54703a975c536a94202e3', '91da09a2751942b0beab08bda305c9e5');
INSERT INTO `cms_rolepri` VALUES ('6a718108ec4646af92f2ac7b281b828b', '4640f8c6fe5248c38adcfaa01a55babb', '674cc9e6d84344d79787d0fe7a0f13e2');
INSERT INTO `cms_rolepri` VALUES ('f9d8e00a56b9417396c22b93e5a9b9ca', '4640f8c6fe5248c38adcfaa01a55babb', 'ee0391be728343a7b38b85e17f39e01d');
INSERT INTO `cms_rolepri` VALUES ('73c49c0317d946789925245cee1b04ce', '4640f8c6fe5248c38adcfaa01a55babb', 'ee64d01c925f44559cafa9eb69a7b316');
INSERT INTO `cms_rolepri` VALUES ('037a023abe6941aa85b6af1b15d0ce37', '4640f8c6fe5248c38adcfaa01a55babb', '2918285549b44bbdae10d206e3341245');
INSERT INTO `cms_rolepri` VALUES ('3259498aa4834fad8ef12881e4f9e930', '4640f8c6fe5248c38adcfaa01a55babb', '479486e7e79e4f8ab8e7410707d2eff5');
INSERT INTO `cms_rolepri` VALUES ('72ceebc3ffd94018b89e7570884b8a6d', '4640f8c6fe5248c38adcfaa01a55babb', '203b633107014db1b422ce40dbb8aa34');
INSERT INTO `cms_rolepri` VALUES ('98e2fc72d9b44f1885580ca3e98998b4', '4640f8c6fe5248c38adcfaa01a55babb', '6b93b45f40424494b8714b6ac9ccf758');
INSERT INTO `cms_rolepri` VALUES ('6be5717eb38d4b5596dc9f8e6578d4af', '5adaf0ad9ad54703a975c536a94202e3', 'df5ac44d905941808e300f725738d450');
INSERT INTO `cms_rolepri` VALUES ('1c82c8895f0744c28bceebfeffce5eeb', '5adaf0ad9ad54703a975c536a94202e3', 'f4d893bd497245299af3a8880e435544');
INSERT INTO `cms_rolepri` VALUES ('a42af3ce512c45d1a8232d9227b48131', '5adaf0ad9ad54703a975c536a94202e3', '2d7d2a3b40e54efe8afb57d431013b01');
INSERT INTO `cms_rolepri` VALUES ('0fb5809e3f514a0a9f74513d0e6fbe08', '5adaf0ad9ad54703a975c536a94202e3', '426d2e8fe0fe4613aca8d2539db5bbff');
INSERT INTO `cms_rolepri` VALUES ('435a398d90bb4989b00ddfa39f1597b4', '5adaf0ad9ad54703a975c536a94202e3', '4fe6628897564fa4a93e4a94ca70e4ce');
INSERT INTO `cms_rolepri` VALUES ('f582a2a7dc2e49abb8cbe0c6ed3744a3', '5adaf0ad9ad54703a975c536a94202e3', '00f1280244ca4c36bae92b40eae00d6c');
INSERT INTO `cms_rolepri` VALUES ('30a530315e274c4f9b2039b11915eab8', '5adaf0ad9ad54703a975c536a94202e3', 'cc028a4e70c44e35b0e2df67d8ddea6e');
INSERT INTO `cms_rolepri` VALUES ('57deb51ed6434dbb81aff5fa7285387f', '5adaf0ad9ad54703a975c536a94202e3', 'aa1da01788d2410c932f382f66d7b9be');
INSERT INTO `cms_rolepri` VALUES ('409950ec166b40b3bff5ff4e6f8ef2c4', '5adaf0ad9ad54703a975c536a94202e3', '6d883930be1641d4b9a2713a4ec56447');
INSERT INTO `cms_rolepri` VALUES ('ec7baa0f8d944014b5fff3a91e4cd26e', '4640f8c6fe5248c38adcfaa01a55babb', 'd333441d87b5442d8ad718b31b384642');
INSERT INTO `cms_rolepri` VALUES ('30ad0dfcff0a4836b84cb60debfc5474', '4640f8c6fe5248c38adcfaa01a55babb', '2004d1f6327c479fae471a4f8d890b3d');
INSERT INTO `cms_rolepri` VALUES ('bd0accca89df408ba86efcca4a9e8369', 'b9d8dc92128e46bca71757d31cbc8aff', '1ed935c9fc71459589062f5dd063bd70');
INSERT INTO `cms_rolepri` VALUES ('71412a44911041e180b0dfe3350ae284', 'b9d8dc92128e46bca71757d31cbc8aff', 'b45e5303ba0d4a84b3f889ace7682f95');
INSERT INTO `cms_rolepri` VALUES ('648c2db3fec84ce6a404caf480685169', 'b9d8dc92128e46bca71757d31cbc8aff', '720c056805414dffa9bc46860d2672da');
INSERT INTO `cms_rolepri` VALUES ('12031877a7fe4d518ad88d9beeb2781f', 'b9d8dc92128e46bca71757d31cbc8aff', 'aa1da01788d2410c932f382f66d7b9be');
INSERT INTO `cms_rolepri` VALUES ('f3330b552569408699d756b570205b4a', 'b9d8dc92128e46bca71757d31cbc8aff', '6d883930be1641d4b9a2713a4ec56447');
INSERT INTO `cms_rolepri` VALUES ('73d88861be714af8a67f36582be4f585', 'b9d8dc92128e46bca71757d31cbc8aff', 'e70b5436f88a48f8ac2fd3743f286153');
INSERT INTO `cms_rolepri` VALUES ('4a4b30fc793d4feb8ea9be8b75291877', 'b9d8dc92128e46bca71757d31cbc8aff', 'c033396876d2441e89cd4a5eeb0cc80c');
INSERT INTO `cms_rolepri` VALUES ('4f9dff19ac024910bcc05356b9fc8220', 'b9d8dc92128e46bca71757d31cbc8aff', '30646d6c7c534de48b10b40181ec61c9');
INSERT INTO `cms_rolepri` VALUES ('9ead8ab69cd84dffa9726cf8013c3a8e', 'b9d8dc92128e46bca71757d31cbc8aff', '7d2bbd7684164447af1170f9a688b1ad');
INSERT INTO `cms_rolepri` VALUES ('ac2614486ebe46e892713e5b94c8a67f', 'b9d8dc92128e46bca71757d31cbc8aff', 'e77cd48a0498446e9c89f64ce95adb8a');
INSERT INTO `cms_rolepri` VALUES ('a743c58bce6140ed9cd5dfa3de21bf55', 'b9d8dc92128e46bca71757d31cbc8aff', '01c2ba9201a94d91a6c96817b4c124e7');
INSERT INTO `cms_rolepri` VALUES ('0fa25529ffe2462ca0bee2cf6e4aba1b', 'b9d8dc92128e46bca71757d31cbc8aff', '674cc9e6d84344d79787d0fe7a0f13e2');
INSERT INTO `cms_rolepri` VALUES ('9611e45c75e64bf286450a9254a50c9f', 'b9d8dc92128e46bca71757d31cbc8aff', 'ee0391be728343a7b38b85e17f39e01d');
INSERT INTO `cms_rolepri` VALUES ('5f4bc82234764d628b452eb101b2e35b', '5adaf0ad9ad54703a975c536a94202e3', 'e70b5436f88a48f8ac2fd3743f286153');
INSERT INTO `cms_rolepri` VALUES ('4d1369a71231414a951ad469e7a4511b', '5adaf0ad9ad54703a975c536a94202e3', '40d445df2cdc413c8af83541e89cb1fe');
INSERT INTO `cms_rolepri` VALUES ('a593d65b3e304c74ad229655a94a6a50', '5adaf0ad9ad54703a975c536a94202e3', '6051563466694150a757a61bdac07fbf');
INSERT INTO `cms_rolepri` VALUES ('40dd2a22c713432a85f9ca4ffe04a0da', '5adaf0ad9ad54703a975c536a94202e3', '113c69c68b9549e99dca335110f99004');
INSERT INTO `cms_rolepri` VALUES ('06c7d1e26a354544bd06d985cf7160c2', '5adaf0ad9ad54703a975c536a94202e3', '4c7a712ea0744cd4ae487d5632276a28');
INSERT INTO `cms_rolepri` VALUES ('3107219ce4834519a65784b559e94a4f', '5adaf0ad9ad54703a975c536a94202e3', '57fed44afb644f8cbc20aeec1a385eec');
INSERT INTO `cms_rolepri` VALUES ('7c033860b07c4bfeb233d27218d02125', '5adaf0ad9ad54703a975c536a94202e3', 'c033396876d2441e89cd4a5eeb0cc80c');
INSERT INTO `cms_rolepri` VALUES ('61b02033651447a182d7f35c65337618', '5adaf0ad9ad54703a975c536a94202e3', '30646d6c7c534de48b10b40181ec61c9');
INSERT INTO `cms_rolepri` VALUES ('01569d2df52a4053a1a383503821fe69', '5adaf0ad9ad54703a975c536a94202e3', '7d2bbd7684164447af1170f9a688b1ad');
INSERT INTO `cms_rolepri` VALUES ('8f891b3c89fe43d88198ca13e2496cb1', '5adaf0ad9ad54703a975c536a94202e3', 'e77cd48a0498446e9c89f64ce95adb8a');
INSERT INTO `cms_rolepri` VALUES ('dbc41a40e980426b9ad5a065970880b7', '5adaf0ad9ad54703a975c536a94202e3', '01c2ba9201a94d91a6c96817b4c124e7');
INSERT INTO `cms_rolepri` VALUES ('18750511a1734e18964324153d031137', '5adaf0ad9ad54703a975c536a94202e3', '674cc9e6d84344d79787d0fe7a0f13e2');
INSERT INTO `cms_rolepri` VALUES ('347918e221f8402da72cbc9ca6041591', '5adaf0ad9ad54703a975c536a94202e3', 'ee0391be728343a7b38b85e17f39e01d');
INSERT INTO `cms_rolepri` VALUES ('ff973f95986b47a8b0010ef5b8de14bd', '5adaf0ad9ad54703a975c536a94202e3', 'ee64d01c925f44559cafa9eb69a7b316');
INSERT INTO `cms_rolepri` VALUES ('589dabf62a6f43ad9857b0d9000851bf', '5adaf0ad9ad54703a975c536a94202e3', '2918285549b44bbdae10d206e3341245');
INSERT INTO `cms_rolepri` VALUES ('e21182364e3344c4b30d6a2e4c6ea77a', '5adaf0ad9ad54703a975c536a94202e3', '479486e7e79e4f8ab8e7410707d2eff5');
INSERT INTO `cms_rolepri` VALUES ('6585d568ff584c7f885eeeea5c948a41', '5adaf0ad9ad54703a975c536a94202e3', '203b633107014db1b422ce40dbb8aa34');
INSERT INTO `cms_rolepri` VALUES ('4b8d9a68989442f7aef6df1359c4ca88', '5adaf0ad9ad54703a975c536a94202e3', '6b93b45f40424494b8714b6ac9ccf758');
INSERT INTO `cms_rolepri` VALUES ('ea2bc378f7624b3c9c9858c23f66cad9', '5adaf0ad9ad54703a975c536a94202e3', 'd333441d87b5442d8ad718b31b384642');
INSERT INTO `cms_rolepri` VALUES ('e82b4e32067c47d98071da9922c0f278', '5adaf0ad9ad54703a975c536a94202e3', '2004d1f6327c479fae471a4f8d890b3d');
INSERT INTO `cms_rolepri` VALUES ('596e414bf3614a249fb629d1665bcb38', '5adaf0ad9ad54703a975c536a94202e3', '5533094c92aa4e3c801f181f9be23ee1');
INSERT INTO `cms_rolepri` VALUES ('19080f08f2724cc58bc86c66e55042ef', '5adaf0ad9ad54703a975c536a94202e3', '2321fce09869418fbdcdecffc0154750');
INSERT INTO `cms_rolepri` VALUES ('c7234ed219a34dfe98e2ffdbe2759945', '5adaf0ad9ad54703a975c536a94202e3', '284aea685eb7429997dab0a0f91d0d0c');
INSERT INTO `cms_rolepri` VALUES ('07b195af76344380a141b878848b7ce3', '5adaf0ad9ad54703a975c536a94202e3', '200c258410784957b140f14875bcd1a7');
INSERT INTO `cms_rolepri` VALUES ('4e38c650ae974f6ab05c04d2eca763a6', '5adaf0ad9ad54703a975c536a94202e3', 'bc26d58ff0f24c278da3baa2a9606909');
INSERT INTO `cms_rolepri` VALUES ('f2df8592887548fca497151d7e7806fe', '5adaf0ad9ad54703a975c536a94202e3', '4e05556478c94685a495bdfa28a27915');
INSERT INTO `cms_rolepri` VALUES ('67fc9d0918dc498d8913ed7469ed64f6', '5adaf0ad9ad54703a975c536a94202e3', '4f732e0dc8bb4541a568e4f512c625eb');
INSERT INTO `cms_rolepri` VALUES ('e539bbc8b3724cff856dabb7c678f4fa', '5adaf0ad9ad54703a975c536a94202e3', 'a75a65a6b58a45869bb81d8eb46e48ed');
INSERT INTO `cms_rolepri` VALUES ('c6e69ea49eb143ac94684a4450ce6499', '5adaf0ad9ad54703a975c536a94202e3', 'a07277b1ad0f4ff1ae16c9525d2db6a1');
INSERT INTO `cms_rolepri` VALUES ('430c66fef7af4c1fb840af1134964034', '5adaf0ad9ad54703a975c536a94202e3', 'c7c8229b695a4cd780974a8a9d4281e0');
INSERT INTO `cms_rolepri` VALUES ('23373da079154131aff966eddd2250a8', '5adaf0ad9ad54703a975c536a94202e3', 'e9a132c01a11497291a8301d38eaaa1f');
INSERT INTO `cms_rolepri` VALUES ('0bb6732798474f69abf42da18866a461', '5adaf0ad9ad54703a975c536a94202e3', '285342ea6d054aa8adca503c799f561d');
INSERT INTO `cms_rolepri` VALUES ('23c0c335f85f42829e5824f25aa43d0a', '5adaf0ad9ad54703a975c536a94202e3', '10dfe625a74d4131918013a66a6405e6');
INSERT INTO `cms_rolepri` VALUES ('e74b56c6c26145c4a73dffd2ef46ab2b', '5adaf0ad9ad54703a975c536a94202e3', '2fa12d1dc46c42c29dd5c20f8a6cbb9a');
INSERT INTO `cms_rolepri` VALUES ('f076535b423742c39425ccb174fe6483', '5adaf0ad9ad54703a975c536a94202e3', 'f4f5d09a19d4487489306f8fec5d49a9');
INSERT INTO `cms_rolepri` VALUES ('7e2168d2605e434c8e29a3ece3eb6d23', '5adaf0ad9ad54703a975c536a94202e3', '5e483c7c626b4364a85bf0ef0ae6aa42');
INSERT INTO `cms_rolepri` VALUES ('3731fc9fde7c4eba99f162c9c2d5b3fa', '5adaf0ad9ad54703a975c536a94202e3', '096a375443f641638aec821fb3bd51b0');
INSERT INTO `cms_rolepri` VALUES ('4fa30ac651594a9db3fabc1102007da3', '5adaf0ad9ad54703a975c536a94202e3', 'fb36cd7579a94f2d81730c41944d4dd8');
INSERT INTO `cms_rolepri` VALUES ('c5e259709da54782b8c988ccde1d5def', '5adaf0ad9ad54703a975c536a94202e3', 'b793b95bd92147eb9e42eb274dd25d32');
INSERT INTO `cms_rolepri` VALUES ('a191770632d34890b718763208ec45a7', '5adaf0ad9ad54703a975c536a94202e3', 'f4a4cf0a0367460fafee0b5c0f8f1f08');
INSERT INTO `cms_rolepri` VALUES ('c7ab44bbf0ce49f79bf8d1b9596dbac5', '5adaf0ad9ad54703a975c536a94202e3', 'a384773a82024076932fd2a96a10be1a');
INSERT INTO `cms_rolepri` VALUES ('d33edf3086f34884a6fb818dbd145074', '5adaf0ad9ad54703a975c536a94202e3', '56a97061cbd4402d91c37d0d5fad9572');
INSERT INTO `cms_rolepri` VALUES ('48ea3fa97aeb416aa5ba55e77aeaec37', '5adaf0ad9ad54703a975c536a94202e3', '32e0b981eec9452280b66e4648e31948');
INSERT INTO `cms_rolepri` VALUES ('cd05f856f32948f690d84da037d05099', '5adaf0ad9ad54703a975c536a94202e3', 'bbb0ff859830492da4043e3f46eeb232');
INSERT INTO `cms_rolepri` VALUES ('54bb50f9ddcd4ddaa854e5e414be1a8b', '4640f8c6fe5248c38adcfaa01a55babb', '5533094c92aa4e3c801f181f9be23ee1');
INSERT INTO `cms_rolepri` VALUES ('82ddc2e245ec4bdd87a07460ff4ba13b', '4640f8c6fe5248c38adcfaa01a55babb', '2321fce09869418fbdcdecffc0154750');
INSERT INTO `cms_rolepri` VALUES ('d8a2446c14e84bc18c1c34dcf6c69408', '4640f8c6fe5248c38adcfaa01a55babb', '200c258410784957b140f14875bcd1a7');
INSERT INTO `cms_rolepri` VALUES ('219bb0eb50b347ea801e2843a2ab983c', '4640f8c6fe5248c38adcfaa01a55babb', 'bc26d58ff0f24c278da3baa2a9606909');
INSERT INTO `cms_rolepri` VALUES ('a560ab89151947c1b462a942c236ba36', '4640f8c6fe5248c38adcfaa01a55babb', '4e05556478c94685a495bdfa28a27915');
INSERT INTO `cms_rolepri` VALUES ('1e9e4c4cc9364fdb814ab9bfeb5b36fd', '4640f8c6fe5248c38adcfaa01a55babb', '4f732e0dc8bb4541a568e4f512c625eb');
INSERT INTO `cms_rolepri` VALUES ('199923b0e56d4e6eb5fb0ea3999e9e8a', '4640f8c6fe5248c38adcfaa01a55babb', 'a75a65a6b58a45869bb81d8eb46e48ed');
INSERT INTO `cms_rolepri` VALUES ('8eef7c62394546aaa0bfefb0b6b725de', '4640f8c6fe5248c38adcfaa01a55babb', 'c7c8229b695a4cd780974a8a9d4281e0');
INSERT INTO `cms_rolepri` VALUES ('190018aec17d42f180ff9a22d2fb3a80', '4640f8c6fe5248c38adcfaa01a55babb', 'e9a132c01a11497291a8301d38eaaa1f');
INSERT INTO `cms_rolepri` VALUES ('86d006fb274d465e82c7e37cd667193a', '4640f8c6fe5248c38adcfaa01a55babb', '285342ea6d054aa8adca503c799f561d');
INSERT INTO `cms_rolepri` VALUES ('92d26aeb5bd74831aa351f133a836c27', '4640f8c6fe5248c38adcfaa01a55babb', '10dfe625a74d4131918013a66a6405e6');
INSERT INTO `cms_rolepri` VALUES ('b597b8559dcd457694e95d00dd7f8484', '4640f8c6fe5248c38adcfaa01a55babb', '2fa12d1dc46c42c29dd5c20f8a6cbb9a');
INSERT INTO `cms_rolepri` VALUES ('08ba296594aa44349718fab15d603f2b', '4640f8c6fe5248c38adcfaa01a55babb', 'b793b95bd92147eb9e42eb274dd25d32');
INSERT INTO `cms_rolepri` VALUES ('d728be8521094ab38c475570764a9009', '4640f8c6fe5248c38adcfaa01a55babb', 'f4a4cf0a0367460fafee0b5c0f8f1f08');
INSERT INTO `cms_rolepri` VALUES ('08e940bd01c0410fbf0383681c77eb8d', '4640f8c6fe5248c38adcfaa01a55babb', 'a384773a82024076932fd2a96a10be1a');
INSERT INTO `cms_rolepri` VALUES ('81ea0f6feca948dba075616c6a004a0f', '4640f8c6fe5248c38adcfaa01a55babb', '56a97061cbd4402d91c37d0d5fad9572');
INSERT INTO `cms_rolepri` VALUES ('5ed914408691443fa1bb40770b65f510', '4640f8c6fe5248c38adcfaa01a55babb', '32e0b981eec9452280b66e4648e31948');
INSERT INTO `cms_rolepri` VALUES ('376a89a2d8f047d6a22199fb6d833d03', '4640f8c6fe5248c38adcfaa01a55babb', 'bbb0ff859830492da4043e3f46eeb232');
INSERT INTO `cms_rolepri` VALUES ('a65ab3eb0385418fb68598e6e5d2b709', '4640f8c6fe5248c38adcfaa01a55babb', '17dade74081f415db361fc94c2bd8bec');
INSERT INTO `cms_rolepri` VALUES ('f4622f98c41042d49f8ca9994025e3c3', '4640f8c6fe5248c38adcfaa01a55babb', '45d4d9c3057f456d98c3eab718df1769');
INSERT INTO `cms_rolepri` VALUES ('a205be35dd084a38bddd46f330ef9bab', 'b9d8dc92128e46bca71757d31cbc8aff', 'ee64d01c925f44559cafa9eb69a7b316');
INSERT INTO `cms_rolepri` VALUES ('d75e611e8acb4214a8e6307092483000', 'b9d8dc92128e46bca71757d31cbc8aff', '2918285549b44bbdae10d206e3341245');
INSERT INTO `cms_rolepri` VALUES ('868e017e8f604623b673c737a959a53c', 'b9d8dc92128e46bca71757d31cbc8aff', '479486e7e79e4f8ab8e7410707d2eff5');
INSERT INTO `cms_rolepri` VALUES ('f54d547bdcef47ad9b81fb0b2cecbc8f', 'b9d8dc92128e46bca71757d31cbc8aff', '203b633107014db1b422ce40dbb8aa34');
INSERT INTO `cms_rolepri` VALUES ('ddf4f7b83c7f46fabb476dbd3ecbc49d', 'b9d8dc92128e46bca71757d31cbc8aff', '6b93b45f40424494b8714b6ac9ccf758');
INSERT INTO `cms_rolepri` VALUES ('fb2b414907934bce8598572e970d25b3', 'b9d8dc92128e46bca71757d31cbc8aff', 'd333441d87b5442d8ad718b31b384642');
INSERT INTO `cms_rolepri` VALUES ('896ecce76c3246a4a7b5e08f16dc2071', 'b9d8dc92128e46bca71757d31cbc8aff', '2004d1f6327c479fae471a4f8d890b3d');
INSERT INTO `cms_rolepri` VALUES ('72dfd54b95684f7fad109db7153485d4', 'b9d8dc92128e46bca71757d31cbc8aff', '5533094c92aa4e3c801f181f9be23ee1');
INSERT INTO `cms_rolepri` VALUES ('31113fd1ca1b4b7eb2187bd6322d8cc3', 'b9d8dc92128e46bca71757d31cbc8aff', '2321fce09869418fbdcdecffc0154750');
INSERT INTO `cms_rolepri` VALUES ('f07dc50983924027b69a926040d80cb9', 'b9d8dc92128e46bca71757d31cbc8aff', '284aea685eb7429997dab0a0f91d0d0c');
INSERT INTO `cms_rolepri` VALUES ('30f2fa8e43304d26b1580e268dd927db', 'b9d8dc92128e46bca71757d31cbc8aff', '200c258410784957b140f14875bcd1a7');
INSERT INTO `cms_rolepri` VALUES ('b681b823a567487882149b3f488f8a45', 'b9d8dc92128e46bca71757d31cbc8aff', 'bc26d58ff0f24c278da3baa2a9606909');
INSERT INTO `cms_rolepri` VALUES ('46b2649741ef458a808a906852adbc9f', 'b9d8dc92128e46bca71757d31cbc8aff', '4e05556478c94685a495bdfa28a27915');
INSERT INTO `cms_rolepri` VALUES ('492630e649c2480e989e5f166d392a35', 'b9d8dc92128e46bca71757d31cbc8aff', '4f732e0dc8bb4541a568e4f512c625eb');
INSERT INTO `cms_rolepri` VALUES ('66209533f51047b3a1215276ae4c05b8', 'b9d8dc92128e46bca71757d31cbc8aff', 'a75a65a6b58a45869bb81d8eb46e48ed');
INSERT INTO `cms_rolepri` VALUES ('b541e16665e44cb3a0ed6c88f6db0f8d', 'b9d8dc92128e46bca71757d31cbc8aff', 'a07277b1ad0f4ff1ae16c9525d2db6a1');
INSERT INTO `cms_rolepri` VALUES ('0fc92c8bb5b944568f49829db095a145', 'b9d8dc92128e46bca71757d31cbc8aff', 'c7c8229b695a4cd780974a8a9d4281e0');
INSERT INTO `cms_rolepri` VALUES ('e4d8bd9111584f28b1c4c14b858114a6', 'b9d8dc92128e46bca71757d31cbc8aff', 'e9a132c01a11497291a8301d38eaaa1f');
INSERT INTO `cms_rolepri` VALUES ('5d53a90a22044a7c856523e922003bc3', 'b9d8dc92128e46bca71757d31cbc8aff', '285342ea6d054aa8adca503c799f561d');
INSERT INTO `cms_rolepri` VALUES ('f590d7dd0e6f4ae6a82784e3c4230952', 'b9d8dc92128e46bca71757d31cbc8aff', '10dfe625a74d4131918013a66a6405e6');
INSERT INTO `cms_rolepri` VALUES ('28c347a8ffbf421d9ef02bfb246f868e', 'b9d8dc92128e46bca71757d31cbc8aff', '2fa12d1dc46c42c29dd5c20f8a6cbb9a');
INSERT INTO `cms_rolepri` VALUES ('11a663b7a02043e49108daf76302c78b', 'b9d8dc92128e46bca71757d31cbc8aff', 'f4f5d09a19d4487489306f8fec5d49a9');
INSERT INTO `cms_rolepri` VALUES ('48d13552539642f199409bf4ac461c7b', 'b9d8dc92128e46bca71757d31cbc8aff', '5e483c7c626b4364a85bf0ef0ae6aa42');
INSERT INTO `cms_rolepri` VALUES ('5fb50250ff4047cba71832e4bfeb2a9f', 'b9d8dc92128e46bca71757d31cbc8aff', '096a375443f641638aec821fb3bd51b0');
INSERT INTO `cms_rolepri` VALUES ('fe33d584fd0c452b97c3b259f9d1a976', 'b9d8dc92128e46bca71757d31cbc8aff', 'fb36cd7579a94f2d81730c41944d4dd8');
INSERT INTO `cms_rolepri` VALUES ('3f14a2a17fc4406a8195d5528cf44951', 'b9d8dc92128e46bca71757d31cbc8aff', 'b793b95bd92147eb9e42eb274dd25d32');
INSERT INTO `cms_rolepri` VALUES ('a0403ff6b6e64d2c80d26bdeb05d6cb4', 'b9d8dc92128e46bca71757d31cbc8aff', 'f4a4cf0a0367460fafee0b5c0f8f1f08');
INSERT INTO `cms_rolepri` VALUES ('5d5f082631c9449bb8e1240a41402d4d', 'b9d8dc92128e46bca71757d31cbc8aff', 'a384773a82024076932fd2a96a10be1a');
INSERT INTO `cms_rolepri` VALUES ('7cd86d65ad0f40eb96b392b8798b4dda', 'b9d8dc92128e46bca71757d31cbc8aff', '56a97061cbd4402d91c37d0d5fad9572');
INSERT INTO `cms_rolepri` VALUES ('4e0f81aacfb343a2b914001930bf6078', 'b9d8dc92128e46bca71757d31cbc8aff', '32e0b981eec9452280b66e4648e31948');
INSERT INTO `cms_rolepri` VALUES ('995b4dd06a61486abd769b2d3bd57ce0', 'b9d8dc92128e46bca71757d31cbc8aff', 'bbb0ff859830492da4043e3f46eeb232');
INSERT INTO `cms_rolepri` VALUES ('cc0868b3dc1a417096e818b90a664267', 'b9d8dc92128e46bca71757d31cbc8aff', 'e6c92e1fd41245649b5f91954de45cf4');
INSERT INTO `cms_rolepri` VALUES ('7704be12cf974c42830b59993fd98c97', 'b9d8dc92128e46bca71757d31cbc8aff', '18b8c00a09ff4ab39352da3562693e9d');
INSERT INTO `cms_rolepri` VALUES ('1279bf7422194a2597c798f05c0a9822', 'b9d8dc92128e46bca71757d31cbc8aff', '737a19eb1de1440bab5dfda2769b4c4b');
INSERT INTO `cms_rolepri` VALUES ('104faba89af24747bb495238308df24f', 'b9d8dc92128e46bca71757d31cbc8aff', '816cd43d63a54abc9ff382b3a1f2f272');
INSERT INTO `cms_rolepri` VALUES ('039b816c46e441228592575f38c8a4ff', 'b9d8dc92128e46bca71757d31cbc8aff', '17dade74081f415db361fc94c2bd8bec');
INSERT INTO `cms_rolepri` VALUES ('46640836ec494ae389fcb604b47a3b9b', 'b9d8dc92128e46bca71757d31cbc8aff', '45d4d9c3057f456d98c3eab718df1769');
INSERT INTO `cms_rolepri` VALUES ('cd0fe4ec2bca49eb9fa1e28229e758c2', '5adaf0ad9ad54703a975c536a94202e3', 'e6c92e1fd41245649b5f91954de45cf4');
INSERT INTO `cms_rolepri` VALUES ('739ae16a64b44653830881618e2717da', '5adaf0ad9ad54703a975c536a94202e3', '18b8c00a09ff4ab39352da3562693e9d');
INSERT INTO `cms_rolepri` VALUES ('6a849774e521444bb3809485a63ea80e', '5adaf0ad9ad54703a975c536a94202e3', '737a19eb1de1440bab5dfda2769b4c4b');
INSERT INTO `cms_rolepri` VALUES ('de9598e3dd6744408b19e56af6305130', '5adaf0ad9ad54703a975c536a94202e3', '816cd43d63a54abc9ff382b3a1f2f272');
INSERT INTO `cms_rolepri` VALUES ('62f6aaaa62a3410c830a6a8a23e20f8f', '5adaf0ad9ad54703a975c536a94202e3', '17dade74081f415db361fc94c2bd8bec');
INSERT INTO `cms_rolepri` VALUES ('b69ce2f0e1f54f7b8a2a8138cd717230', '5adaf0ad9ad54703a975c536a94202e3', '45d4d9c3057f456d98c3eab718df1769');
INSERT INTO `cms_rolepri` VALUES ('b98cbe61d2df4577b6c11b5b16cf07a7', '5adaf0ad9ad54703a975c536a94202e3', '5e38ca10515048f9b502e4cef8b26c44');
INSERT INTO `cms_rolepri` VALUES ('e9779e7fd6cd4b15bed88d1314bf6306', '5adaf0ad9ad54703a975c536a94202e3', '76eb781ddeb94a38aa25090e32f47aa0');
INSERT INTO `cms_rolepri` VALUES ('359b84a5310243cb9f42a57adefa0dd6', '5adaf0ad9ad54703a975c536a94202e3', 'c2b01ea60d3d4612ae6a3cdf6bb086fc');
INSERT INTO `cms_rolepri` VALUES ('2c64a944f189422aa8a875c9dec52125', '4640f8c6fe5248c38adcfaa01a55babb', 'aa1da01788d2410c932f382f66d7b9be');
INSERT INTO `cms_rolepri` VALUES ('5f4cf623011340b3addd785abbcaa070', '4640f8c6fe5248c38adcfaa01a55babb', '720c056805414dffa9bc46860d2672da');
INSERT INTO `cms_rolepri` VALUES ('eb884666132a4d5ca5d19e987ef44256', '4640f8c6fe5248c38adcfaa01a55babb', 'b45e5303ba0d4a84b3f889ace7682f95');
INSERT INTO `cms_rolepri` VALUES ('fa782fb0480b4252aabe2de1919642f9', 'b9d8dc92128e46bca71757d31cbc8aff', '9ec33b86fc2a43d799eb4e61f9fc75f7');
INSERT INTO `cms_rolepri` VALUES ('482ccf66ef06424ba9bb59c10a7bc9a5', 'b9d8dc92128e46bca71757d31cbc8aff', '43abd33dde83442d9ef0d6ed0668e22b');
INSERT INTO `cms_rolepri` VALUES ('8dfc2bb1730e4d8fb80c075392ca1807', 'b9d8dc92128e46bca71757d31cbc8aff', 'fbc9ec03c6cb49f3bff66ecf948e14e1');
INSERT INTO `cms_rolepri` VALUES ('491bc4c8a353422ea63630638afc32d1', '5adaf0ad9ad54703a975c536a94202e3', '9fac8ca5cbfc478795b6a533bea00bff');
INSERT INTO `cms_rolepri` VALUES ('d737bd09ce2848839c322539fd587122', '5adaf0ad9ad54703a975c536a94202e3', 'fbc9ec03c6cb49f3bff66ecf948e14e1');
INSERT INTO `cms_rolepri` VALUES ('03a0268ae04448c98e7572ea36a8be0a', '5adaf0ad9ad54703a975c536a94202e3', '5e38ca10515048f9b502e4cef8b26c44');
INSERT INTO `cms_rolepri` VALUES ('83c25a06ba26486786dd9bf5bee1a5ee', '5adaf0ad9ad54703a975c536a94202e3', '76eb781ddeb94a38aa25090e32f47aa0');
INSERT INTO `cms_rolepri` VALUES ('f200c87c970e44e28c5fa998e1b5a391', '5adaf0ad9ad54703a975c536a94202e3', 'c2b01ea60d3d4612ae6a3cdf6bb086fc');

INSERT INTO `cms_user` VALUES ('42ef5fe9706b4c98bebdae12186ba089', '管理员', 'admin', '{MD5}ISMvKXpXpadDiUoOSoAfww==', '', '', '', '1', '1', '2013-06-01 10:01:01', '0', '1', '2013-06-01 10:01:01', '4cbb677c282d417980f4a6295fb9c045', '2013-06-01 10:01:01', '9', '');

INSERT INTO `cms_userrole` VALUES ('1844982b6f24478fa3ece5756a67f53c', '42ef5fe9706b4c98bebdae12186ba089', '5adaf0ad9ad54703a975c536a94202e3');

INSERT INTO `cms_variable` VALUES ('adminuser', 'administrator');
INSERT INTO `cms_variable` VALUES ('adminpasswd', '{MD5}tZxnvxlqR1gZHkL3ZnDOug==');
INSERT INTO `cms_variable` VALUES ('adminlastlogin', '2013-06-01 10:00:01');
INSERT INTO `cms_variable` VALUES ('adminemail', '');
INSERT INTO `cms_variable` VALUES ('adminphone', '');
INSERT INTO `cms_variable` VALUES ('adminname', '系统管理员');
INSERT INTO `cms_variable` VALUES ('auditflow', '1');
INSERT INTO `cms_variable` VALUES ('badwordenable', '1');
INSERT INTO `cms_variable` VALUES ('badword', '');
INSERT INTO `cms_variable` VALUES ('sysname', '协同发布系统');
INSERT INTO `cms_variable` VALUES ('uploadpath', '') ; 
INSERT INTO `cms_variable` VALUES ('counter', '0') ; 
INSERT INTO `cms_variable` VALUES ('assort', 'message') ; 
INSERT INTO `cms_variable` VALUES ('copyright', '') ;
